#
# Sends a request to call hello() from within a worker.
#

import sys
from time import sleep
from zmq_tasks import add
from enum import Enum
#client.py
import zmq
context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://192.168.14.150:5555")
request=0
class States(Enum):
    READY="READY_FOR_BLK_PROCESS"

while(True):
        print("Sending request {} …READY_FOR_BLK_PROCESS".format(request))
        if(request==0):
            socket.send(b"READY_FOR_BLK_PROCESS")
            print("sent....")
        print("waiting on receive....")
        sleep(1)
        message = socket.recv()
        print("server recieved something....")
        if(message==b'facingContinue'):
            print("recieved facing continue!!!")
            add.delay(1,1)
            print("sending 1644430643") 
            socket.send(b"1644430643")
            print("sent 1644430643")
        if(message==b'facingCompleted'):
            add.delay(1,2)
            print("recieved facing completed!!!")
        if(message==b'facingAbort'):
            add.delay(1,3)
            print("recieved facing abort!!!")
        if(message==b'facingReset'):
            add.delay(1,4)
            print("recieved facing reset!!!")
        if(message==b'slideContinue'):
            print("recieved slide continue!!!")
            add.delay(1,1)
            print("sending 1644430644") 
            socket.send(b"1644430644")
            print("sent 1644430644")
        if(message==b'slideCompleted'):
            add.delay(1,2)
            print("recieved slide completed!!!")
        if(message==b'slideAbort'):
            add.delay(1,3)
            print("recieved slide abort!!!")
        if(message==b'slideReset'):
            add.delay(1,4)
            print("recieved slide reset!!!")
        if(message==b'tapeTransferToSlide'):
            print("recieved tape continue!!!")
            add.delay(1,1)
            print("sending 1644430645") 
            socket.send(b"1644430645")
            print("sent 1644430645")
        if(message==b'tapeDiscard'):
            add.delay(1,2)
            print("recieved tape completed!!!")
        if(message==b'tapeRehydrate'):
            add.delay(1,3)
            print("recieved tape abort!!!")
        if(message==b'tapeReset'):
            add.delay(1,4)
            print("recieved tape reset!!!")
        if(message==b'sectionPolishingComplete'):
            print("recieved section transfer!!!")
            add.delay(1,1)
            print("sending 1644430646") 
            socket.send(b"1644430646")
            print("sent 1644430646")
        if(message==b'sectionTakePolishingCut'):
            print("recieved sectionTakePolishingCut!!!")
            add.delay(1,1)
            print("sending 1644430646") 
            socket.send(b"1644430646")
            print("sent 1644430646")
        else:
            print("Received reply {} [ {} ]". format(request, message))
        request+=1
socket.close()
context.term()


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage: {} number1 number2'.format(sys.argv[0]))
        sys.exit(1)

    # By calling hello.delay(), we request hello() to be executed in a worker
    # rather than executed directly in the current process, which is what a
    # call to hello() would do.
    # http://docs.celeryproject.org/en/latest/userguide/calling.html
    add.delay(sys.argv[1], int(sys.argv[2]))
    print('done adding from celery')

