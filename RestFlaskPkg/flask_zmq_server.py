from flask import Flask, request
from flask_cors import CORS, cross_origin
from flask_restful import Resource, Api
from json import dumps
from flask_jsonpify import jsonify
from time import sleep
from enum import Enum
import os


import time
import zmq
context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:5555")

app = Flask(__name__)
api = Api(app)

CORS(app)
class States(Enum):
    READY="READY_FOR_BLK_PROCESS"

@app.route("/")
def receive_celery():
    global socket,context
    print("before close connection")
    socket.close()
    # context.term()
    # context = zmq.Context() 
    print("close connection")
    socket = context.socket(zmq.REP)
    print("socket connection")
    socket.bind("tcp://*:5555")
    print("default::waiting on receive...")
    message = socket.recv() # Wait for next request from client
    #message = socket.recv_multipart() # Wait for next request from client
    print("default::Received request: {}".format(message))
    message=message.decode()
    print("default::Received request decoded: {}".format(message))
    my_list = message.split(",")
    print("default::Received my_list: {}".format(my_list))
    time.sleep(1)
    return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 
    # return {'id': str(desired_array[0]),'cameraLocation': str(desired_array[1])} 


class StartBlockProcess(Resource):
    def get(self):  
        global socket,context
        os.system('rostopic pub /Supervisor/SupervisorControl std_msgs/String "I_Initializing" --once')
        # os.system('rostopic pub /decision_making/SupervisorFSM/events std_msgs/String "I_Initializing" --once')
        print("StartBlockProcess::sent")
        # print("before close connection")
        # socket.close()
        # # context.term()
        # # context = zmq.Context() 
        # print("close connection")
        # socket = context.socket(zmq.REP)
        # print("socket connection")
        # socket.bind("tcp://*:5555")
        # print("StartBlockProcess::waiting on receive")
        # message = socket.recv() # Wait for next request from client
        # messageAck = b"ack"
        # socket.send(messageAck)
        # print("StartBlockProcess::Received request: {}".format(message))
        # message=message.decode()
        # print("StartBlockProcess::Received request decoded: {}".format(message))
        # my_list = message.split(",")
        # print("StartBlockProcess::Received my_list: {}".format(my_list))
        # return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 

class RunBlockProcess(Resource):
    def get(self):  
        os.system('rostopic pub /Supervisor/SupervisorControl std_msgs/String "I_Process" --once')
        # os.system('rostopic pub /decision_making/SupervisorFSM/events std_msgs/String "I_StartTest" --once')
        print("RunBlockProcess::sent")
        # print("RunBlockProcess::waiting on receive")
        # message = socket.recv() # Wait for next request from client
        # messageAck = b"ack"
        # socket.send(messageAck)
        # print("RunBlockProcess::Received request: {}".format(message))
        # message=message.decode()
        # print("RunBlockProcess::Received request decoded: {}".format(message))
        # my_list = message.split(",")
        # print("RunBlockProcess::Received my_list: {}".format(my_list))
        # return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 

class StopBlockProcess(Resource):
    def get(self):  
        os.system('rostopic pub /Supervisor/SupervisorControl std_msgs/String "I_ShutDown" --once')
        # os.system('rostopic pub /decision_making/SupervisorFSM/events std_msgs/String "I_ShutDown" --once')
        print("StopBlockProcess::sent")
        # print("StopBlockProcess::waiting on receive")
        # message = socket.recv() # Wait for next request from client
        # messageAck = b"ack"
        # socket.send(messageAck)
        # print("StopBlockProcess::Received request: {}".format(message))
        # message=message.decode()
        # print("StopBlockProcess::Received request decoded: {}".format(message))
        # my_list = message.split(",")
        # print("StopBlockProcess::Received my_list: {}".format(my_list))
        # return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 

class FacingContinue(Resource):
    def get(self):  
        os.system('rostopic pub /decision_making/FacingPerceptionEngineFSM/events std_msgs/String "I_ProcessComplete" --once')
        print("FacingContinue::sent")

class FacingCompleted(Resource):
    def get(self):  
        os.system('rostopic pub /decision_making/FacingPerceptionEngineFSM/events std_msgs/String "I_FacingComplete" --once')
        print("facingCompleted::sent")

class FacingAbort(Resource):
    def get(self):  
        os.system('rostopic pub /decision_making/FacingPerceptionEngineFSM/events std_msgs/String "I_FacingComplete" --once')
        print("facingAbort::sent")
        
class FacingReset(Resource):
    def get(self):  
        # global socket
        # message = b"facingReset"
        # socket.send(message) # Send reply back to client
        print("facingReset::sent")
        # print("facingReset::waiting on receive")
        # message = socket.recv() # Wait for next request from client
        # print("facingReset::Received request: {}".format(message))
        # message=message.decode()
        # print("facingReset::Received request decoded: {}".format(message))
        # my_list = message.split(",")
        # print("facingReset::Received my_list: {}".format(my_list))
        # # Do some 'work'
        # time.sleep(1)    
        # return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 

class SlideContinue(Resource):
    def get(self):  
        # global socket
        # message = b"slideContinue"
        # socket.send(message) # Send reply back to client
        print("slideContinue::sent")
        # print("slideContinue::waiting on receive")
        # message = socket.recv() # Wait for next request from client
        # print("slideContinue::Received request: {}".format(message))
        # message=message.decode()
        # print("slideContinue::Received request decoded: {}".format(message))
        # my_list = message.split(",")
        # print("slideContinue::Received my_list: {}".format(my_list))
        # # Do some 'work'
        # time.sleep(1)    
        # return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 

class SlideCompleted(Resource):
    def get(self):  
        # global socket
        # message = b"slideCompleted"
        # socket.send(message) # Send reply back to client
        print("slideCompleted::sent")
        # print("slideCompleted::waiting on receive")
        # message = socket.recv() # Wait for next request from client
        # print("slideCompleted::Received request: {}".format(message))
        # message=message.decode()
        # print("slideCompleted::Received request decoded: {}".format(message))
        # my_list = message.split(",")
        # print("slideCompleted::Received my_list: {}".format(my_list))
        # # Do some 'work'
        # time.sleep(1)    
        # return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 
        
class SlideAbort(Resource):
    def get(self):  
        # global socket
        # message = b"slideAbort"
        # socket.send(message) # Send reply back to client
        print("slideAbort::sent")
        # print("slideAbort::waiting on receive")
        # message = socket.recv() # Wait for next request from client
        # print("slideAbort::Received request: {}".format(message))
        # message=message.decode()
        # print("slideAbort::Received request decoded: {}".format(message))
        # my_list = message.split(",")
        # print("slideAbort::Received my_list: {}".format(my_list))
        # # Do some 'work'
        # time.sleep(1)    
        # return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 
        

class SlideReset(Resource):
    def get(self):  
        # global socket
        # message = b"slideReset"
        # socket.send(message) # Send reply back to client
        print("slideReset::sent")
        # print("slideReset::waiting on receive")
        # message = socket.recv() # Wait for next request from client
        # print("slideReset::Received request: {}".format(message))
        # message=message.decode()
        # print("slideReset::Received request decoded: {}".format(message))
        # my_list = message.split(",")
        # print("slideReset::Received my_list: {}".format(my_list))
        # # Do some 'work'
        # time.sleep(1)    
        # return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 


class TapeTransferToSlide(Resource):
    def get(self):  
        os.system('rostopic pub /decision_making/TapePerceptionEngineFSM/events std_msgs/String "I_ProcessComplete" --once')
        print("tapeTransferToSlide::sent")

class TapeDiscard(Resource):
    def get(self):  
        os.system('rostopic pub /decision_making/TapePerceptionEngineFSM/events std_msgs/String "I_TakeSection" --once')
        print("tapeDiscard::sent")

class TapeAbort(Resource):
    def get(self):  
        os.system('rostopic pub /decision_making/TapePerceptionEngineFSM/events std_msgs/String "I_ProcessComplete" --once')
        print("tapeRehydrate::sent")

class TapeReset(Resource):
    def get(self):  
        global socket
        message = b"tapeReset"
        socket.send(message) # Send reply back to client
        print("tapeReset::sent")
        print("tapeReset::waiting on receive")
        message = socket.recv() # Wait for next request from client
        print("tapeReset::Received request: {}".format(message))
        message=message.decode()
        print("tapeReset::Received request decoded: {}".format(message))
        my_list = message.split(",")
        print("tapeReset::Received my_list: {}".format(my_list))
        # Do some 'work'
        time.sleep(1)    
        return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 

class SectionTakePolishingCut(Resource):
    def get(self):  
        os.system('rostopic pub /decision_making/SectioningPerceptionEngineFSM/events std_msgs/String "I_Polish" --once')
        print("sectionTakePolishingCut::sent")

class SectionPolishingComplete(Resource):
    def get(self):  
        os.system('rostopic pub /decision_making/SectioningPerceptionEngineFSM/events std_msgs/String "I_PolishComplete" --once')
        print("sectionPolishingComplete::sent")

class SectionReject(Resource):
    def get(self):  
        os.system('rostopic pub /decision_making/SectioningPerceptionEngineFSM/events std_msgs/String "I_PolishComplete" --once')     
        print("sectionReject::sent")


api.add_resource(StartBlockProcess, '/startBlockProcess') # Route_0
api.add_resource(RunBlockProcess, '/runBlockProcess') # Route_0
api.add_resource(StopBlockProcess, '/stopBlockProcess') # Route_0
api.add_resource(FacingContinue, '/facingContinue') # Route_1
api.add_resource(FacingCompleted, '/facingCompleted') # Route_2
api.add_resource(FacingAbort, '/facingAbort') # Route_3
api.add_resource(FacingReset, '/facingReset') # Route_4
api.add_resource(SlideContinue, '/slideContinue') # Route_5
api.add_resource(SlideCompleted, '/slideCompleted') # Route_6
api.add_resource(SlideAbort, '/slideAbort') # Route_7
api.add_resource(SlideReset, '/slideReset') # Route_8
api.add_resource(TapeTransferToSlide, '/tapeTransferToSlide') # Route_9
api.add_resource(TapeDiscard, '/tapeDiscard') # Route_10
api.add_resource(TapeAbort, '/tapeRehydrate') # Route_11
api.add_resource(TapeReset, '/tapeReset') # Route_12
api.add_resource(SectionTakePolishingCut, '/sectionTakePolishingCut') # Route_13
api.add_resource(SectionPolishingComplete, '/sectionPolishingComplete') # Route_14
api.add_resource(SectionReject, '/sectionReject') # Route_15



if __name__ == '__main__':
   app.run(port=5002)
   
   
   
# function invoked by python - from jetson to attached - to server here
