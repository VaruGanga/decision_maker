#
# Sends a request to call hello() from within a worker.
#

import sys

from zmq_tasks import add
#client.py
import zmq
context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://10.0.0.23:5555")
for request in range(10):
        print("Sending request {} …".format(request))
        socket.send(b"Hello")
        message = socket.recv()
        print("Received reply {} [ {} ]". format(request, message))
        break
socket.close()
context.term()


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage: {} number1 number2'.format(sys.argv[0]))
        sys.exit(1)

    # By calling hello.delay(), we request hello() to be executed in a worker
    # rather than executed directly in the current process, which is what a
    # call to hello() would do.
    # http://docs.celeryproject.org/en/latest/userguide/calling.html
    add.delay(sys.argv[1], int(sys.argv[2]))
    print('done adding from celery')

