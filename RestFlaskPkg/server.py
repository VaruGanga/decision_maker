from flask import Flask, request
from flask_cors import CORS, cross_origin
from flask_restful import Resource, Api
from json import dumps
from flask_jsonpify import jsonify
from time import sleep

import time
import zmq
context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:5555")

app = Flask(__name__)
api = Api(app)

CORS(app)

@app.route("/")
def hello():    
    global socket
    while True:
        message = socket.recv() # Wait for next request from client
        print("Received request: {}".format(message))
        # Do some 'work'
        time.sleep(1)
        message = b"World"
        socket.send(message) # Send reply back to client
    sleep(5)  
    return jsonify({'ack':'Block Process Information from worker!'})

class FacingContinue(Resource):
    def get(self):  
        sleep(5)    
        return {'ack': 'Facing Continuing...'} 

# class Task(Resource):
#     def get(self):  
#         sleep(5)    
#         return {'task': [{'id':1, 'name':'Clarapath_1'},{'id':2, 'name':'Clarapath_2'}]} 

# class Task_Name(Resource):
#     def get(self, employee_id):
#         print('Task id:' + employee_id)
#         result = {'data': {'id':1, 'name':'Employee1'}}
#         return jsonify(result)       


api.add_resource(FacingContinue, '/facingContinue') # Route_1


# api.add_resource(Task, '/task') # Route_1
# api.add_resource(Task_Name, '/task/<task_id>') # Route_3


if __name__ == '__main__':
   app.run(port=5002)
   
   
# function invoked by python - from jetson to attached - to server here
