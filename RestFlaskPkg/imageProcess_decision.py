#
# Sends a request to call hello() from within a worker.
#

import sys
from time import sleep
from tasks import imageProcess


print("Sending … imageProcess() arg 0 arg1")    
imageProcess.delay(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])    
print("sent....")
# print("waiting on receive....")
# sleep(1)
# message = socket.recv()


# if __name__ == '__main__':
#     if len(sys.argv) != 3:
#         print('usage: {} number1 number2'.format(sys.argv[0]))
#         sys.exit(1)

#     # By calling hello.delay(), we request hello() to be executed in a worker
#     # rather than executed directly in the current process, which is what a
#     # call to hello() would do.
#     # http://docs.celeryproject.org/en/latest/userguide/calling.html
#     imageProcess.delay(int(sys.argv[1]), int(sys.argv[2]))
#     print('done imageProcess from celery')

