import time
from mqclient import mqclient
from celery import Celery
import rospy
import rospy as rp
from std_msgs.msg import String
import zmq
import json
import rosgraph
import os

app = Celery('tasks', backend='rpc://', broker='pyamqp://sectionstar:secstar@0.0.0.0:5672/sample_host')
qh = mqclient()
qh.connect()

@app.task
def hydration(blockId,HydrationTimeS,mssg):
    
    rp.init_node('HydrationMonitor', anonymous=False)  
    pub = rp.Publisher('/decision_making/HydrationWorkerSupervisorFSM/events', String, queue_size=10)
    
    time.sleep(HydrationTimeS)
    #qh.connect()
  
   #rate = rospy.Rate(10) # 10hz
    qh.publish(mssg,'SectioningQueue')
    hello_str = "I_BlockReady"
    rp.loginfo("Hydration worker sending ROS message")
    pub.publish(hello_str)
    return 0

@app.task
def add(x, y):
#    print("hello")  
#    time.sleep(60)
    return x + y

@app.task
def printSomething(somthing):
 #   print("who is this" + somthing)
    return somthing

@app.task
def imageProcess(blockId,cameraLocation, cutIndex, sessionId):
    # global rospy
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://10.0.0.98:5555")
    message = json.dumps(blockId)
    message = message.encode()
    message1 = json.dumps(cameraLocation)
    message1 = message1.encode()

    message2 = blockId+','+str(cameraLocation)+','+str(cutIndex)+','+str(sessionId)
    message2 = message2.encode()
    socket.send(message2)
    print("sent block id , cameraLocation , message2 consists of ",message2)  
    
    # print("waiting on receive....")
    # message = socket.recv()
    # if(message==b'facingContinue'):
    #     print("imageProcess:: received facing continue!!!")
    #     os.system('rostopic pub /decision_making/FacingPerceptionEngineFSM/events std_msgs/String "I_ProcessComplete" --once')
    # if(message==b'facingCompleted'):
    #     print("imageProcess:: received facing continue!!!")
    #     os.system('rostopic pub /decision_making/FacingPerceptionEngineFSM/events std_msgs/String "I_FacingComplete" --once')
    # if(message==b'sectionTakePolishingCut'):
    #     print("imageProcess:: received sectionTakePolishingCut!!!")
    #     os.system('rostopic pub /decision_making/SectioningPerceptionEngineFSM/events std_msgs/String "I_Polish" --once')
    # if(message==b'sectionPolishingComplete'):
    #     print("imageProcess:: received sectionPolishingComplete!!!")
    #     os.system('rostopic pub /decision_making/SectioningPerceptionEngineFSM/events std_msgs/String "I_PolishComplete" --once')
    # if(message==b'sectionReject'):
    #     print("imageProcess:: received sectionReject!!!")
    #     os.system('rostopic pub /decision_making/SectioningPerceptionEngineFSM/events std_msgs/String "I_PolishComplete" --once')     
    # if(message==b'tapeTransferToSlide'):
    #     print("imageProcess:: received tapeTransferToSlide!!!")
    #     os.system('rostopic pub /decision_making/TapePerceptionEngineFSM/events std_msgs/String "I_ProcessComplete" --once')
    # if(message==b'tapeDiscard'):
    #     print("imageProcess:: received tapeDiscard!!!")
    #     os.system('rostopic pub /decision_making/TapePerceptionEngineFSM/events std_msgs/String "I_TakeSection" --once')
    # if(message==b'tapeRehydrate'):
    #     print("imageProcess:: received tapeRehydrate!!!")
    #     os.system('rostopic pub /decision_making/TapePerceptionEngineFSM/events std_msgs/String "I_ProcessComplete" --once')
    socket.close()
    context.term()
    return 0
 
@app.task  
def iProcess_dev(blockId, cameraLocation, cutIndex, sessionId):
    global rospy
    context = zmq.Context()
    socket1 = context.socket(zmq.REQ)
    socket1.connect("tcp://10.0.0.50:5555")
    message = json.dumps(blockId)
    message = message.encode()
    
    message2 = blockId+','+str(cameraLocation)+','+str(cutIndex) +','+str(sessionId) 
    message2 = message2.encode()
    socket1.send(message2)
    
    print("iProcess_dev::sent block id , cameraLocation , message2 consists of ",message2)  
    print("iProcess_dev::waiting on receive....")
    # sleep(1)
    message3 = socket1.recv()
    print("iProcess_dev::server recieved something....")
    if(message3==b'facingContinue'):
        print("iProcess_dev::recieved facing continue!!!")
        add.delay(1,1)
        print("sending block id , cameraLocation , message2 consists of ",message2) 
        socket1.send(message2)
        print("sent block id , cameraLocation , message2 consists of ",message2)    
        # pubStr='/decision_making/FacingPerceptionEngineFSM/events'
        # rospy.init_node('WorkerMonitor', anonymous=False)
        # print('ros WorkerMonitor launched!!!')
        # pub = rp.Publisher('/decision_making/FacingPerceptionEngineFSM/events', String, queue_size=10)
        # continue_str = "I_ProcessComplete"
        # rate = rospy.Rate(10) # 10hz
        # print("publishing to ros ",continue_str)
        # pub.publish(continue_str)
        #rate.sleep()
    # if(message3==b'facingCompleted'):
    #     print("imageProcess:: received facingCompleted!!!")
    #     add.delay(1,1)
    #     print("iProcess_dev::sending block id , cameraLocation , message2 consists of ",message2) 
    #     socket1.send(message2)
    #     print("sent block id , cameraLocation , message2 consists of ",message2)    
    # if(message3==b'sectionTakePolishingCut'):
    #     print("iProcess_dev:: received sectionTakePolishingCut!!!")
    #     add.delay(1,1)
    #     print("iProcess_dev::sending block id , cameraLocation , message2 consists of ",message2) 
    #     socket1.send(message2)
    #     print("sent block id , cameraLocation , message2 consists of ",message2) 
    # if(message3==b'sectionPolishingComplete'):
    #     print("iProcess_dev:: received sectionPolishingComplete!!!")
    #     add.delay(1,1)
    #     print("iProcess_dev::sending block id , cameraLocation , message2 consists of ",message2) 
    #     socket1.send(message2)
    #     print("iProcess_dev::sent block id , cameraLocation , message2 consists of ",message2) 
    # if(message3==b'sectionReject'):
    #     print("iProcess_dev:: received sectionReject!!!")
    #     add.delay(1,1)
    #     print("iProcess_dev::sending block id , cameraLocation , message2 consists of ",message2) 
    #     socket1.send(message2)
    #     print("iProcess_dev::sent block id , cameraLocation , message2 consists of ",message2)
    # if(message3==b'tapeTransferToSlide'):
    #     print("iProcess_dev:: received tapeTransferToSlide!!!")
    #     add.delay(1,1)
    #     print("iProcess_dev::sending block id , cameraLocation , message2 consists of ",message2) 
    #     socket1.send(message2)
    #     print("iProcess_dev::sent block id , cameraLocation , message2 consists of ",message2) 
    # if(message3==b'tapeDiscard'):
    #     print("iProcess_dev:: received tapeDiscard!!!")
    #     add.delay(1,1)
    #     print("iProcess_dev::sending block id , cameraLocation , message2 consists of ",message2) 
    #     socket1.send(message2)
    #     print("iProcess_dev::sent block id , cameraLocation , message2 consists of ",message2) 
    # if(message3==b'tapeRehydrate'):
    #     print("iProcess_dev:: received tapeRehydrate!!!")
    #     add.delay(1,1)
    #     print("iProcess_dev::sending block id , cameraLocation , message2 consists of ",message2) 
    #     socket1.send(message2)
    #     print("iProcess_dev::sent block id , cameraLocation , message2 consists of ",message2)
    socket1.close()
    context.term()
    return 0
	
 
@app.task  
def iProcess_dev_remote(blockId,cameraLocation, cutIndex, sessionId):
    # global rospy
    context = zmq.Context()
    socket1 = context.socket(zmq.REQ)
    socket1.connect("tcp://192.168.14.150:5555")
    message = json.dumps(blockId)
    message = message.encode()
    
    message2 = blockId+','+str(cameraLocation)+','+str(cutIndex)+','+str(sessionId)
    message2 = message2.encode()
    socket1.send(message2)
    
    print("iProcess_dev_remote::sent block id , cameraLocation , message2 consists of ",message2)  
    # print("iProcess_dev_remote::waiting on receive....")
    # # sleep(1)
    # message = socket1.recv()
    # print("iProcess_dev_remote::server recieved something....")
    # if(message==b'facingContinue'):
    #     print("recieved facing continue!!!")
    #     add.delay(1,1)
    #     print("iProcess_dev_remote::sending block id , cameraLocation , message2 consists of ",message2) 
    #     socket1.send(message2)
    #     print("sent block id , cameraLocation , message2 consists of ",message2)
    # if(message==b'facingCompleted'):
    #     print("iProcess_dev_remote::recieved facingCompleted!!!")
    #     add.delay(1,1)
    #     print("iProcess_dev_remote::sending block id , cameraLocation , message2 consists of ",message2) 
    #     socket1.send(message2)
    #     print("sent block id , cameraLocation , message2 consists of ",message2)
    # if(message==b'sectionTakePolishingCut'):
    #     print("iProcess_dev_remote:: received sectionTakePolishingCut!!!")
    #     add.delay(1,1)
    #     print("iProcess_dev_remote::sending block id , cameraLocation , message2 consists of ",message2) 
    #     socket1.send(message2)
    #     print("iProcess_dev_remote::sent block id , cameraLocation , message2 consists of ",message2)
    # if(message==b'sectionPolishingComplete'):
    #     print("iProcess_dev_remote:: received sectionPolishingComplete!!!")
    #     add.delay(1,1)
    #     print("iProcess_dev_remote::sending block id , cameraLocation , message2 consists of ",message2) 
    #     socket1.send(message2)
    #     print("iProcess_dev_remote::sent block id , cameraLocation , message2 consists of ",message2)
    # if(message==b'sectionReject'):
    #     print("iProcess_dev_remote:: received sectionReject!!!")
    #     add.delay(1,1)
    #     print("iProcess_dev_remote::sending block id , cameraLocation , message2 consists of ",message2) 
    #     socket1.send(message2)
    #     print("iProcess_dev_remote::sent block id , cameraLocation , message2 consists of ",message2)
    # if(message==b'tapeTransferToSlide'):
    #     print("iProcess_dev:: received tapeTransferToSlide!!!")
    #     add.delay(1,1)
    #     print("iProcess_dev::sending block id , cameraLocation , message2 consists of ",message2) 
    #     socket1.send(message2)
    #     print("iProcess_dev::sent block id , cameraLocation , message2 consists of ",message2) 
    # if(message==b'tapeDiscard'):
    #     print("iProcess_dev:: received tapeDiscard!!!")
    #     add.delay(1,1)
    #     print("iProcess_dev::sending block id , cameraLocation , message2 consists of ",message2) 
    #     socket1.send(message2)
    #     print("iProcess_dev::sent block id , cameraLocation , message2 consists of ",message2) 
    # if(message==b'tapeRehydrate'):
    #     print("iProcess_dev:: received tapeRehydrate!!!")
    #     add.delay(1,1)
    #     print("iProcess_dev::sending block id , cameraLocation , message2 consists of ",message2) 
    #     socket1.send(message2)
    #     print("iProcess_dev::sent block id , cameraLocation , message2 consists of ",message2)
    socket1.close()
    context.term()
    return 0
    
    
    