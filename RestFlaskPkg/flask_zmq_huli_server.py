#!/usr/bin/python
from flask import Flask, request
from flask_cors import CORS, cross_origin
from flask_restful import Resource, Api
from json import dumps
from flask_jsonpify import jsonify
from time import sleep
from enum import Enum
import os

# cors = CORS(app, resources={r"/*": {"origins": "*"}})
import time
import zmq
context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:5555")

app = Flask(__name__)
api = Api(app)

# CORS(app)
cors = CORS(app, resources={r"/*": {"origins": "*"}})

class States(Enum):
    READY="READY_FOR_BLK_PROCESS"

@app.route("/")
def hello():
    global socket,context
    print("before close connection")
    socket.close()
    # context.term()
    # context = zmq.Context() 
    print("close connection")
    socket = context.socket(zmq.REP)
    print("socket connection")
    socket.bind("tcp://*:5555")
    print("default::waiting on receive...")
    message = socket.recv() # Wait for next request from client
    
    messageAck = b"ack"
    socket.send(messageAck)
    #message = socket.recv_multipart() # Wait for next request from client
    print("default::Received request: {}".format(message))
    message=message.decode()
    print("default::Received request decoded: {}".format(message))
    my_list = message.split(",")
    print("default::Received my_list: {}".format(my_list))
    time.sleep(1)
    return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 
    # return {'id': str(desired_array[0]),'cameraLocation': str(desired_array[1])} 

class StartBlockProcess(Resource):
    def get(self):  
        os.system('rostopic pub /Supervisor/SupervisorControl std_msgs/String "I_Initializing" --once')
        # os.system('rostopic pub /decision_making/SupervisorFSM/events std_msgs/String "I_Initializing" --once')
        print("StartBlockProcess::sent")
        print("StartBlockProcess::waiting on receive")
        message = socket.recv() # Wait for next request from client
        messageAck = b"ack"
        socket.send(messageAck)
        return {message:'started block process...'}

class RunBlockProcess(Resource):
    def get(self):  
        os.system('rostopic pub /Supervisor/SupervisorControl std_msgs/String "I_Process" --once')
        # os.system('rostopic pub /decision_making/SupervisorFSM/events std_msgs/String "I_Process" --once')
        print("RunBlockProcess::sent")
        print("RunBlockProcess::waiting on receive")
        message = socket.recv() # Wait for next request from client
        messageAck = b"ack"
        socket.send(messageAck)
        return {message:'running  block process...'}

class StopBlockProcess(Resource):
    def get(self):  
        os.system('rostopic pub /Supervisor/SupervisorControl std_msgs/String "I_ShutDown" --once')
        # os.system('rostopic pub /decision_making/SupervisorFSM/events std_msgs/String "I_ShutDown" --once')
        print("StopBlockProcess::sent")
        print("StopBlockProcess::waiting on receive")
        message = socket.recv() # Wait for next request from client
        messageAck = b"ack"
        socket.send(messageAck)
        return {message:'stoping  block process...'}


class FacingContinue(Resource):
    def get(self):  
        os.system('rostopic pub /decision_making/FacingPerceptionEngineFSM/events std_msgs/String "I_ProcessComplete" --once')
        print("FacingContinue::sent")
        print("FacingContinue::waiting on receive")
        message = socket.recv() # Wait for next request from client
        messageAck = b"ack"
        socket.send(messageAck)
        print("FacingContinue::Received request: {}".format(message))
        message=message.decode()
        print("FacingContinue::Received request decoded: {}".format(message))
        my_list = message.split(",")
        print("FacingContinue::Received my_list: {}".format(my_list))
        return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 

class FacingCompleted(Resource):
    def get(self):  
        os.system('rostopic pub /decision_making/FacingPerceptionEngineFSM/events std_msgs/String "I_FacingComplete" --once')
        print("facingCompleted::sent")
        print("facingCompleted::waiting on receive")
        message = socket.recv() # Wait for next request from client
        messageAck = b"ack"
        socket.send(messageAck)
        print("facingCompleted::Received request: {}".format(message))
        message=message.decode()
        print("facingCompleted::Received request decoded: {}".format(message))
        my_list = message.split(",")
        print("facingCompleted::Received my_list: {}".format(my_list))
        return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 

class FacingAbort(Resource):
    def get(self):  
        os.system('rostopic pub /decision_making/FacingPerceptionEngineFSM/events std_msgs/String "I_FacingComplete" --once')
        print("facingAbort::sent")
        print("facingAbort::waiting on receive")
        message = socket.recv() # Wait for next request from client
        messageAck = b"ack"
        socket.send(messageAck)
        print("facingAbort::Received request: {}".format(message))
        message=message.decode()
        print("facingAbort::Received request decoded: {}".format(message))
        my_list = message.split(",")
        print("facingAbort::Received my_list: {}".format(my_list))
        return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 
        
class FacingReset(Resource):
    def get(self):  
        global socket
        message = b"facingReset"
        socket.send(message) # Send reply back to client
        print("facingReset::sent")
        print("facingReset::waiting on receive")
        message = socket.recv() # Wait for next request from client
        print("facingReset::Received request: {}".format(message))
        message=message.decode()
        print("facingReset::Received request decoded: {}".format(message))
        my_list = message.split(",")
        print("facingReset::Received my_list: {}".format(my_list))
        time.sleep(1)    
        return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 

class SlideContinue(Resource):
    def get(self):  
        global socket
        message = b"slideContinue"
        socket.send(message) # Send reply back to client
        print("slideContinue::sent")
        print("slideContinue::waiting on receive")
        message = socket.recv() # Wait for next request from client
        print("slideContinue::Received request: {}".format(message))
        message=message.decode()
        print("slideContinue::Received request decoded: {}".format(message))
        my_list = message.split(",")
        print("slideContinue::Received my_list: {}".format(my_list))
        # Do some 'work'
        time.sleep(1)    
        return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 

class SlideCompleted(Resource):
    def get(self):  
        global socket
        message = b"slideCompleted"
        socket.send(message) # Send reply back to client
        print("slideCompleted::sent")
        print("slideCompleted::waiting on receive")
        message = socket.recv() # Wait for next request from client
        print("slideCompleted::Received request: {}".format(message))
        message=message.decode()
        print("slideCompleted::Received request decoded: {}".format(message))
        my_list = message.split(",")
        print("slideCompleted::Received my_list: {}".format(my_list))
        # Do some 'work'
        time.sleep(1)    
        return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 
        
class SlideAbort(Resource):
    def get(self):  
        global socket
        message = b"slideAbort"
        socket.send(message) # Send reply back to client
        print("slideAbort::sent")
        print("slideAbort::waiting on receive")
        message = socket.recv() # Wait for next request from client
        print("slideAbort::Received request: {}".format(message))
        message=message.decode()
        print("slideAbort::Received request decoded: {}".format(message))
        my_list = message.split(",")
        print("slideAbort::Received my_list: {}".format(my_list))
        # Do some 'work'
        time.sleep(1)    
        return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 
        

class SlideReset(Resource):
    def get(self):  
        global socket
        message = b"slideReset"
        socket.send(message) # Send reply back to client
        print("slideReset::sent")
        print("slideReset::waiting on receive")
        message = socket.recv() # Wait for next request from client
        print("slideReset::Received request: {}".format(message))
        message=message.decode()
        print("slideReset::Received request decoded: {}".format(message))
        my_list = message.split(",")
        print("slideReset::Received my_list: {}".format(my_list))
        # Do some 'work'
        time.sleep(1)    
        return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 


class TapeTransferToSlide(Resource):
    def get(self):  
        # global socket
        # message = b"tapeTransferToSlide"
        # socket.send(message) # Send reply back to client
        os.system('rostopic pub /decision_making/TapePerceptionEngineFSM/events std_msgs/String "I_ProcessComplete" --once')
        print("tapeTransferToSlide::sent")
        print("tapeTransferToSlide::waiting on receive")
        message = socket.recv() # Wait for next request from client
        messageAck = b"ack"
        socket.send(messageAck)
        print("tapeTransferToSlide::Received request: {}".format(message))
        message=message.decode()
        print("tapeTransferToSlide::Received request decoded: {}".format(message))
        my_list = message.split(",")
        print("tapeTransferToSlide::Received my_list: {}".format(my_list))
        # Do some 'work'
        # time.sleep(1)    
        return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 

class TapeDiscard(Resource):
    def get(self):  
        # global socket
        # message = b"tapeDiscard"
        # socket.send(message) # Send reply back to client
        os.system('rostopic pub /decision_making/TapePerceptionEngineFSM/events std_msgs/String "I_TakeSection" --once')
        print("tapeDiscard::sent")
        print("tapeDiscard::waiting on receive")
        message = socket.recv() # Wait for next request from client
        messageAck = b"ack"
        socket.send(messageAck)
        print("tapeDiscard::Received request: {}".format(message))
        message=message.decode()
        print("tapeDiscard::Received request decoded: {}".format(message))
        my_list = message.split(",")
        print("tapeDiscard::Received my_list: {}".format(my_list))
        # Do some 'work'
        # time.sleep(1)    
        return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 

class TapeAbort(Resource):
    def get(self):  
        # global socket
        # message = b"tapeRehydrate"
        # socket.send(message) # Send reply back to client
        os.system('rostopic pub /decision_making/TapePerceptionEngineFSM/events std_msgs/String "I_ProcessComplete" --once')
        print("tapeRehydrate::sent")
        print("tapeRehydrate::waiting on receive")
        message = socket.recv() # Wait for next request from client
        messageAck = b"ack"
        socket.send(messageAck)
        print("tapeRehydrate::Received request: {}".format(message))
        message=message.decode()
        print("tapeRehydrate::Received request decoded: {}".format(message))
        my_list = message.split(",")
        print("tapeRehydrate::Received my_list: {}".format(my_list))
        # Do some 'work'
        # time.sleep(1)    
        return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 

class TapeReset(Resource):
    def get(self):  
        global socket
        message = b"tapeReset"
        socket.send(message) # Send reply back to client
        print("tapeReset::sent")
        print("tapeReset::waiting on receive")
        message = socket.recv() # Wait for next request from client
        print("tapeReset::Received request: {}".format(message))
        message=message.decode()
        print("tapeReset::Received request decoded: {}".format(message))
        my_list = message.split(",")
        print("tapeReset::Received my_list: {}".format(my_list))
        # Do some 'work'
        time.sleep(1)    
        return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 

class SectionTakePolishingCut(Resource):
    def get(self):  
        # global socket
        # message = b"sectionTakePolishingCut"
        # socket.send(message) # Send reply back to client
        os.system('rostopic pub /decision_making/SectioningPerceptionEngineFSM/events std_msgs/String "I_Polish" --once')
        print("sectionTakePolishingCut::sent")
        print("sectionTakePolishingCut::waiting on receive")
        message = socket.recv() # Wait for next request from client
        messageAck = b"ack"
        socket.send(messageAck)
        print("sectionTakePolishingCut::Received request: {}".format(message))
        message=message.decode()
        print("sectionTakePolishingCut::Received request decoded: {}".format(message))
        my_list = message.split(",")
        print("sectionTakePolishingCut::Received my_list: {}".format(my_list))
        # Do some 'work'
        # time.sleep(1)    
        return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 

class SectionPolishingComplete(Resource):
    def get(self):  
        # global socket
        # message = b"sectionPolishingComplete"
        # socket.send(message) # Send reply back to client
        os.system('rostopic pub /decision_making/SectioningPerceptionEngineFSM/events std_msgs/String "I_PolishComplete" --once')
        print("sectionPolishingComplete::sent")
        print("sectionPolishingComplete::waiting on receive")
        message = socket.recv() # Wait for next request from client
        messageAck = b"ack"
        socket.send(messageAck)
        print("sectionPolishingComplete::Received request: {}".format(message))
        message=message.decode()
        print("sectionPolishingComplete::Received request decoded: {}".format(message))
        my_list = message.split(",")
        print("sectionPolishingComplete::Received my_list: {}".format(my_list))
        # Do some 'work'
        time.sleep(1)    
        return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 

class SectionReject(Resource):
    def get(self):  
        # global socket
        # message = b"sectionReject"
        # socket.send(message) # Send reply back to client
        os.system('rostopic pub /decision_making/SectioningPerceptionEngineFSM/events std_msgs/String "I_PolishComplete" --once')     
        print("sectionReject::sent")
        print("sectionReject::waiting on receive")
        message = socket.recv() # Wait for next request from client
        messageAck = b"ack"
        socket.send(messageAck)
        print("sectionReject::Received request: {}".format(message))
        message=message.decode()
        print("sectionReject::Received request decoded: {}".format(message))
        my_list = message.split(",")
        print("sectionReject::Received my_list: {}".format(my_list))
        # Do some 'work'
        time.sleep(1)    
        return {'id': str(my_list[0]),'cameraLocation': str(my_list[1]),'cutIndex': str(my_list[2]),'session_id': str(my_list[3])} 



api.add_resource(StartBlockProcess, '/startBlockProcess') # Route_0
api.add_resource(RunBlockProcess, '/runBlockProcess') # Route_0
api.add_resource(StopBlockProcess, '/stopBlockProcess') # Route_0                                                              
api.add_resource(FacingContinue, '/facingContinue') # Route_1
api.add_resource(FacingCompleted, '/facingCompleted') # Route_2
api.add_resource(FacingAbort, '/facingAbort') # Route_3
api.add_resource(FacingReset, '/facingReset') # Route_4
api.add_resource(SlideContinue, '/slideContinue') # Route_5
api.add_resource(SlideCompleted, '/slideCompleted') # Route_6
api.add_resource(SlideAbort, '/slideAbort') # Route_7
api.add_resource(SlideReset, '/slideReset') # Route_8
api.add_resource(TapeTransferToSlide, '/tapeTransferToSlide') # Route_9
api.add_resource(TapeDiscard, '/tapeDiscard') # Route_10
api.add_resource(TapeAbort, '/tapeRehydrate') # Route_11
api.add_resource(TapeReset, '/tapeReset') # Route_12
api.add_resource(SectionTakePolishingCut, '/sectionTakePolishingCut') # Route_13
api.add_resource(SectionPolishingComplete, '/sectionPolishingComplete') # Route_14
api.add_resource(SectionReject, '/sectionReject') # Route_15


# api.add_resource(Task, '/task') # Route_1
# api.add_resource(Task_Name, '/task/<task_id>') # Route_3
if __name__ == '__main__':
   app.run(host='10.0.0.98',port=5002) # specify ip here
   
   
# function invoked by python - from jetson to attached - to server here
