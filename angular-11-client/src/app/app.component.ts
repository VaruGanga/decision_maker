import { Component } from '@angular/core';
import{ GlobalConstants } from 'src/app/common/global-constants';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

// export enum State {
//   none,
//   facing,
//   slide,
//   tape,
//   section
// }

export class AppComponent {
  title = 'Human in the Loop RealTime';
  constructor(public ts: GlobalConstants) {}
  // facingActive = false;
  // sectioningActive = false;
  // slideActive = true;
  // tapeActive = false;
  getActiveProject(index: string) {
    this.ts.getActiveProject(index);
  }

  // activeProject(index: string, activeFlg: boolean) {
  //   switch(index){
  //     case "0":
  //       this.facingActive = activeFlg;
  //       break;
  //     case "1":
  //       this.sectioningActive = activeFlg;
  //       break;
  //     case "2":
  //       this.tapeActive = activeFlg;
  //       break;
  //     case "3":
  //       this.slideActive = activeFlg;
  //       break;
  //   }
    
  // }
}
