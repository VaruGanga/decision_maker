export class Block {
    id?: any;
    devices__id?: string;
    sessions__id?: string;
    block_id?: string;  
    created_at?: Date;  
    updated_at?: Date;  
  }
