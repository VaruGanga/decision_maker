export class Image {
  id?: any;
  download_path?: string;
  thumbnail_path?: string;
  description?: string;
  comment?: string;
  camera_location?: string;
  image_type?: string;
  blocks__id?:string;
  sessions__id?: string;
  cut_index?: string;
  func?: string;
  facing_decision?: string;
  facing_action?: string;
  image_index?: string;
  json?: string;
  created_at?: Date;  
  updated_at?: Date; 
}


