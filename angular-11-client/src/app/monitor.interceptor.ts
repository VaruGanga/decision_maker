import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { concatMap, delay, retry, retryWhen } from 'rxjs/operators';

export const retryCount = 10;
export const retryWaitMilliSeconds = 1000;

@Injectable()
export class MonitorInterceptor implements HttpInterceptor {

  constructor() {}

//   intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
//     console.log("monitor.interceptor:: request::", request); 
//     return next.handle(request).pipe(
//       retryWhen(error => 
//         error.pipe(
//           concatMap((error, count) => {
//             if (count <= retryCount ) {
//               console.log("monitor.interceptor:: error detected"); 
//               return of(error);
//             }
//             console.log("monitor.interceptor:: error detected 1"); 
//             return throwError(error);
//           }),
//           delay(retryWaitMilliSeconds)
//         )
//       )
//     )
//   }
// }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      retry()
    )
  }
}
