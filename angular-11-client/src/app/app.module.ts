import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxImageZoomModule } from 'ngx-image-zoom';

import { AddImageComponent } from './components/add-image/add-image.component';
import { ImageDetailsComponent } from './components/image-details/image-details.component';
import { ImagesListComponent } from './components/images-list/images-list.component';
import { EnlargeImageComponent } from './components/enlarge-image/enlarge-image.component';
import { MainComponent } from './components/main/main.component';
import { BlocksListComponent } from './components/blocks-list/blocks-list.component';
import { SectionComponent } from './components/section/section.component';
import { TapeComponent } from './components/tape/tape.component';
import { SlideComponent } from './components/slide/slide.component';
import { FacingComponent } from './components/facing/facing.component';
import { MonitorInterceptor } from './monitor.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    AddImageComponent,
    ImageDetailsComponent,
    ImagesListComponent,
    EnlargeImageComponent,
    MainComponent,
    BlocksListComponent,
    SectionComponent,
    TapeComponent,
    SlideComponent,
    FacingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxImageZoomModule 
  ],
  providers: [
    { 
      provide: HTTP_INTERCEPTORS, 
      useClass: MonitorInterceptor, 
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
