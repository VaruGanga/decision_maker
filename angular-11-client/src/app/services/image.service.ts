import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Image } from '../models/image.model';
import { catchError, retry } from 'rxjs/operators';

const baseUrl = 'http://10.0.0.98:8080/api/images';


const facingWhtUrl = 'http://10.0.0.98:8080/api/images/facing/0';
const facingUvUrl = 'http://10.0.0.98:8080/api/images/facing/1';
const facingLdfUrl = 'http://10.0.0.98:8080/api/images/facing/2';
const facingAllLdfUrl = 'http://10.0.0.98:8080/api/images/ldf';

const sectionWhtUrl = 'http://10.0.0.98:8080/api/images/section/0';
const sectionUvUrl = 'http://10.0.0.98:8080/api/images/section/1';

const tapeWhtUrl = 'http://10.0.0.98:8080/api/images/tape/0';
const tapeUvUrl ='http://10.0.0.98:8080/api/images/tape/1';

const slideWhtUrl ='http://10.0.0.98:8080/api/images/slide/0';
const slideUvUrl ='http://10.0.0.98:8080/api/images/slide/1';

// const facingWhtCutUrl = 'http://10.0.0.98:8080/api/images/facing/0/0';
// const facingUvCutUrl = 'http://10.0.0.98:8080/api/images/facing/1/0';
// const facingLdfCutUrl = 'http://10.0.0.98:8080/api/images/facing/2/0';

// const sectionWhtCutUrl = 'http://10.0.0.98:8080/api/images/section/0/0';
const sectionUvCutUrl = 'http://10.0.0.98:8080/api/images/section/1/0';
const sectionSegCutUrl = 'http://10.0.0.98:8080/api/images/section/3/0';

// const tapeWhtCutUrl = 'http://10.0.0.98:8080/api/images/tape/0/0';
// const tapeUvCutUrl ='http://10.0.0.98:8080/api/images/tape/1/0';

// const slideWhtCutUrl ='http://10.0.0.98:8080/api/images/slide/0/0';
// const slideUvCutUrl ='http://10.0.0.98:8080/api/images/slide/1/0';

// const facingWhtBlkUrl = 'http://10.0.0.98:8080/api/images/facing/0/blockId/';
// const facingUvBlkUrl = 'http://10.0.0.98:8080/api/images/facing/1/blockId';
// const facingLdfBlkUrl = 'http://10.0.0.98:8080/api/images/facing/2/blockId';

// const sectionWhtBlkUrl = 'http://10.0.0.98:8080/api/images/section/0/blockId';
// const sectionUvBlkUrl = 'http://10.0.0.98:8080/api/images/section/1/blockId';

// const tapeWhtBlkUrl = 'http://10.0.0.98:8080/api/images/tape/0/blockId';
// const tapeUvBlkUrl ='http://10.0.0.98:8080/api/images/tape/1/blockId';

// const slideWhtBlkUrl ='http://10.0.0.98:8080/api/images/slide/0/blockId';
// const slideUvBlkUrl ='http://10.0.0.98:8080/api/images/slide/1/blockId';


@Injectable({
  providedIn: 'root'
})
export class ImageService {
  // static http: any;

  constructor(private http: HttpClient) { }

  getAll(): Observable<Image[]> {
    return this.http.get<Image[]>(baseUrl);
  }

  get(id: any): Observable<Image> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  create(data: any): Observable<any> {
    return this.http.post(baseUrl, data);
  }

  update(id: any, data: any): Observable<any> {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  updateComment(comment: any, data: any): Observable<any> {
    return this.http.put(`${baseUrl}/${comment}`, data);
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(baseUrl);
  }

  findByTitle(download_path: any): Observable<Image[]> {
    return this.http.get<Image[]>(`${baseUrl}?download_path=${download_path}`);
  }

  findById(blocks__id: any): Observable<Image[]> {
    return this.http.get<Image[]>(`${baseUrl}?blocks__id=${blocks__id}`);
  }

  findByCameraLocationImageType(cameraLocation: any, image_type: any): Observable<Image> {
     if(cameraLocation === 0 && image_type===0 ) {
      return this.http.get(facingWhtUrl); 
     } else if(cameraLocation === 0 && image_type===1 ) {
      return this.http.get(facingUvUrl); 
    } else if(cameraLocation === 0 && image_type===2 ) {
      return this.http.get(facingLdfUrl); 
     } else if(cameraLocation === 1 && image_type===0 ) {
      // console.log("imageservice::findByCameraLocationImageType: cameralocation = ",cameraLocation, " image_type =  ",image_type);
      return this.http.get(sectionWhtUrl); 
     } else if(cameraLocation ===  1 && image_type=== 1 ) {
      // console.log("imageservice::findByCameraLocationImageType: cameralocation = ",cameraLocation, " image_type =  ",image_type);
      return this.http.get(sectionUvUrl); 
     } else if(cameraLocation === 2 && image_type=== 0 ) {
      return this.http.get(tapeWhtUrl); 
     } else if(cameraLocation === 2 && image_type===1 ) {
      return this.http.get(tapeUvUrl); 
     } else if(cameraLocation === 3 && image_type===0 ) {
      return this.http.get(slideWhtUrl); 
     } else if(cameraLocation === 3 && image_type===1 ) {
      return this.http.get(slideUvUrl); 
     } 

     return  this.http.get(`${baseUrl}/${cameraLocation}`);
    //  return this.http.get<Image[]>(`${baseUrl}?camera_location=${cameraLocation}?image_type=${image_type}`);
  }

  // findByCameraLocationImageTypeCutIndex(cameraLocation: any, blk_id: any, image_type: any, cut_index: any): Observable<Image[]> {
    findByCameraLocationImageTypeCutIndex(cameraLocation: any,  image_type: any, cut_index: any): Observable<Image[]> {
 
    if(cameraLocation ===  1 && image_type=== 1 && cut_index=== 0) {
     // console.log("imageservice::findByCameraLocationImageType: cameralocation = ",cameraLocation, " image_type =  ",image_type);
    //  return this.http.get(sectionUvCutUrl); 
    //  return this.http.get<Image[]>(`${sectionUvCutUrl}/${blk_id}`); 
     return this.http.get<Image[]>(`${sectionUvCutUrl}`); 
    } else if(cameraLocation ===  1 && image_type=== 3 && cut_index=== 0) {
      // console.log("imageservice::findByCameraLocationImageType: cameralocation = ",cameraLocation, " image_type =  ",image_type);
      // return this.http.get(sectionSegCutUrl); 
      // return this.http.get<Image[]>(`${sectionSegCutUrl}/${blk_id}`); 
      return this.http.get<Image[]>(`${sectionSegCutUrl}`); 
     }
    return this.http.get<Image[]>(`${sectionUvCutUrl}`); 
 }

  findAllLdf(sessionId: any, cut_index: any): Observable<Image[]> {
     return this.http.get<Image[]>(`${facingAllLdfUrl}/sessionId/${sessionId}/cutIndex/${cut_index}`);
 }
 
   findByCamLocImageTypeBlockId(cameraLocation: any, image_type: any, block_id: any, cut_index: any, sessionId: any): Observable<Image> {
    // console.log("imageservice::findByCamLocImageTypeBlockId: cameralocation = ",cameraLocation, " image_type =  ",image_type, " cut_index =  ",cut_index);
    if(cameraLocation === 0 && image_type===0 ) {
      return this.http.get(`${facingWhtUrl}/blockId/${block_id}/cutIndex/${cut_index}/sessionId/${sessionId}`);          
     } else if(cameraLocation === 0 && image_type===1 ) {
      return this.http.get(`${facingUvUrl}/blockId/${block_id}/cutIndex/${cut_index}/sessionId/${sessionId}`); 
    } else if(cameraLocation === 0 && image_type===2 ) {
      return this.http.get(`${facingLdfUrl}/blockId/${block_id}/cutIndex/${cut_index}/sessionId/${sessionId}`); 
     } else if(cameraLocation === 1 && image_type===0 ) {
      console.log("imageservice::findByCamLocImageTypeBlockId: cameralocation = ",cameraLocation, " image_type =  ",image_type, " cut_index =  ",cut_index, " sessionId= ", sessionId);
      return this.http.get(`${sectionWhtUrl}/blockId/${block_id}/cutIndex/${cut_index}/sessionId/${sessionId}`); 
     } else if(cameraLocation ===  1 && image_type=== 1 ) {
      console.log("imageservice::findByCamLocImageTypeBlockId: cameralocation = ",cameraLocation, " image_type =  ",image_type, " cut_index =  ",cut_index, " sessionId= ", sessionId);
      return this.http.get(`${sectionUvUrl}/blockId/${block_id}/cutIndex/${cut_index}/sessionId/${sessionId}`); 
     } else if(cameraLocation === 2 && image_type=== 0 ) {
      console.log("imageservice::findByCamLocImageTypeBlockId: cameralocation = ",cameraLocation, " image_type =  ",image_type, " cut_index =  ",cut_index, " sessionId= ", sessionId);
      return this.http.get(`${tapeWhtUrl}/blockId/${block_id}/cutIndex/${cut_index}/sessionId/${sessionId}`); 
     } else if(cameraLocation === 2 && image_type===1 ) {
      console.log("imageservice::findByCamLocImageTypeBlockId: cameralocation = ",cameraLocation, " image_type =  ",image_type, " cut_index =  ",cut_index, " sessionId= ", sessionId);
      return this.http.get(`${tapeUvUrl}/blockId/${block_id}/cutIndex/${cut_index}/sessionId/${sessionId}`); 
     } else if(cameraLocation === 3 && image_type===0 ) {
      return this.http.get(`${slideWhtUrl}/blockId/${block_id}/cutIndex/${cut_index}/sessionId/${sessionId}`); 
     } else if(cameraLocation === 3 && image_type===1 ) {
      return this.http.get(`${slideUvUrl}/blockId/${block_id}/cutIndex/${cut_index}/sessionId/${sessionId}`); 
     } 
    return this.http.get(`${facingWhtUrl}/blockId/${block_id}/cutIndex/${cut_index}/sessionId/${sessionId}`); 

    // return this.http.get<Image[]>(`${baseUrl}?cameraLocation=${cameraLocation}/?image_type=${image_type}/?blocks__id=${blocks__id}`);
   //  return this.http.get<Image[]>(`${baseUrl}?camera_location=${cameraLocation}?image_type=${image_type}`);
 }
}
