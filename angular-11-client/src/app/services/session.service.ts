import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Session } from '../models/session.model';

const baseUrl = 'http://10.0.0.98:8080/api/sessions';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Session[]> {
    return this.http.get<Session[]>(baseUrl);
  }

  get(id: any): Observable<Session> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  findBySessionId(sessionId: any): Observable<Session> {
  
    return this.http.get(`${baseUrl}/sessionId/${sessionId}`); 

    // return this.http.get<Image[]>(`${baseUrl}?cameraLocation=${cameraLocation}/?image_type=${image_type}/?blocks__id=${blocks__id}`);
   //  return this.http.get<Image[]>(`${baseUrl}?camera_location=${cameraLocation}?image_type=${image_type}`);
 }
}
