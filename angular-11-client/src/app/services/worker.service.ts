import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Worker } from '../models/worker.model';

const baseUrl = 'http://10.0.0.98:5002';
const startBlockProcessgUrl = 'http://10.0.0.98:5002/startBlockProcess';
const runBlockProcessgUrl = 'http://10.0.0.98:5002/runBlockProcess';
const stopBlockProcessgUrl = 'http://10.0.0.98:5002/stopBlockProcess';
const facingUrl = 'http://10.0.0.98:5002/facingContinue';
const facingCompletedUrl = 'http://10.0.0.98:5002/facingCompleted';
const facingAbortUrl = 'http://10.0.0.98:5002/facingAbort';
const facingResetUrl = 'http://10.0.0.98:5002/facingReset';


const slideUrl = 'http://10.0.0.98:5002/slideContinue';
const slideCompletedUrl = 'http://10.0.0.98:5002/slideCompleted';
const slideAbortUrl = 'http://10.0.0.98:5002/slideAbort';
const slideResetUrl = 'http://10.0.0.98:5002/slideReset';

const tapeUrl = 'http://10.0.0.98:5002/tapeTransferToSlide';
const tapeDiscardUrl = 'http://10.0.0.98:5002/tapeDiscard';
const tapeRehydrateUrl = 'http://10.0.0.98:5002/tapeRehydrate';
const tapeResetUrl = 'http://10.0.0.98:5002/tapeReset';

const getSectionTakePolishingCutUrl = 'http://10.0.0.98:5002/sectionTakePolishingCut';
const sectionPolishingCompleteUrl = 'http://10.0.0.98:5002/sectionPolishingComplete';
const sectionRejectUrl = 'http://10.0.0.98:5002/sectionReject';

@Injectable({
  providedIn: 'root'
})
export class WorkerService {

  constructor(private http: HttpClient) { }

  get(): Observable<Worker> {
    return this.http.get<Worker>(baseUrl);
  }

  getStartBlockProcess(): Observable<Worker> {
    return this.http.get<Worker>(startBlockProcessgUrl);
  }

  getRunBlockProcess(): Observable<Worker> {
    return this.http.get<Worker>(runBlockProcessgUrl);
  }

  getStopBlockProcess(): Observable<Worker> {
    return this.http.get<Worker>(stopBlockProcessgUrl);
  }

// Facing
  getFacing(): Observable<Worker> {
    return this.http.get<Worker>(facingUrl);
  }
  getFacingCompleted(): Observable<Worker> {
    return this.http.get<Worker>(facingCompletedUrl);
  }

  getFacingAbort(): Observable<Worker> {
    return this.http.get<Worker>(facingAbortUrl);
  }

  getFacingReset(): Observable<Worker> {
    return this.http.get<Worker>(facingResetUrl);
  }

  
// Slide

  getSlideContinue(): Observable<Worker> {
    return this.http.get<Worker>(slideUrl);
  }
  getSlideCompleted(): Observable<Worker> {
    return this.http.get<Worker>(slideCompletedUrl);
  }

  getSlideAbort(): Observable<Worker> {
    return this.http.get<Worker>(slideAbortUrl);
  }

  getSlideReset(): Observable<Worker> {
    return this.http.get<Worker>(slideResetUrl);
  }
// Tape
  getTapeTransferToSlide(): Observable<Worker> {
    return this.http.get<Worker>(tapeUrl);
  }
  getTapeDiscard(): Observable<Worker> {
    return this.http.get<Worker>(tapeDiscardUrl);
  }

  getTapeRehydrate(): Observable<Worker> {
    return this.http.get<Worker>(tapeRehydrateUrl);
  }

  getTapeReset(): Observable<Worker> {
    return this.http.get<Worker>(tapeResetUrl);
  }

// Section
  getSectionTakePolishingCut(): Observable<Worker> {
    return this.http.get<Worker>(getSectionTakePolishingCutUrl);
  }
  getSectionPolishingComplete(): Observable<Worker> {
    return this.http.get<Worker>(sectionPolishingCompleteUrl);
  }
  getSectionReject(): Observable<Worker> {
    return this.http.get<Worker>(sectionPolishingCompleteUrl);
  }
 
}
