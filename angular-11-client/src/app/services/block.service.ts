import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Block } from '../models/block.model';
import { Image } from '../models/image.model';

const block_baseUrl = 'http://10.0.0.98:8080/api/blocks';
const block_baseUrl_all = 'http://10.0.0.98:8080/api/blocks/all/blocks';

@Injectable({
  providedIn: 'root'
})
export class BlockService {
  // static get(blk_id: string) {
  //   throw new Error("Method not implemented.");
  // }

  constructor(private http: HttpClient) { }

  getAll(): Observable<Block[]> {
    return this.http.get<Block[]>(block_baseUrl_all);
  }

  get(id: any): Observable<Block> {
    return this.http.get(`${block_baseUrl}/${id}`);
  }

  create(data: any): Observable<any> {
    return this.http.post(block_baseUrl, data);
  }

  update(id: any, data: any): Observable<any> {
    return this.http.put(`${block_baseUrl}/${id}`, data);
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${block_baseUrl}/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(block_baseUrl);
  }

  // findById(id: any, sessionId: any): Observable<Block[]> {
  //   return this.http.get<Block[]>(`${block_baseUrl}/id/${id}/sessionId/${sessionId}`);
  // }

  findByBlockId(block_id: any, sessionId: any): Observable<Block> {
    return this.http.get<Block>(`${block_baseUrl}/block_id/${block_id}/sessionId/${sessionId}`);
  }

  findByBlockIdCamLocCutIndex(block_id: any, camera_location:any, cut_index:any): Observable<Block[]> {
    return this.http.get<Block[]>(`${block_baseUrl}/block_id=${block_id}/camera_location=${camera_location}/cut_index=${cut_index}`);
  }
  

  findByTitle(block_id: any): Observable<Block[]> {
    return this.http.get<Block[]>(`${block_baseUrl}?block_id=${block_id}`);
  }
}
