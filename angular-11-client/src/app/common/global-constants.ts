import { Injectable } from "@angular/core";
import { BlockService } from 'src/app/services/block.service';
import { ImageService } from 'src/app/services/image.service';
import { Worker } from "src/app/models/worker.model"
import { Block } from "../models/block.model";
import { Image } from "../models/image.model";
@Injectable({
    providedIn: 'root',
  })
export class GlobalConstants {
    public  workerStarted = false;
    public  worker: Worker = {
        id: '...wait to receive information from the queue...',
        cameraLocation: '0',
        cutIndex: '0',
        session_id: '0'
          };
    public  facingActive = false;
    public   sectioningActive = false;
    public   tapeActive = false;
    public   slideActive = false;

    public  facingAvailable = [false, false, false, false]; // flag for each image in facing window current cut
    public   sectioningAvailable = [false, false]; // flag for each image in section window current cut
    public   tapeAvailable = [false, false, false]; 
    public   slideAvailable = [false, false];

    public noIndex = -1;

    blocks?: Block;
    images?: Image[];
    private blockService?: BlockService
    private imageService?: ImageService

    currentFacingWhtImagemicrons = 0;
    currentFacingWhtBlockDefault?: Block;
    currentFacingWhtImageDefault?: Image;
    currentFacingUVImagemicrons = 0;
    currentFacingUVBlockDefault?: Block;
    currentFacingUVImageDefault?: Image;
    currentFacingLDFImagemicrons = 0;
    currentFacingLDFBlockDefault?: Block;
    currentFacingLDFImageDefault?: Image;
    currentFacingLDF1Image?: Image;
    currentFacingLDF2Image?: Image;
    currentFacingLDF3Image?: Image;
    currentFacingLDF4Image?: Image;
    currentFacingLDF5Image?: Image;
    currentFacingLDF6Image?: Image;

    currentTapeWhtImagemicrons = 0;
    currentTapeWhtBlockDefault?: Block;
    currentTapeWhtImageDefault?: Image;
    currentTapeUVImagemicrons = 0;
    currentTapeUVBlockDefault?: Block;
    currentTapeUVImageDefault?: Image;

    currentTapeSectionUVImagemicrons = 0;
    currentTapeSectionUVBlockDefault?: Block;
    currentTapeSectionUVImageDefault?: Image;

    currentTapeSectionSegImagemicrons = 0;
    currentTapeSectionSegBlockDefault?: Block;
    currentTapeSectionSegImageDefault?: Image;

    firstImageDefault?: Image;
    firstFacingWhtImagemicrons = 0;
    firstFacingWhtBlockDefault?: Block;
    firstFacingWhtImageDefault?: Image;
    firstFacingUVImagemicrons = 0;
    firstFacingUVBlockDefault?: Block;
    firstFacingUVImageDefault?: Image;
    firstFacingLDFImagemicrons = 0;
    firstFacingLDFBlockDefault?: Block;
    firstFacingLDFImageDefault?: Image;
  
    currentSectionWhtBlockDefault?: Block;
    currentSectionWhtImageDefault?: Image;
    currentSectionWhtImagemicrons=0;
    currentSectionUVBlockDefault?: Block;
    currentSectionUVImageDefault?: Image;
    currentSectionUVImagemicrons=0;

    firstSectionWhtBlockDefault?: Block;
    firstSectionWhtImageDefault?: Image;
    firstSectionWhtImagemicrons=0;
    firstSectionUVBlockDefault?: Block;
    firstSectionUVImageDefault?: Image;
    firstSectionUVImagemicrons=0;
  
    currentSlideWhtBlockDefault?: Block;
    currentSlideWhtImageDefault?: Image;
    currentSlideWhtImagemicrons=0;
    currentSlideUVBlockDefault?: Block;
    currentSlideUVImageDefault?: Image;
    currentSlideUVImagemicrons=0;

    public deactiveProject(index: string) {
        console.log("globalComponent::facingActive: ",this.facingActive);
        console.log(" sectioningActive: ",this.sectioningActive);
        console.log(" slideActive: ",this.slideActive);
        console.log(" tapeActive: ",this.tapeActive);
        switch(index){
          case "0":
          case '0':
            console.log(" facingAvailable: ",this.facingAvailable);
            this.facingAvailable[0] = false;
            this.facingAvailable[1] = false;
            this.facingAvailable[2] = false;
            this.facingAvailable[3] = false;
            this.facingActive = false;
            break;
          case "1":
          case '1':
            console.log(" sectioningAvailable: ",this.sectioningAvailable);
            this.sectioningAvailable[0] = false;
            this.sectioningAvailable[1] = false;
            this.sectioningActive = false;
            break;
          case "2":
          case '2':
            console.log(" tapeAvailable: ",this.tapeAvailable);
            this.tapeAvailable[0] = false;
            this.tapeAvailable[1] = false;  
            this.tapeAvailable[2] = false;
            this.tapeActive = false;;
            break;
          case "3":
          case '3':
            console.log(" facingAvailable: ",this.facingAvailable);
            this.slideAvailable[0] = false;
            this.slideAvailable[1] = false;
            this.slideActive = false;;
            break;
        }
        console.log("globalComponent::facingActive: ",this.facingActive);
        console.log(" sectioningActive: ",this.sectioningActive);
        console.log(" slideActive: ",this.slideActive);
        console.log(" tapeActive: ",this.tapeActive);
      }

    public activeProject(index: string, activeFlg: boolean, displayIndex: number) {
      if(displayIndex === this.noIndex ) {
        return;
      }
        console.log("globalComponent::facingActive: ",this.facingActive);
        console.log(" sectioningActive: ",this.sectioningActive);
        console.log(" slideActive: ",this.slideActive);
        console.log(" tapeActive: ",this.tapeActive);
        switch(index){
          case "0":
          case '0':
            this.facingAvailable[displayIndex] = true;
            console.log(" facingAvailable: ",this.facingAvailable);
            //if((this.facingAvailable[0] === true) && (this.facingAvailable[1] === true) && (this.facingAvailable[2] === true) && (this.facingAvailable[3] === true)) {
            if((this.facingAvailable[0] === true) && (this.facingAvailable[1] === true) && (this.facingAvailable[2] === true) ) {
              this.facingActive = activeFlg;
            }
            break;
          case "1":
          case '1':
            this.sectioningAvailable[displayIndex] = true;
            console.log(" sectioningAvailable: ",this.sectioningAvailable);
            if((this.sectioningAvailable[0] === true) && (this.sectioningAvailable[1] === true) )  {
              this.sectioningActive = activeFlg;
            }
            break;
          case "2":
          case '2':
            this.tapeAvailable[displayIndex] = true;
            console.log(" tapeAvailable: ",this.tapeAvailable);
            if((this.tapeAvailable[0] === true) && ( this.tapeAvailable[1] === true) && ( this.tapeAvailable[2] === true)) {
              this.tapeActive = activeFlg;
            }
            break;
          case "3":
          case '3':
            this.slideAvailable[displayIndex] = true;
            console.log(" slideAvailable: ",this.slideAvailable);
            if((this.slideAvailable[0] === true) && ( this.slideAvailable[1] === true)) {
              this.slideActive = activeFlg;
            }
            break;
        }
        console.log("globalComponent::facingActive: ",this.facingActive);
        console.log(" sectioningActive: ",this.sectioningActive);
        console.log(" slideActive: ",this.slideActive);
        console.log(" tapeActive: ",this.tapeActive);
      }

      public   getActiveProject(index: string) {
        switch(index){
            case "0":
              return this.facingActive;
            //   break;
            case "1":
                return this.sectioningActive;
            //   break;
            case "2":
                return this.tapeActive;
            //   break;
            case "3":
                return this.slideActive;
            //   break;
          }
          return false;

      }

      
  public searchCamLocaImageTypeBlockId(cameraLocation: number, image_type: number, block_id: string, sessionId: string, activateFlag: boolean): void {
    this.imageService?.findByCamLocImageTypeBlockId(cameraLocation, image_type, block_id, 0, sessionId)
      .subscribe(
        data => {
          // this.ts.currentFacingWhtImageDefault = data;
          switch(cameraLocation)
          {
            case 0:
                switch(image_type)
                {
                  case 0: this.currentFacingWhtImageDefault = data; 
                  console.log("searchCamLocaImageTypeBlockId ",data);    
                    break;
                  case 1: this.currentFacingUVImageDefault = data; 
                  console.log("searchCamLocaImageTypeBlockId ",data);    
                  break;
                  case 2: this.currentFacingLDFImageDefault = data; 
                  console.log("searchCamLocaImageTypeBlockId ",data);    
                  break;
                }
                break;
              }
          // console.log("searchCamLocaImageTypeBlockId ",data);          
        },
        error => {
          console.log(error);
        });
  }

  public searchBlock(cameraLocation:number, blk_id: string, image_type: number): void {
    this.blockService?.get(blk_id)
      .subscribe(
        data => {
          switch(cameraLocation)
          {
            case 0:
              switch(image_type)
              {
                case 0: this.currentFacingWhtBlockDefault = data; break;
                case 1: this.currentFacingUVBlockDefault = data; break;
                case 2: this.currentFacingLDFBlockDefault = data; break;
              }
              break;
        }
          console.log("facingComponent::searchBlock",data);
        },
        error => {
          console.log(error);
        });
  }

      public searchBlockId(camera_location: number): void {
        // this.currentImage = undefined;
        // this.currentIndex = -1;
        console.log("searchBlockId id",this.worker.id);
        this.blockService?.findByBlockId(this.worker.id, this.worker.session_id)
          .subscribe(
            data => {
              this.blocks = data;
              console.log("facingCompoment::searchBlockId blocks__id",this.blocks.id);
               // facing white UV and LDF images
              // if(this.blocks[0].id) {
                // console.log("this.blocks[0].id ", this.blocks[0].id)
                // console.log("this.ts.currentFacingWhtBlockDefault?.id ",this.ts.currentFacingWhtBlockDefault?.id)
                // console.log("this.ts.currentFacingUVBlockDefault?.id ",this.ts.currentFacingUVBlockDefault?.id)
                // console.log("this.ts.currentFacingLDFBlockDefault?.id ",this.ts.currentFacingLDFBlockDefault?.id)
                this.searchBlock(camera_location,this.blocks.id,0);
                this.searchCamLocaImageTypeBlockId(camera_location,0,this.blocks.id, String(this.blocks.sessions__id), false);
                this.searchBlock(camera_location, this.blocks.id,1);
                this.searchCamLocaImageTypeBlockId(camera_location,1,this.blocks.id, String(this.blocks.sessions__id), false);
                this.searchBlock(camera_location,this.blocks.id,2);
                this.searchCamLocaImageTypeBlockId(camera_location,2,this.blocks.id, String(this.blocks.sessions__id), false);
              // }
            },
            error => {
              console.log(error);
            });
      }
}