import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImagesListComponent } from './components/images-list/images-list.component';
import { ImageDetailsComponent } from './components/image-details/image-details.component';
import { AddImageComponent } from './components/add-image/add-image.component';
import { EnlargeImageComponent } from './components/enlarge-image/enlarge-image.component';
import { MainComponent } from './components/main/main.component';
import { BlocksListComponent } from './components/blocks-list/blocks-list.component';
import { FacingComponent } from './components/facing/facing.component';
import { SectionComponent } from './components/section/section.component';
import { TapeComponent } from './components/tape/tape.component';
import { SlideComponent } from './components/slide/slide.component';

const routes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: MainComponent },
  { path: 'facing', component: FacingComponent },
  { path: 'section', component: SectionComponent },
  { path: 'tape', component: TapeComponent },
  { path: 'slide', component: SlideComponent },
  { path: 'blocks', component: BlocksListComponent },
  { path: 'images', component: ImagesListComponent },
  { path: 'images/:id', component: ImageDetailsComponent },
  { path: 'add', component: AddImageComponent },
  { path: 'enlargedImage/:download_path', component: EnlargeImageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
