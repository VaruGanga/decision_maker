import { Component, OnInit } from '@angular/core';
import { Image } from 'src/app/models/image.model';
import { ImageService } from 'src/app/services/image.service';

@Component({
  selector: 'app-add-image',
  templateUrl: './add-image.component.html',
  styleUrls: ['./add-image.component.css']
})
export class AddImageComponent implements OnInit {
  image: Image = {
    download_path: '',
    description: '',
    comment: '',
    camera_location: '',
    image_type: '',
    json: ''
  };
  submitted = false;

  constructor(private imageService: ImageService) { }

  ngOnInit(): void {
  }

  saveImage(): void {
    const data = {
      download_path: this.image.download_path,
      description: this.image.description
    };

    this.imageService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  newImage(): void {
    this.submitted = false;
    this.image = {
      download_path: '',
      description: '',
      comment: '',
      camera_location: '',
      image_type: '',
      json: ''
    };
  }

}
