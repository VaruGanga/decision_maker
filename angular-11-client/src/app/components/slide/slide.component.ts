import { Component, OnInit } from '@angular/core';
import { WorkerService } from 'src/app/services/worker.service';
import { Worker } from "src/app/models/worker.model"
import { Block } from 'src/app/models/block.model';
import { Image } from 'src/app/models/image.model';
import { BlockService } from 'src/app/services/block.service';
import { ImageService } from 'src/app/services/image.service';
import { BlocksListComponent } from 'src/app/components/blocks-list/blocks-list.component';
import { GlobalConstants } from 'src/app/common/global-constants';


@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.css']
})

export class SlideComponent implements OnInit {
  // worker?: Worker[];
  worker: Worker = {
    id: '...wait to receive information from the queue...',
    cameraLocation: '0'
      };
  blockList?:BlocksListComponent;
  // ={
  //   currentFacingWhtImageDefault.download_path = '0/0',
  // };
  blocks?: Block[];
  images?: Image[];
  show = false;
  // currentImageDefault?: Image;
  currentSlideWhtImagemicrons = 0;
  currentSlideWhtBlockDefault?: Block;
  currentSlideWhtImageDefault?: Image;
  currentSlideUVImagemicrons = 0;
  currentSlideUVBlockDefault?: Block;
  currentSlideUVImageDefault?: Image;

  title = 'DecisionMaker';
  constructor(private workerService: WorkerService,private blockService: BlockService, private imageService: ImageService, public ts: GlobalConstants) { }

  ngOnInit(): void {
    console.log("slide::workerStarted ",this.ts.workerStarted);
    if(this.ts.workerStarted === false) {
      this.ts.workerStarted=true;
      this.retrieveDefault();
    }
    // Slide White and UV images
    this.searchCameraLocation(3,0);
    this.searchCameraLocation(3,1);

  }
  retrieveDefault(): void{
    this.workerService.get()
    .subscribe(
      data => {
        this.ts.worker = data;
        // if(this.ts.worker.cameraLocation) {
        //   this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
        // }
        console.log(data);
      },
      error => {
        console.log(error);
      });    
  }

  clickout() {
    this.show = true;
  }
 
  clickit(){
    this.show = !this.show;
  }

  slideContinue(): void{
    this.ts.activeProject("3", false, this.ts.noIndex);
    this.workerService.getSlideContinue()
    .subscribe(
      data => {
        this.ts.worker = data;
        // if(this.ts.worker.cameraLocation=="3") {
        //   this.ts.activeProject("3", true);
        // }
        console.log(data);
      },
      error => {
        console.log(error);
      });    
      
      if(this.blockList!=undefined)
        this.blockList.block_id=this.worker.id;
        this.blockList?.searchTitle();
  }

  
  slideCompleted(): void{
    this.ts.activeProject("3", false, this.ts.noIndex);
    this.workerService.getFacingCompleted()
    .subscribe(
      data => {
        this.ts.worker = data;
        // if(this.ts.worker.cameraLocation=="3") {
        //   this.ts.activeProject("3", true);
        // }
        console.log(data);
      },
      error => {
        console.log(error);
      });    
  }
   
  slideAbort(): void{
    this.ts.activeProject("3", false, this.ts.noIndex);
    this.workerService.getFacingAbort()
    .subscribe(
      data => {
        this.ts.worker = data;
        // if(this.ts.worker.cameraLocation=="3") {
        //   this.ts.activeProject("3", true);
        // }
        console.log(data);
      },
      error => {
        console.log(error);
      });    
  }

  slideReset(): void{
    this.ts.activeProject("3", false, this.ts.noIndex);
    this.workerService.getFacingReset()
    .subscribe(
      data => {
        this.ts.worker = data;
        // if(this.ts.worker.cameraLocation=="3") {
        //   this.ts.activeProject("3", true);
        // }
        console.log(data);
      },
      error => {
        console.log(error);
      });    
  }

  searchCameraLocation(cameraLocation: number, image_type: number): void {
    this.imageService.findByCameraLocationImageType(cameraLocation, image_type)
      .subscribe(
        data => {
          // this.currentFacingWhtImageDefault = data;
          switch(image_type)
          {
            case 0: this.currentSlideWhtImageDefault = data; 
            if(this.currentSlideWhtImageDefault?.blocks__id !== undefined) {
              if(this.currentSlideWhtImageDefault?.cut_index) {
                this.currentSlideWhtImagemicrons = Number(this.currentSlideWhtImageDefault.cut_index) * 4;
              }
              this.searchBlock(this.currentSlideWhtImageDefault.blocks__id, image_type);
            }
            break;
            case 1: this.currentSlideUVImageDefault = data;
            if(this.currentSlideUVImageDefault?.blocks__id !== undefined) {
              if(this.currentSlideUVImageDefault?.cut_index) {
                this.currentSlideUVImagemicrons = Number(this.currentSlideUVImageDefault.cut_index) * 4;
              }
              this.searchBlock(this.currentSlideUVImageDefault.blocks__id, image_type);
            }
             break;
          } 
          console.log(data);          
        },
        error => {
          console.log(error);
        });

        
  }

  searchBlock(blk_id: string, image_type: number): void {
    this.blockService.get(blk_id)
      .subscribe(
        data => {
          switch(image_type)
          {
            case 0: this.currentSlideWhtBlockDefault = data; break;
            case 1: this.currentSlideUVBlockDefault = data; break;
          }
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

}
