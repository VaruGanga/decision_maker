import {Router, ActivatedRoute, Params} from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-enlarge-image',
  templateUrl: './enlarge-image.component.html',
  styleUrls: ['./enlarge-image.component.css']
})
export class EnlargeImageComponent implements OnInit {
  path?='';
  // user_id?: any;
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.path = params['download_path'];
      console.log(this.path );
      // this.user_id = event.user_id;
    });
  }

}
