
import { Component, OnInit } from '@angular/core';
import { WorkerService } from 'src/app/services/worker.service';
import { Worker } from "src/app/models/worker.model"
import { Block } from 'src/app/models/block.model';
import { Image } from 'src/app/models/image.model';
import { Session } from 'src/app/models/session.model';
import { BlockService } from 'src/app/services/block.service';
import { ImageService } from 'src/app/services/image.service';
import { BlocksListComponent } from 'src/app/components/blocks-list/blocks-list.component';
import { GlobalConstants } from 'src/app/common/global-constants';
import { interval, of, throwError } from 'rxjs';
import { catchError, delay, map, mergeMap, retry, retryWhen, tap } from 'rxjs/operators';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.css']
})
export class SectionComponent implements OnInit {
  worker = this.ts.worker;
 // worker?: Worker[];
  // worker: Worker = {
  //   id: '...wait to receive information from the queue...',
  //   cameraLocation: '0'
  //     };
  blockList?:BlocksListComponent;
  // ={
  //   currentSectionWhtImageDefault.download_path = '0/0',
  // };
  blocks?: Block;
  images?: Image[];
  session?: Session = {
    id : null,
    session_id : ''

  };
  show = false;
  refCutIndex=0;
  ret_val = 0;
  // currentImageDefault?: Image;
  // currentSectionWhtImagemicrons = 0;
  // currentSectionWhtBlockDefault?: Block;
  // currentSectionWhtImageDefault?: Image;
  // currentSectionUVImagemicrons = 0;
  // currentSectionUVBlockDefault?: Block;
  // currentSectionUVImageDefault?: Image;

  // refCutIndex = 0;
  // firstImageDefault?: Image;
  // firstFacingWhtImagemicrons = 0;
  // firstFacingWhtBlockDefault?: Block;
  // firstFacingWhtImageDefault?: Image;
  // firstFacingUVImagemicrons = 0;
  // firstFacingUVBlockDefault?: Block;
  // firstFacingUVImageDefault?: Image;
  // firstFacingLDFImagemicrons = 0;
  // firstFacingLDFBlockDefault?: Block;
  // firstFacingLDFImageDefault?: Image;

  title = 'DecisionMaker';
  constructor(private workerService: WorkerService,private blockService: BlockService, private imageService: ImageService, private sessionService: SessionService, public ts: GlobalConstants) { }

  ngOnInit(): void {
    if(this.ts.workerStarted === false) {
      this.ts.workerStarted=true;
      this.retrieveDefault();
    }
    // sectioning White and UV images
    this.ret_val =  this.searchCameraLocation(1,0);
    console.log("ret_val:: searchCameraLocation(1,0)::",this.ret_val);
    this.ret_val =  this.searchCameraLocation(1,1);
    console.log("ret_val:: searchCameraLocation(1,1)::",this.ret_val);
  }
  retrieveDefault(): void{
    const workerApiData = this.workerService.get()
    .pipe(
      map((res: any) => {
        if ((res==null) || (res==undefined) || res.cameraLocation == null) {
          console.log('Error occurred.');
          // delay(100);
          throw new Error('Value expected!');
        }
        return res;
      }),
      retry(), // Retry forever
      // delay(10000),
      catchError(() => of([]))
    );

    workerApiData
    .subscribe(
      data => {
        console.log("retrieveDefault:: ",data);
        this.ts.worker = data;
        console.log('retrieveDefault:: before this.ts.worker.cameraLocation ',this.ts.worker.cameraLocation)

        // if(this.ts.worker.cameraLocation) {
          // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
          if(this.ts.worker.cameraLocation == "1" || this.ts.worker.cameraLocation == '1') {
            this.searchBlockId(this.ts.worker.cameraLocation, this.ts.worker.cutIndex, this.ts.worker.session_id);
          }
        // }
        console.log('retrieveDefault:: after this.ts.worker.cameraLocation ',this.ts.worker.cameraLocation)
        
      },
      error => {
        console.log(error);
      });    
  }
 
  clickout() {
    this.show = true;
  }
 
  clickit(){
    this.show = !this.show;
  }

  sectionTakePolishingCut(): void{
    this.ts.deactiveProject("1");
    this.workerService.getSectionTakePolishingCut()
    .subscribe(
      () => {
        this.workerService.get()
        .subscribe(
          data => {
            this.ts.worker = data;
            console.log("section:: sectionTakePolishingCut :: data ",data);
            // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
            this.searchBlockId(this.ts.worker.cameraLocation, this.ts.worker.cutIndex, this.ts.worker.session_id);
            console.log(data);
      },
      error => {
        console.log("sectionTakePolishingCut:: ", error);
      });    
    });    
      
      if(this.blockList!=undefined)
        this.blockList.block_id=this.ts.worker.id;
        this.blockList?.searchTitle();
  }

  sectionPolishingComplete(): void{
    this.ts.deactiveProject("1");
    this.workerService.getSectionPolishingComplete()
    .subscribe(
      () => {
        this.workerService.get()
        .subscribe(
          data => {
            this.ts.worker = data;
            console.log("section:: sectionPolishingComplete :: data ",data);
            // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
            this.searchBlockId(this.ts.worker.cameraLocation, this.ts.worker.cutIndex, this.ts.worker.session_id);
            console.log(data);
          },
          error => {
            console.log(error);
          });  
      });    
  }

  sectionReject(): void{
    this.ts.deactiveProject("1");
    this.workerService.getSectionReject()
    .subscribe(
      () => {
        this.workerService.get()
        .subscribe(
          data => {
            this.ts.worker = data;
            console.log("section:: sectionReject :: data ",data);
            // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
            this.searchBlockId(this.ts.worker.cameraLocation, this.ts.worker.cutIndex, this.ts.worker.session_id);
            console.log(data);
          },
          error => {
            console.log(error);
          }); 
      },
      error => {
        console.log("sectionReject:: ",error);
      });    
  }
 
  searchCameraLocation(cameraLocation: number, image_type: number): number {
    console.log("searchCameraLocation:: first line");
    this.imageService.findByCameraLocationImageType(cameraLocation, image_type)
      .subscribe(
        (data => {
          //throw error for demonstration
          if (data == null) {
            console.log("--------------found null searchCameraLocation ::",data);
            return throwError(-1);
          }
          switch(cameraLocation)
          {
            case 1:
          // this.ts.currentSectionWhtImageDefault = data;
              switch(image_type)
              {
                case 0: this.ts.currentSectionWhtImageDefault = data; 
                  console.log(" this.ts.currentSectionWhtImageDefault?.sessions__id  ", this.ts.currentSectionWhtImageDefault?.sessions__id);
                  this.searchSessionLabelFromDBId(String(this.ts.currentSectionWhtImageDefault?.sessions__id));
                  console.log(" session  ", this.session);
                  if(this.ts.currentSectionWhtImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentSectionWhtImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentSectionWhtImageDefault?.cut_index)+1;
                      this.ts.currentSectionWhtImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentSectionWhtImagemicrons = (Number(this.ts.currentSectionWhtImageDefault.cut_index) - 1) * 4;
                    }
                    
                    this.searchCamLocaImageTypeBlockId(cameraLocation, image_type, this.ts.currentSectionWhtImageDefault.blocks__id, 0, String(this.ts.currentSectionWhtImageDefault.sessions__id), true);
                    this.searchBlock(cameraLocation,this.ts.currentSectionWhtImageDefault.blocks__id, image_type, Number(this.ts.currentSectionWhtImageDefault.cut_index)-1);
                  }
                  break;
                  
                case 1: this.ts.currentSectionUVImageDefault = data; 
                  if(this.ts.currentSectionUVImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentSectionUVImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentSectionUVImageDefault?.cut_index)+1;
                      this.ts.currentSectionUVImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentSectionUVImagemicrons = (Number(this.ts.currentSectionUVImageDefault.cut_index) - 1) * 4;
                    }
                    this.searchCamLocaImageTypeBlockId(cameraLocation, image_type, this.ts.currentSectionUVImageDefault.blocks__id, 0, String(this.ts.currentSectionUVImageDefault.sessions__id), true);
                    this.searchBlock(cameraLocation,this.ts.currentSectionUVImageDefault.blocks__id, image_type, Number(this.ts.currentSectionUVImageDefault?.cut_index)-1);
                  }
                break;

              }
              break;
          }
          console.log("searchCameraLocation:: ",data);  
          console.log("searchCameraLocation:: last line1");      
          return 0;  
        }),
        error => {
          console.log("searchCameraLocation:: last line2");      
          
          console.log("searchCameraLocation:: error",error);
        });
        console.log("searchCameraLocation:: last line3");      

        return 0;
  }

  searchCamLocaImageTypeBlockId(cameraLocation: number, image_type: number, block_id: string, cut_index: number, sessionId: string, activateFlag: boolean): number {
    const apiData = this.imageService.findByCamLocImageTypeBlockId(cameraLocation, image_type, block_id, cut_index, sessionId)
    .pipe(
      map((res: any) => {
        if ((res==null) || (res==undefined) || (sessionId == null) && (cameraLocation == 1)  ) {
          console.log('Error occurred.');
          // delay(100);
          throw new Error('Value expected!');
        }
        return res;
      }),
      retry(), // Retry forever
      // delay(10000),
      catchError(() => of([]))
    );

    
    
    apiData
      .subscribe(
        (data => {
          //throw error for demonstration
          if (data == null) {
            console.log("--------------found null searchCamLocaImageTypeBlockId ::",data);
            // throw new Error("Invalid data");
            return throwError(-1);
          } else {
            if(!activateFlag) {
              console.log("--------------activate menu flag searchCamLocaImageTypeBlockId ");
              this.ts.activeProject(String(this.ts.worker.cameraLocation), true, image_type);
              }
              console.log("searchCamLocaImageTypeBlockId ",data); 
          switch(cameraLocation)
          {
            case 1:
            // this.ts.currentSectionWhtImageDefault = data;
            switch(image_type)
            {
              case 0: 
              if(activateFlag) {
                this.ts.firstSectionWhtImageDefault = data; 
                console.log(" this.ts.firstSectionWhtImageDefault ",this.ts.firstSectionWhtImageDefault);
                if(this.ts.firstSectionWhtImageDefault?.blocks__id !== undefined) {
                  if(Number(this.ts.firstSectionWhtImageDefault?.cut_index) >= 0) {         
                    this.refCutIndex = Number(this.ts.firstSectionWhtImageDefault?.cut_index)+1;
                    this.ts.firstSectionWhtImageDefault.cut_index = String(this.refCutIndex);
                    console.log(" this.ts.firstSectionWhtImageDefault.cut_index ",this.ts.firstSectionWhtImageDefault.cut_index);
                    this.ts.firstSectionWhtImagemicrons = (Number(this.ts.firstSectionWhtImageDefault.cut_index) - 1) * 4;
                  }
                }
                else  {
                  console.log(" this.ts.firstSectionWhtImageDefault.blocks__id ",this.ts.firstSectionWhtImageDefault?.blocks__id);
                }
              } else {
                  this.ts.currentSectionWhtImageDefault = data; 
            
                  if(this.ts.currentSectionWhtImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentSectionWhtImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentSectionWhtImageDefault.cut_index)+1;
                      this.ts.currentSectionWhtImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentSectionWhtImagemicrons = (Number(this.ts.currentSectionWhtImageDefault.cut_index) - 1) * 4;
                    }
                    // this.searchBlock(cameraLocation,this.ts.currentSectionWhtImageDefault.blocks__id, image_type);
                  }
                }console.log("searchCamLocaImageTypeBlockId ",data);    
                break;
              case 1: 
              if(activateFlag) {
                this.ts.firstSectionUVImageDefault = data; 
                if(this.ts.firstSectionUVImageDefault?.blocks__id !== undefined) {
                  if(Number(this.ts.firstSectionUVImageDefault?.cut_index) >= 0) {
                    this.refCutIndex = Number(this.ts.firstSectionUVImageDefault.cut_index)+1;
                    this.ts.firstSectionUVImageDefault.cut_index = String(this.refCutIndex);
                    console.log(" this.ts.firstSectionUVImageDefault.cut_index ",this.ts.firstSectionUVImageDefault.cut_index);
                    this.ts.firstFacingUVImagemicrons = (Number(this.ts.firstSectionUVImageDefault.cut_index) - 1) * 4;
                  }
                  // this.searchBlock(cameraLocation,this.ts.currentFacingUVImageDefault.blocks__id, image_type);
                }
              } else {
                  this.ts.currentSectionUVImageDefault = data; 
                  if(this.ts.currentSectionUVImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentSectionUVImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentSectionUVImageDefault.cut_index)+1;
                      this.ts.currentSectionUVImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentSectionUVImagemicrons = (Number(this.ts.currentSectionUVImageDefault.cut_index) - 1) * 4;
                    }
                    // this.searchBlock(cameraLocation,this.ts.currentSectionUVImageDefault.blocks__id, image_type);
                  }
                }
                console.log("searchCamLocaImageTypeBlockId ",data);    
                break;
                
            }
          break;
          }
        }
          return 0;
          // console.log("searchCamLocaImageTypeBlockId ",data);          
        } ),
        
        error => {
          console.log("searchCamLocaImageTypeBlockId :: error",error);
          return 0;
        });
        return 0;
  }

  searchBlock(camera_location: number, blk_id: string, image_type: number, cutIndex: number): void {
    this.blockService.get(blk_id)
      .subscribe(
        data => {
          switch(camera_location)
          {
            case 1:
              // if(cutIndex==0){
              //   switch(image_type)
              //   {
              //     case 0: this.firstFacingWhtBlockDefault = data; break;
              //     case 1: this.firstFacingUVBlockDefault = data; break;
              //     case 2: this.firstFacingLDFBlockDefault = data; break;
              //   }
              // } else {
                switch(image_type)
                {
                  case 0: this.ts.currentSectionWhtBlockDefault = data; break;
                  case 1: this.ts.currentSectionUVBlockDefault = data; break;
                }
              //}
              break;
            }
              
          
          console.log("sectionComponent::searchBlock",data);
        },
        error => {
          console.log("searchBlock::error",error);
        });
  }

  //  we have id in session table find the session_id 
  searchSessionLabelFromDBId(id: String): void {
    // this.session = null;
    //const apiDataId = this.sessionService.get(id);
    const apiDataId = this.sessionService.get(id) // findByCamLocImageTypeBlockId(cameraLocation, image_type, block_id, cut_index, sessionId)
    .pipe(
      map((res: any) => {
        if ((res==null) || (res==undefined) ) {
          console.log('searchSessionLabelFromDBId::Error occurred.');
          // delay(100);
          throw new Error('searchSessionLabelFromDBId::Value expected!');
        }
        return res;
      }),
      retry(), // Retry forever
      // delay(10000),
      catchError(() => of([]))
    );

    apiDataId
    .subscribe(
      (sessionData => {
        if (sessionData == null) {
          console.log("--------------sessionData found null searchSessionLabelFromDBId ::",sessionData);
          // throw new Error("Invalid data");
          return throwError(-1);
        } else {
          console.log("--------------sessionData in searchSessionLabelFromDBId ::",sessionData);
          this.session =sessionData;
        }
        return ;
      }
      ))
  }

   //  we have session_id in session table find the id 
  searchSessionId(sessionId: String): void {
    // this.session = null;
    const apiDataSessionId = this.sessionService.findBySessionId(sessionId) // findByCamLocImageTypeBlockId(cameraLocation, image_type, block_id, cut_index, sessionId)
    .pipe(
      map((res: any) => {
        if ((res==null) || (res==undefined) ) {
          console.log('Error occurred.');
          // delay(100);
          throw new Error('Value expected!');
        }
        return res;
      }),
      retry(), // Retry forever
      // delay(10000),
      catchError(() => of([]))
    );

    apiDataSessionId
    .subscribe(
      (sessionData => {
        if (sessionData == null) {
          console.log("--------------sessionData found null searchSessionId ::",sessionData);
          // throw new Error("Invalid data");
          return throwError(-1);
        } else {
          console.log("--------------sessionData searchSessionId ::",sessionData);
          console.log("--------------sessionData searchSessionId ::",sessionData.id);
          this.session =sessionData;

          console.log("searchBlockId id",this.ts.worker.id,"  this.session?.id ", this.session?.id);
          const apiblkId = this.blockService?.findByBlockId(this.ts.worker.id,this.session?.id)
          .pipe(
            map((res: any) => {
              if ((res==null) || (res==undefined) ) {
                console.log('Error occurred.');
                // delay(100);
                throw new Error('Value expected!');
              }
              return res;
            }),
            retry(), 
            // delay(10000),
            catchError(() => of([]))
          );
          
      
          apiblkId
            .subscribe(
              data => {
                if (data === null || data === undefined   ) {
                  console.log("--------------data found null searchCamLocaImageTypeBlockId ::",data);
                  console.log("--------------data found null searchCamLocaImageTypeBlockId ::",data.block_id);
                  // throw new Error("Invalid data");
                  return throwError(-1);
                } else {
                this.ret_val = -1;
                this.blocks = data;
                console.log("sectionComponent::searchBlockId blocks",this.blocks);
                 // facing white UV and LDF images
                // if( this.blocks != undefined && this.blocks[0] != undefined && this.blocks[0].id ) {
                  if(this.blocks?.id) {
                    console.log("sectionComponent::searchBlockId blocks",this.blocks);
                  console.log("sectionComponent::searchBlockId blocks__id",this.blocks.id);
                  console.log("this.blocks[0].id ", this.blocks.id)
                  console.log("this.ts.currentSectionWhtBlockDefault?.id ",this.ts.currentSectionWhtBlockDefault?.id)
                  // console.log("this.ts.currentSectionUVBlockDefault?.id ",this.ts.currentSectionUVBlockDefault?.id)
                  // console.log("this.ts.currentSectionLDFBlockDefault?.id ",this.ts.currentSectionLDFBlockDefault?.id)
                  this.searchBlock(Number(this.ts.worker.cameraLocation),this.blocks.id,0, Number(this.ts.worker.cutIndex));
                  // while(this.ret_val == -1) {
                  // this.searchSessionId(String(session_id));
                  if(this.session != undefined && this.session.id) {
                    this.ret_val = this.searchCamLocaImageTypeBlockId(Number(this.ts.worker.cameraLocation),0,this.blocks.id, Number(this.ts.worker.cutIndex), String( this.session.id), false);
                    this.ret_val = this.searchCamLocaImageTypeBlockId(Number(this.ts.worker.cameraLocation),0,this.blocks.id, 0, String( this.session.id), true);
                  }
                  //if(this.ret_val == -1) {
                    console.log("...............searchBlockId  :: error detected?",this.ret_val);
                  // }
                  //}
                  this.searchBlock(Number(this.ts.worker.cameraLocation), this.blocks.id,1, Number(this.ts.worker.cutIndex));
                  this.ret_val = -1;
      
                  // while(this.ret_val == -1) {
                  if(this.session != undefined && this.session.id) {
                  this.ret_val = this.searchCamLocaImageTypeBlockId(Number(this.ts.worker.cameraLocation),1,this.blocks.id, Number(this.ts.worker.cutIndex), String( this.session.id), false);
                  this.ret_val = this.searchCamLocaImageTypeBlockId(Number(this.ts.worker.cameraLocation),1,this.blocks.id, 0, String( this.session.id), true);
                  }
                  //if(this.ret_val == -1) {
                    console.log("...............searchBlockId  :: error detected?",this.ret_val);
                  // }
                  //}
                }
              }
              return ;
              },
              error => {
                console.log("..............searchBlockId  :: error detected!!!.....", error);
              });


          
        }
        return ;
      }
      ));
  }

  searchBlockId(camera_location: number,cut_index: number, session_id:string): void {
    // this.currentImage = undefined;
    // this.currentIndex = -1;
    
    this.searchSessionId(this.ts.worker.session_id);
    console.log("searchBlockId id",this.ts.worker.id,"  this.session?.id ", this.session?.id);
    if(this.session != null && this.session.id != null) {
    // const apiblkId = this.blockService.findByBlockId(this.ts.worker.id,this.session.id);
    // const apiblkId = this.blockService?.findByBlockId(this.ts.worker.id,this.session?.id)
    // .pipe(
    //   map((res: any) => {
    //     if ((res==null) || (res==undefined) ) {
    //       console.log('Error occurred.');
    //       // delay(100);
    //       throw new Error('Value expected!');
    //     }
    //     return res;
    //   }),
    //   retry(), // Retry forever
    //   // delay(10000),
    //   catchError(() => of([]))
    // );
    

    // apiblkId
    //   .subscribe(
    //     data => {
    //       if (data === null || data === undefined || || data[0] == null || data[0] == undefined ||data[0].id === undefined) {
    //         console.log("--------------data found null searchCamLocaImageTypeBlockId ::",data);
    //         // throw new Error("Invalid data");
    //         return throwError(-1);
    //       } else {
    //       this.ret_val = -1;
    //       this.blocks = data;
    //       console.log("sectionComponent::searchBlockId blocks",this.blocks);
    //        // facing white UV and LDF images
    //       // if( this.blocks != undefined && this.blocks[0] != undefined && this.blocks[0].id ) {
    //         if(this.blocks[0].id) {
    //           console.log("sectionComponent::searchBlockId blocks",this.blocks);
    //         console.log("sectionComponent::searchBlockId blocks__id",this.blocks[0].id);
    //         console.log("this.blocks[0].id ", this.blocks[0].id)
    //         console.log("this.ts.currentSectionWhtBlockDefault?.id ",this.ts.currentSectionWhtBlockDefault?.id)
    //         // console.log("this.ts.currentSectionUVBlockDefault?.id ",this.ts.currentSectionUVBlockDefault?.id)
    //         // console.log("this.ts.currentSectionLDFBlockDefault?.id ",this.ts.currentSectionLDFBlockDefault?.id)
    //         this.searchBlock(Number(camera_location),this.blocks[0].id,0, Number(cut_index));
    //         // while(this.ret_val == -1) {
    //         // this.searchSessionId(String(session_id));
    //         if(this.session != undefined && this.session.id) {
    //         this.ret_val = this.searchCamLocaImageTypeBlockId(Number(camera_location),0,this.blocks[0].id, Number(cut_index), String( this.session.id));
    //         }
    //         //if(this.ret_val == -1) {
    //           console.log("...............searchBlockId  :: error detected?",this.ret_val);
    //         // }
    //         //}
    //         this.searchBlock(Number(camera_location), this.blocks[0].id,1, Number(cut_index));
    //         this.ret_val = -1;

    //         // while(this.ret_val == -1) {
    //         this.ret_val = this.searchCamLocaImageTypeBlockId(Number(camera_location),1,this.blocks[0].id, Number(cut_index), String(session_id));
    //         //if(this.ret_val == -1) {
    //           console.log("...............searchBlockId  :: error detected?",this.ret_val);
    //         // }
    //         //}
    //       }
    //     }
    //     return ;
    //     },
    //     error => {
    //       console.log("..............searchBlockId  :: error detected!!!.....", error);
    //     });
      }
  }


}
