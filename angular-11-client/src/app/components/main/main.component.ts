import { Component, OnInit } from '@angular/core';
import { WorkerService } from 'src/app/services/worker.service';
import { Worker } from "src/app/models/worker.model"
import { Block } from 'src/app/models/block.model';
import { Image } from 'src/app/models/image.model';
import { BlockService } from 'src/app/services/block.service';
import { ImageService } from 'src/app/services/image.service';
import { BlocksListComponent } from 'src/app/components/blocks-list/blocks-list.component';
import{ GlobalConstants } from 'src/app/common/global-constants';
import { SessionService } from 'src/app/services/session.service';
import { catchError, delay, map, retry } from 'rxjs/operators';
import { of, throwError } from 'rxjs';
import { Session } from 'src/app/models/session.model';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
 
})
export class MainComponent implements OnInit {
  worker= this.ts.worker;
  // worker?: Worker[];
  // worker: Worker = {
  //   id: '...wait to receive information from the queue...',
  //   cameraLocation: '0'
  //     };
  blockList?:BlocksListComponent;
  // ={
  //   currentFacingWhtImageDefault.download_path = '0/0',
  // }; 
  blocks?: Block;
  images?: Image[];
  session?: Session = {
    id : null,
    session_id : ''

  };
  show = false;  
  refCutIndex=0;
  ret_val = 0;
  // currentImageDefault?: Image;
  // currentFacingWhtBlockDefault?: Block;
  // currentFacingWhtImageDefault?: Image;
  // currentFacingWhtImagemicrons=0;
  // currentFacingUVBlockDefault?: Block;
  // currentFacingUVImageDefault?: Image;
  // currentFacingUVImagemicrons=0;
  // currentFacingLDFBlockDefault?: Block;
  // currentFacingLDFImageDefault?: Image;
  // currentFacingLDFImagemicrons=0;

  // firstImageDefault?: Image;
  // firstFacingWhtImagemicrons = 0;
  // firstFacingWhtBlockDefault?: Block;
  // firstFacingWhtImageDefault?: Image;
  // firstFacingUVImagemicrons = 0;
  // firstFacingUVBlockDefault?: Block;
  // firstFacingUVImageDefault?: Image;
  // firstFacingLDFImagemicrons = 0;
  // firstFacingLDFBlockDefault?: Block;
  // firstFacingLDFImageDefault?: Image;

  // currentSectionWhtBlockDefault?: Block;
  // currentSectionWhtImageDefault?: Image;
  // currentSectionWhtImagemicrons=0;
  // currentSectionUVBlockDefault?: Block;
  // currentSectionUVImageDefault?: Image;
  // currentSectionUVImagemicrons=0;

  // currentTapeWhtBlockDefault?: Block;
  // currentTapeWhtImageDefault?: Image;
  // currentTapeWhtImagemicrons=0;
  // currentTapeUVBlockDefault?: Block;
  // currentTapeUVImageDefault?: Image;
  // currentTapeUVImagemicrons=0;

  // currentSlideWhtBlockDefault?: Block;
  // currentSlideWhtImageDefault?: Image;
  // currentSlideWhtImagemicrons=0;
  // currentSlideUVBlockDefault?: Block;
  // currentSlideUVImageDefault?: Image;
  // currentSlideUVImagemicrons=0;

  title = 'DecisionMaker';
  constructor(private workerService: WorkerService,private blockService: BlockService, private sessionService: SessionService, private imageService: ImageService, public ts: GlobalConstants) { }

  ngOnInit(): void {
    console.log("main::workerStarted ",this.ts.workerStarted);
    if(this.ts.workerStarted===false) {
      this.ts.workerStarted = true;
      this.retrieveDefault();
    }
    // facing white UV and LDF images
    this.searchCameraLocation(0,0);
    this.searchCameraLocation(0,1);
    this.searchCameraLocation(0,2);
    // sectioning White and UV images
    this.searchCameraLocation(1,0);
    this.searchCameraLocation(1,1);
    // Tape White and UV images
    this.searchCameraLocation(2,0);
    this.searchCameraLocation(2,1);
    // Slide White and UV images
    this.searchCameraLocation(3,0);
    this.searchCameraLocation(3,1);

  }
  retrieveDefault(): void{
    // while(true) {
      this.workerService.get()
      .subscribe(
        data => {
          this.ts.worker = data;
          console.log("main:: retrieveDefault :: data ",data);
          // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
          this.searchBlockId(this.ts.worker.cameraLocation, this.ts.worker.cutIndex, this.ts.worker.session_id);
          this.ts.workerStarted = true;
          console.log(data);
        },
        error => {
          console.log(error);
        });   
    //     delay(1000);
    // } 
  }

  clickout() {
    this.show = true;
  }
 
  clickit(){
    this.show = !this.show;
  }

  startBlockProcess(): void{
    this.workerService.getStartBlockProcess()
    .subscribe(
      data => {
        // this.workerService.get()
        // .subscribe(
        //   data => {
        //     this.ts.worker = data;
        //     console.log("main:: retrieveDefault :: data ",data);
        //     // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
        //     this.searchBlockId(this.ts.worker.cameraLocation, this.ts.worker.cutIndex, this.ts.worker.session_id);
        //     this.ts.workerStarted = true;
        //     console.log(data);
        //   },
        //   error => {
        //     console.log(error);
        //   });
        console.log(data);
      },
      error => {
        console.log(error);
      });    
      
      if(this.blockList!=undefined)
        this.blockList.block_id=this.worker.id;
        this.blockList?.searchTitle();

  }

  runBlockProcess(): void{
    this.workerService.getRunBlockProcess()
    .subscribe(
      data => {
        
        console.log(data);
      },
      error => {
        console.log(error);
      });    
      
      if(this.blockList!=undefined)
        this.blockList.block_id=this.worker.id;
        this.blockList?.searchTitle();

  }

  stopBlockProcess(): void{
    this.workerService.getStopBlockProcess()
    .subscribe(
      data => {
        
        console.log(data);
      },
      error => {
        console.log(error);
      });    
      
      if(this.blockList!=undefined)
        this.blockList.block_id=this.worker.id;
        this.blockList?.searchTitle();

  }

  searchBlock(camera_location: number, blk_id: string, image_type: number, cutIndex: number): void {
    this.blockService.get(blk_id)
      .subscribe(
        data => {
          switch(camera_location)
          {
            case 0:
              if(cutIndex==0) {
                switch(image_type)
                {
                  case 0: this.ts.firstFacingWhtBlockDefault = data; break;
                  case 1: this.ts.firstFacingUVBlockDefault = data; break;
                  case 2: this.ts.firstFacingLDFBlockDefault = data; break;
                }
              } else {
                switch(image_type)
                {
                  case 0: this.ts.currentFacingWhtBlockDefault = data; break;
                  case 1: this.ts.currentFacingUVBlockDefault = data; break;
                  case 2: this.ts.currentFacingLDFBlockDefault = data; break;
                }
              }
              break;
              case 1:
              // if(cutIndex==0){
              //   switch(image_type)
              //   {
              //     case 0: this.ts.firstSectionWhtBlockDefault = data; break;
              //     case 1: this.ts.firstSectionUVBlockDefault = data; break;
              //   }
              // } else {
                switch(image_type)
                {
                  case 0: this.ts.currentSectionWhtBlockDefault = data; break;
                  case 1: this.ts.currentSectionUVBlockDefault = data; break;
                }
              // }
              break;
              case 2:
              // if(cutIndex==0){
              //   switch(image_type)
              //   {
              //     case 0: this.ts.firstTapeWhtBlockDefault = data; break;
              //     case 1: this.ts.firstTapeUVBlockDefault = data; break;
              //   }
              // } else {
                switch(image_type)
                {
                  case 0: this.ts.currentTapeWhtBlockDefault = data; break;
                  case 1: this.ts.currentTapeUVBlockDefault = data; break;
                }
              // }
              break;
              case 3:
              // if(cutIndex==0){
              //   switch(image_type)
              //   {
              //     case 0: this.ts.firstSlideWhtBlockDefault = data; break;
              //     case 1: this.ts.firstSlideUVBlockDefault = data; break;
              //   }
              // } else {
                switch(image_type)
                {
                  case 0: this.ts.currentSlideWhtBlockDefault = data; break;
                  case 1: this.ts.currentSlideUVBlockDefault = data; break;
                }
              // }
              break;
            }
              
          
          console.log("facingComponent::searchBlock",data);
        },
        error => {
          console.log(error);
        });
  }

  searchCameraLocation(cameraLocation: number, image_type: number): void {

    this.imageService.findByCameraLocationImageType(cameraLocation, image_type)
      .subscribe(
        data => {
          // this.ts.currentFacingWhtImageDefault = data;
          switch(cameraLocation) 
          {
            case 0:
              switch(image_type)
              {
                case 0: this.ts.currentFacingWhtImageDefault = data; 
                if(this.ts.currentFacingWhtImageDefault?.blocks__id !== undefined) {
                  if(Number(this.ts.currentFacingWhtImageDefault?.cut_index) >= 0) {          // if(Number(this.ts.currentFacingLDFImageDefault?.cut_index) >= 0) {
                    this.refCutIndex = Number(this.ts.currentFacingWhtImageDefault?.cut_index)+1;
                    this.ts.currentFacingWhtImageDefault.cut_index = String(this.refCutIndex);
                    this.ts.currentFacingWhtImagemicrons = (Number(this.ts.currentFacingWhtImageDefault.cut_index) - 1) * 20;
                  }
                  // this.searchCamLocaImageTypeBlockId(cameraLocation, image_type, this.ts.currentFacingWhtImageDefault.blocks__id, 0)
                  this.searchBlock(cameraLocation,this.ts.currentFacingWhtImageDefault.blocks__id, image_type, Number(this.ts.currentFacingWhtImageDefault.cut_index)-1);
                }
                break;
                case 1: this.ts.currentFacingUVImageDefault = data;
                if(this.ts.currentFacingUVImageDefault?.blocks__id !== undefined) {
                  if(Number(this.ts.currentFacingUVImageDefault?.cut_index) >= 0) {
                    this.refCutIndex = Number(this.ts.currentFacingUVImageDefault?.cut_index)+1;
                    this.ts.currentFacingUVImageDefault.cut_index = String(this.refCutIndex);
                    this.ts.currentFacingUVImagemicrons = (Number(this.ts.currentFacingUVImageDefault.cut_index) - 1) * 20;
                  }
                  // this.searchCamLocaImageTypeBlockId(cameraLocation, image_type, this.ts.currentFacingWhtImageDefault.blocks__id, 0)
                  this.searchBlock(cameraLocation,this.ts.currentFacingUVImageDefault.blocks__id, image_type, Number(this.ts.currentFacingUVImageDefault.cut_index)-1);
                }
                break;
                case 2: this.ts.currentFacingLDFImageDefault = data; 
                if(this.ts.currentFacingLDFImageDefault?.blocks__id !== undefined) {
                  if(Number(this.ts.currentFacingLDFImageDefault?.cut_index) >= 0) {
                    this.refCutIndex = Number(this.ts.currentFacingLDFImageDefault?.cut_index)+1;
                    this.ts.currentFacingLDFImageDefault.cut_index = String(this.refCutIndex);
                    this.ts.currentFacingLDFImagemicrons = (Number(this.ts.currentFacingLDFImageDefault.cut_index) - 1) * 20;
                  }
                  // this.searchCamLocaImageTypeBlockId(cameraLocation, image_type, this.ts.currentFacingWhtImageDefault.blocks__id, 0)
                  this.searchBlock(cameraLocation,this.ts.currentFacingLDFImageDefault.blocks__id, image_type, Number(this.ts.currentFacingLDFImageDefault.cut_index)-1);
                }
                break;
              }
              break;
            case 1:
                switch(image_type)
                {
                  case 0: this.ts.currentSectionWhtImageDefault = data; 
                  if(this.ts.currentSectionWhtImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentSectionWhtImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentSectionWhtImageDefault?.cut_index)+1;
                      this.ts.currentSectionWhtImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentSectionWhtImagemicrons = (Number(this.ts.currentSectionWhtImageDefault.cut_index) - 1) * 4;
                    }
                    // this.searchCamLocaImageTypeBlockId(cameraLocation, image_type, this.ts.currentFacingWhtImageDefault.blocks__id, 0)
                    this.searchBlock(cameraLocation,this.ts.currentSectionWhtImageDefault.blocks__id, image_type, Number(this.ts.currentSectionWhtImageDefault.cut_index)-1);
                  }
                  break;
                  case 1: this.ts.currentSectionUVImageDefault = data;
                  if(this.ts.currentSectionUVImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentSectionUVImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentSectionUVImageDefault?.cut_index)+1;
                      this.ts.currentSectionUVImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentSectionUVImagemicrons = (Number(this.ts.currentSectionUVImageDefault.cut_index) - 1) * 4;
                    }
                    // this.searchCamLocaImageTypeBlockId(cameraLocation, image_type, this.ts.currentFacingWhtImageDefault.blocks__id, 0)
                    this.searchBlock(cameraLocation,this.ts.currentSectionUVImageDefault.blocks__id, image_type, Number(this.ts.currentSectionUVImageDefault.cut_index)-1);
                  }
                  break;
                } 
                break;
            case 2:
              switch(image_type)
                {
                  case 0: this.ts.currentTapeWhtImageDefault = data; 
                  if(this.ts.currentTapeWhtImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentTapeWhtImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentTapeWhtImageDefault?.cut_index)+1;
                      this.ts.currentTapeWhtImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentTapeWhtImagemicrons = (Number(this.ts.currentTapeWhtImageDefault.cut_index) - 1) * 4;
                    }
                    // this.searchCamLocaImageTypeBlockId(cameraLocation, image_type, this.ts.currentFacingWhtImageDefault.blocks__id, 0)
                    this.searchBlock(cameraLocation,this.ts.currentTapeWhtImageDefault.blocks__id, image_type, Number(this.ts.currentTapeWhtImageDefault.cut_index)-1);
                  }
                  break;
                  case 1: this.ts.currentTapeUVImageDefault = data;
                  if(this.ts.currentTapeUVImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentTapeUVImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentTapeUVImageDefault?.cut_index)+1;
                      this.ts.currentTapeUVImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentTapeUVImagemicrons = (Number(this.ts.currentTapeUVImageDefault.cut_index) - 1) * 4;
                    }
                    // this.searchCamLocaImageTypeBlockId(cameraLocation, image_type, this.ts.currentFacingWhtImageDefault.blocks__id, 0)
                    this.searchBlock(cameraLocation,this.ts.currentTapeUVImageDefault.blocks__id, image_type, Number(this.ts.currentTapeUVImageDefault.cut_index)-1);
                  }
                  break;
                } 
                break;
            case 3:
              switch(image_type)
                {
                  case 0: this.ts.currentSlideWhtImageDefault = data; 
                  if(this.ts.currentSlideWhtImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentSlideWhtImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentTapeWhtImageDefault?.cut_index)+1;
                      this.ts.currentSlideWhtImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentSlideWhtImagemicrons = (Number(this.ts.currentSlideWhtImageDefault.cut_index) - 1) * 4;
                    }
                    // this.searchCamLocaImageTypeBlockId(cameraLocation, image_type, this.ts.currentFacingWhtImageDefault.blocks__id, 0)
                    this.searchBlock(cameraLocation,this.ts.currentSlideWhtImageDefault.blocks__id, image_type, Number(this.ts.currentSlideWhtImageDefault.cut_index)-1);
                  }
                  break;
                  case 1: this.ts.currentSlideUVImageDefault = data;
                  if(this.ts.currentSlideUVImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentSlideUVImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentSlideUVImageDefault?.cut_index)+1;
                      this.ts.currentSlideUVImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentSlideUVImagemicrons = (Number(this.ts.currentSlideUVImageDefault.cut_index) - 1) * 4;
                    }
                    // this.searchCamLocaImageTypeBlockId(cameraLocation, image_type, this.ts.currentFacingWhtImageDefault.blocks__id, 0)
                    this.searchBlock(cameraLocation,this.ts.currentSlideUVImageDefault.blocks__id, image_type, Number(this.ts.currentSlideUVImageDefault.cut_index)-1);
                  }
                  break;
                } 
                break;
          }
          console.log(data);          
        },
        error => {
          console.log(error);
        });
  }

  searchAllLdf( activateFlag: boolean): void {
    console.log("String(this.ts.currentFacingWhtImageDefault?.sessions__id), Number(this.ts.currentFacingWhtImageDefault?.cut_index)-1",
    String(this.ts.currentFacingWhtImageDefault?.sessions__id), Number(this.ts.currentFacingWhtImageDefault?.cut_index)-1)
    const allLdfData= this.imageService.findAllLdf(String(this.ts.currentFacingWhtImageDefault?.sessions__id), Number(this.ts.currentFacingWhtImageDefault?.cut_index)-1)
    .pipe(
     map((res: any) => {
       if ((res==null) || (res==undefined) ) {
         console.log('searchAllLdf::Error occurred.');
         // delay(100);
         throw new Error('searchAllLdf::Value expected!');
       }
       return res;
     }),
     retry(), // Retry forever
     // delay(10000),
     catchError(() => of([]))
   );
   
   allLdfData.subscribe(
         data => {
           if (data == null || data.length < 6) {
             console.log("--------------allLdfData found null searchAllLdf ::",data);
             console.log("--------------allLdfData data.length::",data.length);
             // throw new Error("Invalid data");
             return throwError(-1);
           } else {
           
             console.log("--------------allLdfData data::",data);
             console.log("--------------allLdfData data.length::",data.length);
           if(activateFlag) {
             this.ts.activeProject(String(this.ts.worker.cameraLocation), true, 3);
           }
           console.log("facingComponent::searchAllLdf: ",data);  
           this.ts.currentFacingLDF1Image = data[0]; 
           this.ts.currentFacingLDF2Image = data[1];
           this.ts.currentFacingLDF3Image = data[2]; 
           this.ts.currentFacingLDF4Image = data[3]; 
           this.ts.currentFacingLDF5Image = data[4]; 
           this.ts.currentFacingLDF6Image = data[5]; 
           }
           return ;
         },
         error => {
           console.log(error);
         });
   }

  searchCamLocaImageTypeBlockId(cameraLocation: number, image_type: number, block_id: string, cut_index: number, sessionId: string, activateFlag: boolean): number {
    const apiData = this.imageService.findByCamLocImageTypeBlockId(cameraLocation, image_type, block_id, cut_index, sessionId)
    .pipe(
      map((res: any) => {
        if ((res==null) || (res==undefined) || (sessionId == null) && (cameraLocation == 1)  ) {
          console.log('Error occurred.');
          // delay(100);
          throw new Error('Value expected!');
        }
        return res;
      }),
      retry(), // Retry forever
      // delay(10000),
      catchError(() => of([]))
    );

    
    
    apiData
      .subscribe(
        (data => {
          //throw error for demonstration
          if (data == null) {
            console.log("--------------found null searchCamLocaImageTypeBlockId ::",data);
            // throw new Error("Invalid data");
            return throwError(-1);
          } else {
            console.log("--------------activate menu flag searchCamLocalImageTypeBlockId ");
            if(!activateFlag) {
              if(Number(this.ts.worker.cameraLocation) >= 0) {
                this.ts.activeProject(String(this.ts.worker.cameraLocation), true, image_type);
              }
            }
          switch(cameraLocation)
          {
            case 0:
            // this.ts.currentFacingWhtImageDefault = data;
            switch(image_type)
            {
              case 0: 
                if(activateFlag) {
                  this.ts.firstFacingWhtImageDefault = data; 
                  if(this.ts.firstFacingWhtImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.firstFacingWhtImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.firstFacingWhtImageDefault?.cut_index)+1;
                      this.ts.firstFacingWhtImageDefault.cut_index = String(this.refCutIndex);
                      console.log(" this.ts.firstFacingWhtImageDefault.cut_index ",this.ts.firstFacingWhtImageDefault.cut_index);
                      this.ts.firstFacingWhtImagemicrons = (Number(this.ts.firstFacingWhtImageDefault.cut_index) - 1) * 20;
                    }
                  }
                } else {
                  this.ts.activeProject(String(this.ts.worker.cameraLocation), true, image_type);
                  this.ts.currentFacingWhtImageDefault = data; 
                  if(this.ts.currentFacingWhtImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentFacingWhtImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentFacingWhtImageDefault.cut_index)+1;
                      this.ts.currentFacingWhtImageDefault.cut_index = String(this.refCutIndex);
                      console.log(" this.ts.currentFacingWhtImageDefault.cut_index ",this.ts.currentFacingWhtImageDefault.cut_index);
                      this.ts.currentFacingWhtImagemicrons = (Number(this.ts.currentFacingWhtImageDefault.cut_index) - 1) * 20;
                    }
                    // this.searchBlock(cameraLocation,this.ts.currentFacingWhtImageDefault.blocks__id, image_type);
                  }
                }
                console.log("searchCamLocaImageTypeBlockId ",data);    
                break;
              case 1: 
                if(activateFlag) {
                  this.ts.firstFacingUVImageDefault = data; 
                  if(this.ts.firstFacingUVImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.firstFacingUVImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.firstFacingUVImageDefault.cut_index)+1;
                      this.ts.firstFacingUVImageDefault.cut_index = String(this.refCutIndex);
                      console.log(" this.ts.firstFacingUVImageDefault.cut_index ",this.ts.firstFacingUVImageDefault.cut_index);
                      this.ts.firstFacingUVImagemicrons = (Number(this.ts.firstFacingUVImageDefault.cut_index) - 1) * 20;
                    }
                    // this.searchBlock(cameraLocation,this.ts.currentFacingUVImageDefault.blocks__id, image_type);
                  }
                } else {
                  // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
                  if( image_type === 1) {
                    this.ts.activeProject(String(this.ts.worker.cameraLocation), true, 2);
                  } else {
                    this.ts.activeProject(String(this.ts.worker.cameraLocation), true, image_type);
                  }
                  this.ts.currentFacingUVImageDefault = data; 
                  if(this.ts.currentFacingUVImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentFacingUVImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentFacingUVImageDefault.cut_index)+1;
                      this.ts.currentFacingUVImageDefault.cut_index = String(this.refCutIndex);
                      console.log(" this.ts.currentFacingUVImageDefault.cut_index ",this.ts.currentFacingUVImageDefault.cut_index);
                      this.ts.currentFacingUVImagemicrons = (Number(this.ts.currentFacingUVImageDefault.cut_index) - 1) * 20;
                    }
                    // this.searchBlock(cameraLocation,this.ts.currentFacingUVImageDefault.blocks__id, image_type);
                  }
                }
                console.log("searchCamLocaImageTypeBlockId ",data);    
                break;
              case 2:
                if(activateFlag) { 
                  this.ts.firstFacingLDFImageDefault = data; 
                  if(this.ts.firstFacingLDFImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.firstFacingLDFImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.firstFacingLDFImageDefault.cut_index)+1;
                      this.ts.firstFacingLDFImageDefault.cut_index = String(this.refCutIndex);
                      console.log(" this.ts.firstFacingLDFImageDefault.cut_index ",this.ts.firstFacingLDFImageDefault.cut_index);
                      this.ts.firstFacingLDFImagemicrons = (Number(this.ts.firstFacingLDFImageDefault.cut_index) - 1) * 20;
                    }
                    // this.searchBlock(cameraLocation, this.ts.currentFacingLDFImageDefault.blocks__id, image_type);
                  }
                } else  { 
                  this.ts.currentFacingLDFImageDefault = data; 
                  if(this.ts.currentFacingLDFImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentFacingLDFImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentFacingLDFImageDefault.cut_index)+1;
                      this.ts.currentFacingLDFImageDefault.cut_index = String(this.refCutIndex);
                      console.log(" this.ts.currentFacingLDFImageDefault.cut_index ",this.ts.currentFacingLDFImageDefault.cut_index);
                      this.ts.currentFacingLDFImagemicrons = (Number(this.ts.currentFacingLDFImageDefault.cut_index) - 1) * 20;
                    }
                    // this.searchBlock(cameraLocation, this.ts.currentFacingLDFImageDefault.blocks__id, image_type);
                    this.searchAllLdf(true);
                  }
                }
                console.log("searchCamLocaImageTypeBlockId ",data);    
                break;
            }
          break;
          case 1:
            switch(image_type)
            {
              case 0: 
                  this.ts.currentSectionWhtImageDefault = data; 
            
                  if(this.ts.currentSectionWhtImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentSectionWhtImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentSectionWhtImageDefault.cut_index)+1;
                      this.ts.currentSectionWhtImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentSectionWhtImagemicrons = (Number(this.ts.currentSectionWhtImageDefault.cut_index) - 1) * 4;
                    }
                  }
                console.log("searchCamLocaImageTypeBlockId ",data);    
                break;
              case 1: 
                  this.ts.currentSectionUVImageDefault = data; 
                  if(this.ts.currentSectionUVImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentSectionUVImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentSectionUVImageDefault.cut_index)+1;
                      this.ts.currentSectionUVImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentSectionUVImagemicrons = (Number(this.ts.currentSectionUVImageDefault.cut_index) - 1) * 4;
                    }
                  }
                console.log("searchCamLocaImageTypeBlockId ",data);    
                break;
                
            }
          break;
          case 2:
            // this.ts.currentTapeWhtImageDefault = data;
            switch(image_type)
            {
              case 0: 
                  this.ts.currentTapeWhtImageDefault = data; 
            
                  if(this.ts.currentTapeWhtImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentTapeWhtImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentTapeWhtImageDefault.cut_index)+1;
                      this.ts.currentTapeWhtImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentTapeWhtImagemicrons = (Number(this.ts.currentTapeWhtImageDefault.cut_index) - 1) * 4;
                    }
                  }
                console.log("searchCamLocaImageTypeBlockId ",data);    
                break;
              case 1: 
                  this.ts.currentTapeUVImageDefault = data; 
                  if(this.ts.currentTapeUVImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentTapeUVImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentTapeUVImageDefault.cut_index)+1;
                      this.ts.currentTapeUVImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentTapeUVImagemicrons = (Number(this.ts.currentTapeUVImageDefault.cut_index) - 1) * 4;
                    }
                  }
                console.log("searchCamLocaImageTypeBlockId ",data);    
                break;
            }
          break;
          }
        }
          return 0;
        } ),
        
        error => {
          console.log("searchCamLocaImageTypeBlockId :: error",error);
          return 0;
        });
        return 0;
  }


  //  we have session_id in session table find the id 
  searchSessionId(sessionId: String): void {
    // this.session = null;
    const apiDataSessionId = this.sessionService.findBySessionId(sessionId) // findByCamLocImageTypeBlockId(cameraLocation, image_type, block_id, cut_index, sessionId)
    .pipe(
      map((res: any) => {
        if ((res==null) || (res==undefined) ) {
          console.log('Error occurred.');
          // delay(100);
          throw new Error('Value expected!');
        }
        return res;
      }),
      retry(), // Retry forever
      // delay(10000),
      catchError(() => of([]))
    );

    apiDataSessionId
    .subscribe(
      (sessionData => {
        if (sessionData == null) {
          console.log("--------------sessionData found null searchSessionId ::",sessionData);
          // throw new Error("Invalid data");
          return throwError(-1);
        } else {
          console.log("--------------sessionData searchSessionId ::",sessionData);
          console.log("--------------sessionData searchSessionId ::",sessionData.id);
          this.session =sessionData;

          console.log("searchBlockId id",this.ts.worker.id,"  this.session?.id ", this.session?.id);
          const apiblkId = this.blockService?.findByBlockId(this.ts.worker.id,this.session?.id)
          .pipe(
            map((res: any) => {
              if ((res==null) || (res==undefined) ) {
                console.log('Error occurred.');
                // delay(100);
                throw new Error('Value expected!');
              }
              return res;
            }),
            retry(), 
            // delay(10000),
            catchError(() => of([]))
          );
          
      
          apiblkId
            .subscribe(
              data => {
                if (data === null || data === undefined   ) {
                  console.log("--------------data found null searchCamLocaImageTypeBlockId ::",data);
                  console.log("--------------data found null searchCamLocaImageTypeBlockId ::",data.block_id);
                  // throw new Error("Invalid data");
                  return throwError(-1);
                } else {
                this.ret_val = -1;
                this.blocks = data;
                console.log("sectionComponent::searchBlockId blocks",this.blocks);
                 // facing white UV and LDF images
                // if( this.blocks != undefined && this.blocks[0] != undefined && this.blocks[0].id ) {
                  if(this.blocks?.id) {
                    console.log("sectionComponent::searchBlockId blocks",this.blocks);
                  console.log("sectionComponent::searchBlockId blocks__id",this.blocks.id);
                  console.log("this.blocks[0].id ", this.blocks.id)
                  console.log("this.ts.currentSectionWhtBlockDefault?.id ",this.ts.currentSectionWhtBlockDefault?.id)
                  // console.log("this.ts.currentSectionUVBlockDefault?.id ",this.ts.currentSectionUVBlockDefault?.id)
                  // console.log("this.ts.currentSectionLDFBlockDefault?.id ",this.ts.currentSectionLDFBlockDefault?.id)
                  this.searchBlock(Number(this.ts.worker.cameraLocation),this.blocks.id,0, Number(this.ts.worker.cutIndex));
                  // while(this.ret_val == -1) {
                  // this.searchSessionId(String(session_id));
                  if(this.session != undefined && this.session.id) {
                  this.ret_val = this.searchCamLocaImageTypeBlockId(Number(this.ts.worker.cameraLocation),0,this.blocks.id, Number(this.ts.worker.cutIndex), String( this.session.id), false);
                  }
                  //if(this.ret_val == -1) {
                    console.log("...............searchBlockId  :: error detected?",this.ret_val);
                  // }
                  //}
                  this.searchBlock(Number(this.ts.worker.cameraLocation), this.blocks.id,1, Number(this.ts.worker.cutIndex));
                  this.ret_val = -1;
      
                  // while(this.ret_val == -1) {
                  if(this.session != undefined && this.session.id) {
                  this.ret_val = this.searchCamLocaImageTypeBlockId(Number(this.ts.worker.cameraLocation),1,this.blocks.id, Number(this.ts.worker.cutIndex), String( this.session.id), false);
                  }
                  //if(this.ret_val == -1) {
                    console.log("...............searchBlockId  :: error detected?",this.ret_val);

                    if(Number(this.ts.worker.cameraLocation) === 0) {
                      this.searchBlock(Number(this.ts.worker.cameraLocation), this.blocks.id,2, Number(this.ts.worker.cutIndex));
                      if(this.session != undefined && this.session.id) {
                      this.ret_val = this.searchCamLocaImageTypeBlockId(Number(this.ts.worker.cameraLocation),2,this.blocks.id, Number(this.ts.worker.cutIndex), String( this.session.id), false);
                      // this.searchAllLdf(true);
                      }
                      console.log("...............searchBlockId  :: error detected?");
                    }
                  // }
                  //}
                }
               
              }
              return ;
              },
              error => {
                console.log("..............searchBlockId  :: error detected!!!.....", error);
              });


          
        }
        return ;
      }
      ));
  }

  searchBlockId(camera_location: number,cut_index: number, session_id:string): void {
    // this.currentImage = undefined;
    // this.currentIndex = -1;
    
    this.searchSessionId(this.ts.worker.session_id);
    console.log("searchBlockId id",this.ts.worker.id,"  this.session?.id ", this.session?.id);
    if(this.session != null && this.session.id != null) {
    // const apiblkId = this.blockService.findByBlockId(this.ts.worker.id,this.session.id);
    // const apiblkId = this.blockService?.findByBlockId(this.ts.worker.id,this.session?.id)
    // .pipe(
    //   map((res: any) => {
    //     if ((res==null) || (res==undefined) ) {
    //       console.log('Error occurred.');
    //       // delay(100);
    //       throw new Error('Value expected!');
    //     }
    //     return res;
    //   }),
    //   retry(), // Retry forever
    //   // delay(10000),
    //   catchError(() => of([]))
    // );
    

    // apiblkId
    //   .subscribe(
    //     data => {
    //       if (data === null || data === undefined || || data[0] == null || data[0] == undefined ||data[0].id === undefined) {
    //         console.log("--------------data found null searchCamLocaImageTypeBlockId ::",data);
    //         // throw new Error("Invalid data");
    //         return throwError(-1);
    //       } else {
    //       this.ret_val = -1;
    //       this.blocks = data;
    //       console.log("sectionComponent::searchBlockId blocks",this.blocks);
    //        // facing white UV and LDF images
    //       // if( this.blocks != undefined && this.blocks[0] != undefined && this.blocks[0].id ) {
    //         if(this.blocks[0].id) {
    //           console.log("sectionComponent::searchBlockId blocks",this.blocks);
    //         console.log("sectionComponent::searchBlockId blocks__id",this.blocks[0].id);
    //         console.log("this.blocks[0].id ", this.blocks[0].id)
    //         console.log("this.ts.currentSectionWhtBlockDefault?.id ",this.ts.currentSectionWhtBlockDefault?.id)
    //         // console.log("this.ts.currentSectionUVBlockDefault?.id ",this.ts.currentSectionUVBlockDefault?.id)
    //         // console.log("this.ts.currentSectionLDFBlockDefault?.id ",this.ts.currentSectionLDFBlockDefault?.id)
    //         this.searchBlock(Number(camera_location),this.blocks[0].id,0, Number(cut_index));
    //         // while(this.ret_val == -1) {
    //         // this.searchSessionId(String(session_id));
    //         if(this.session != undefined && this.session.id) {
    //         this.ret_val = this.searchCamLocaImageTypeBlockId(Number(camera_location),0,this.blocks[0].id, Number(cut_index), String( this.session.id));
    //         }
    //         //if(this.ret_val == -1) {
    //           console.log("...............searchBlockId  :: error detected?",this.ret_val);
    //         // }
    //         //}
    //         this.searchBlock(Number(camera_location), this.blocks[0].id,1, Number(cut_index));
    //         this.ret_val = -1;

    //         // while(this.ret_val == -1) {
    //         this.ret_val = this.searchCamLocaImageTypeBlockId(Number(camera_location),1,this.blocks[0].id, Number(cut_index), String(session_id));
    //         //if(this.ret_val == -1) {
    //           console.log("...............searchBlockId  :: error detected?",this.ret_val);
    //         // }
    //         //}
    //       }
    //     }
    //     return ;
    //     },
    //     error => {
    //       console.log("..............searchBlockId  :: error detected!!!.....", error);
    //     });
      }
  }


}
