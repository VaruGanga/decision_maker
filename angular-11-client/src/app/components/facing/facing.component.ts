import { Component, OnInit } from '@angular/core';
import { WorkerService } from 'src/app/services/worker.service';
import { Worker } from "src/app/models/worker.model"
import { Block } from 'src/app/models/block.model';
import { Image } from 'src/app/models/image.model';
import { BlockService } from 'src/app/services/block.service';
import { ImageService } from 'src/app/services/image.service';
import { BlocksListComponent } from 'src/app/components/blocks-list/blocks-list.component';
import{ GlobalConstants } from 'src/app/common/global-constants';
import { Session } from 'src/app/models/session.model';
import { SessionService } from 'src/app/services/session.service';
import { catchError, map, retry } from 'rxjs/operators';
import { of, throwError } from 'rxjs';
interface facingPercent {
  value: string;
  viewValue: number;
}

@Component({
  selector: 'app-facing',
  templateUrl: './facing.component.html',
  styleUrls: ['./facing.component.css']
})
export class FacingComponent implements OnInit {
  message = '';
  selectedfacingPercent: string | undefined;
  facingPercents: facingPercent[] = [
    {value: '10%' ,viewValue:10},
    {value: '20%' ,viewValue:20},
    {value: '30%' ,viewValue:30},
    {value: '40%',viewValue:40},
    {value: '50%',viewValue:50},
    {value: '60%',viewValue:60},
    {value: '70%',viewValue:70},
    {value: '80%',viewValue:80},
    {value: '90%',viewValue:90},
    {value: '100%',viewValue:100},
  ];

  userComment : string | undefined;

  worker = this.ts.worker;
  // worker?: Worker[];
  // worker: Worker = {
  //   id: '...wait to receive information from the queue...',
  //   cameraLocation: '0'
  //     };
  blockList?:BlocksListComponent;
  // ={
  //   currentFacingWhtImageDefault.download_path = '0/0',
  // };
  blocks?: Block;
  images?: Image[];
  session?: Session = {
    id : null,
    session_id : ''

  };
  show = false;
  refCutIndex = 0;
  ret_val = 0;
  // currentImageDefault?: Image;
  // currentFacingWhtImagemicrons = 0;
  // currentFacingWhtBlockDefault?: Block;
  // currentFacingWhtImageDefault?: Image;
  // currentFacingUVImagemicrons = 0;
  // currentFacingUVBlockDefault?: Block;
  // currentFacingUVImageDefault?: Image;
  // currentFacingLDFImagemicrons = 0;
  // currentFacingLDFBlockDefault?: Block;
  // currentFacingLDFImageDefault?: Image;

  // // refCutIndex = 0;
  // firstImageDefault?: Image;
  // firstFacingWhtImagemicrons = 0;
  // firstFacingWhtBlockDefault?: Block;
  // firstFacingWhtImageDefault?: Image;
  // firstFacingUVImagemicrons = 0;
  // firstFacingUVBlockDefault?: Block;
  // firstFacingUVImageDefault?: Image;
  // firstFacingLDFImagemicrons = 0;
  // firstFacingLDFBlockDefault?: Block;
  // firstFacingLDFImageDefault?: Image;
  constructor(private workerService: WorkerService,private blockService: BlockService, private imageService: ImageService, private sessionService: SessionService, public ts: GlobalConstants) { }

  ngOnInit(): void {
    console.log("facing::workerStarted ",this.ts.workerStarted);
    if(this.ts.workerStarted === false) {
      this.ts.workerStarted=true;
      this.retrieveDefault();
    }  
    // facing white UV and LDF images
    this.searchCameraLocation(0,0);
    this.searchCameraLocation(0,1);
    this.searchCameraLocation(0,2);
    // this.searchAllLdf(false);
  }
  retrieveDefault(): void{
    const workerApiData = this.workerService.get()
    .pipe(
      map((res: any) => {
        if ((res==null) || (res==undefined) || res.cameraLocation == null) {
          console.log('Error occurred.');
          // delay(100);
          throw new Error('Value expected!');
        }
        return res;
      }),
      retry(), // Retry forever
      // delay(10000),
      catchError(() => of([]))
    );

    workerApiData
    .subscribe(
      data => {
        this.ts.worker = data;
        // if(this.ts.worker.cameraLocation) {
          // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
          if(this.ts.worker.cameraLocation == "0" || this.ts.worker.cameraLocation == '0') {
            this.searchBlockId(this.ts.worker.cameraLocation, this.ts.worker.cutIndex , this.ts.worker.session_id);
          }
        // }
        console.log("facingComponent::retrieveDefault ",data);
      },
      error => {
        console.log(error);
      });    
           
  }

  clickout() {
    this.show = true;
  }
 
  clickit(){
    this.show = !this.show;
  }

  facingContinue(): void{
    this.ts.deactiveProject("0");
    this.workerService.getFacing()
    .subscribe(
      () => {
      this.workerService.get()
        .subscribe(
          data => {
            this.ts.worker = data;
            console.log("facing:: facingContinue :: data ",data);
            // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
            this.searchBlockId(this.ts.worker.cameraLocation, this.ts.worker.cutIndex, this.ts.worker.session_id);
            this.ts.workerStarted = true;
            console.log(data);
          },
          error => {
            console.log(error);
          });  
        });   
    // .subscribe(
    //   data => {
    //     // this.ts.worker = data;
    //     // //if(this.ts.worker.cameraLocation) {
    //     //   // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
    //     //   this.searchBlockId(this.ts.worker.cameraLocation, this.ts.worker.cutIndex, this.ts.worker.session_id);
    //     // //}
    //     // console.log(data);
    //   },
    //   error => {
    //     console.log(error);
    //   });    
      
      // if(this.blockList!=undefined)
      //   this.blockList.block_id=this.worker.id;
      //   this.blockList?.searchTitle();
  }

  facingCompleted(): void{
    this.ts.deactiveProject("0");
    this.workerService.getFacingCompleted()
    .subscribe(
      () => {
        this.workerService.get()
        .subscribe(
          data => {
            this.ts.worker = data;
            console.log("facing:: facingCompleted :: data ",data);
            // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
            this.searchBlockId(this.ts.worker.cameraLocation, this.ts.worker.cutIndex, this.ts.worker.session_id);
            this.ts.workerStarted = true;
            console.log(data);
          },
          error => {
            console.log(error);
          }); 
      },
      error => {
        console.log(error);
      });    
  }
   
  facingAbort(): void{
    this.ts.deactiveProject("0");
    this.workerService.getFacingAbort()
    .subscribe(
      () => {
        this.workerService.get()
        .subscribe(
          data => {
            this.ts.worker = data;
            console.log("facing:: facingAbort :: data ",data);
            // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
            this.searchBlockId(this.ts.worker.cameraLocation, this.ts.worker.cutIndex, this.ts.worker.session_id);
            this.ts.workerStarted = true;
            console.log(data);
          },
          error => {
            console.log(error);
          }); 
      },
      error => {
        console.log(error);
      });    
  }

  facingReset(): void{
    this.ts.deactiveProject("0");
    this.workerService.getFacingReset()
    .subscribe(
      () => {
        this.workerService.get()
        .subscribe(
          data => {
            this.ts.worker = data;
            console.log("facing:: facingCompleted :: data ",data);
            // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
            this.searchBlockId(this.ts.worker.cameraLocation, this.ts.worker.cutIndex, this.ts.worker.session_id);
            this.ts.workerStarted = true;
            console.log(data);
          },
          error => {
            console.log(error);
          }); 
      },
      error => {
        console.log(error);
      });    
  }

  searchCameraLocation(cameraLocation: number, image_type: number): void {
    this.imageService.findByCameraLocationImageType(cameraLocation, image_type)
      .subscribe(
        data => {
          switch(cameraLocation)
          {
            case 0:
          // this.ts.currentFacingWhtImageDefault = data;
              switch(image_type)
              {
                case 0: this.ts.currentFacingWhtImageDefault = data; 
                  this.searchSessionLabelFromDBId(String(this.ts.currentFacingWhtImageDefault?.sessions__id));
                  console.log(" session  ", this.session);
                  if(this.ts.currentFacingWhtImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentFacingWhtImageDefault?.cut_index) >= 0) {        
                      this.refCutIndex = Number(this.ts.currentFacingWhtImageDefault?.cut_index)+1;
                      this.ts.currentFacingWhtImageDefault.cut_index = String(this.refCutIndex);
                      console.log(" this.ts.currentFacingWhtImageDefault.cut_index ",this.ts.currentFacingWhtImageDefault.cut_index);
                      this.ts.currentFacingWhtImagemicrons = (Number(this.ts.currentFacingWhtImageDefault.cut_index) - 1) * 20;
                    }
                    console.log(" this.ts.currentFacingWhtImageDefault.sessionId ",this.ts.currentFacingWhtImageDefault.sessions__id);
                    this.searchCamLocaImageTypeBlockId(cameraLocation, image_type, this.ts.currentFacingWhtImageDefault.blocks__id, 0, String(this.ts.currentFacingWhtImageDefault.sessions__id), true);
                    this.searchBlock(cameraLocation,this.ts.currentFacingWhtImageDefault.blocks__id, image_type, Number(this.ts.currentFacingWhtImageDefault.cut_index)-1);
                  }
                  break;
                  
                case 1: this.ts.currentFacingUVImageDefault = data; 
                  if(this.ts.currentFacingUVImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentFacingUVImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentFacingUVImageDefault?.cut_index)+1;
                      this.ts.currentFacingUVImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentFacingUVImagemicrons = (Number(this.ts.currentFacingUVImageDefault.cut_index) - 1) * 20;
                    }
                    this.searchCamLocaImageTypeBlockId(cameraLocation, image_type, this.ts.currentFacingUVImageDefault.blocks__id, 0, String(this.ts.currentFacingUVImageDefault?.sessions__id), true);
                    this.searchBlock(cameraLocation,this.ts.currentFacingUVImageDefault.blocks__id, image_type, Number(this.ts.currentFacingUVImageDefault?.cut_index)-1);
                  }
                break;
                case 2: this.ts.currentFacingLDFImageDefault = data; 
                  if(this.ts.currentFacingLDFImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentFacingLDFImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentFacingLDFImageDefault?.cut_index)+1;
                      this.ts.currentFacingLDFImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentFacingLDFImagemicrons = (Number(this.ts.currentFacingLDFImageDefault.cut_index) - 1) * 20;
                    }
                    this.searchCamLocaImageTypeBlockId(cameraLocation, image_type, this.ts.currentFacingLDFImageDefault.blocks__id, 0, String(this.ts.currentFacingLDFImageDefault?.sessions__id), true);
                    this.searchBlock(cameraLocation, this.ts.currentFacingLDFImageDefault.blocks__id, image_type, Number(this.ts.currentFacingLDFImageDefault?.cut_index)-1);
                    this.searchAllLdf(false);
                  
                  }
                  break;
              }
              break;
          }
          console.log(" ",data);          
        },
        error => {
          console.log(error);
        });
  }

  searchAllLdf( activateFlag: boolean): void {
    console.log("String(this.ts.currentFacingWhtImageDefault?.sessions__id), Number(this.ts.currentFacingWhtImageDefault?.cut_index)-1 ::",
    String(this.ts.currentFacingWhtImageDefault?.sessions__id), Number(this.ts.currentFacingWhtImageDefault?.cut_index)-1)
   const allLdfData= this.imageService.findAllLdf(String(this.ts.currentFacingWhtImageDefault?.sessions__id), Number(this.ts.currentFacingWhtImageDefault?.cut_index)-1)
   .pipe(
    map((res: any) => {
      if ((res==null) || (res==undefined) || (res.length === 0) ) {
        console.log('searchAllLdf::Error occurred.');
        // delay(100);
        throw new Error('searchAllLdf::Value expected!');
      }
      return res;
    }),
    retry(), // Retry forever
    // delay(10000),
    catchError(() => of([]))
  );
  
  allLdfData.subscribe(
        data => {
          if (data == null || data.length < 6) {
            console.log("--------------allLdfData found null searchAllLdf ::",data);
            console.log("--------------allLdfData data.length::",data.length);
            // throw new Error("Invalid data");
            return throwError(-1);
          } else {
            console.log("--------------allLdfData data.length::",data.length);
          if(activateFlag) {
            this.ts.activeProject(String(this.ts.worker.cameraLocation), true, 3);
          }
          console.log("facingComponent::searchAllLdf: ",data);  
          this.ts.currentFacingLDF1Image = data[0]; 
          this.ts.currentFacingLDF2Image = data[1];
          this.ts.currentFacingLDF3Image = data[2]; 
          this.ts.currentFacingLDF4Image = data[3]; 
          this.ts.currentFacingLDF5Image = data[4]; 
          this.ts.currentFacingLDF6Image = data[5]; 
          }
          return ;
        },
        error => {
          console.log(error);
        });
  }

  searchCamLocaImageTypeBlockId(cameraLocation: number, image_type: number, block_id: string, cut_index: number, sessionId: string, activateFlag: boolean): number {
    const apiData = this.imageService.findByCamLocImageTypeBlockId(cameraLocation, image_type, block_id, cut_index, sessionId)
    .pipe(
      map((res: any) => {
        if ((res==null) || (res==undefined) || (sessionId == null) && (cameraLocation == 0)  ) {
          console.log('Error occurred.');
          // delay(100);
          throw new Error('Value expected!');
        }
        return res;
      }),
      retry(), // Retry forever
      // delay(10000),
      catchError(() => of([]))
    );

    
    
    apiData
    .subscribe(
        (data => {
          //throw error for demonstration
          if (data == null) {
            console.log("--------------found null searchCamLocaImageTypeBlockId ::",data);
            // throw new Error("Invalid data");
            return throwError(-1);
          } else {
            if(!activateFlag) {
            console.log("--------------activate menu flag searchCamLocaImageTypeBlockId ");
            this.ts.activeProject(String(this.ts.worker.cameraLocation), true, this.ts.noIndex);
            }
          switch(cameraLocation)
          {
            case 0:
            // this.ts.currentFacingWhtImageDefault = data;
            switch(image_type)
            {
              case 0: 
                if(activateFlag) {
                  this.ts.firstFacingWhtImageDefault = data; 
                  if(this.ts.firstFacingWhtImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.firstFacingWhtImageDefault?.cut_index) >= 0) {         
                      this.refCutIndex = Number(this.ts.firstFacingWhtImageDefault?.cut_index)+1;
                      this.ts.firstFacingWhtImageDefault.cut_index = String(this.refCutIndex);
                      console.log(" this.ts.firstFacingWhtImageDefault.cut_index ",this.ts.firstFacingWhtImageDefault.cut_index);
                      this.ts.firstFacingWhtImagemicrons = (Number(this.ts.firstFacingWhtImageDefault.cut_index) - 1) * 20;
                    }
                  }
                } else {
                  this.ts.activeProject(String(this.ts.worker.cameraLocation), true, image_type);
                  // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
                  this.ts.currentFacingWhtImageDefault = data; 
                  if(this.ts.currentFacingWhtImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentFacingWhtImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentFacingWhtImageDefault.cut_index)+1;
                      this.ts.currentFacingWhtImageDefault.cut_index = String(this.refCutIndex);
                      console.log(" this.ts.currentFacingWhtImageDefault.cut_index ",this.ts.currentFacingWhtImageDefault.cut_index);
                      this.ts.currentFacingWhtImagemicrons = (Number(this.ts.currentFacingWhtImageDefault.cut_index) - 1) * 20;
                    }
                    // this.searchBlock(cameraLocation,this.ts.currentFacingWhtImageDefault.blocks__id, image_type);
                  }
                }
                console.log("searchCamLocaImageTypeBlockId ",data);    
                break;
              case 1: 
                if(activateFlag) {
                  this.ts.firstFacingUVImageDefault = data; 
                  if(this.ts.firstFacingUVImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.firstFacingUVImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.firstFacingUVImageDefault.cut_index)+1;
                      this.ts.firstFacingUVImageDefault.cut_index = String(this.refCutIndex);
                      console.log(" this.ts.firstFacingUVImageDefault.cut_index ",this.ts.firstFacingUVImageDefault.cut_index);
                      this.ts.firstFacingUVImagemicrons = (Number(this.ts.firstFacingUVImageDefault.cut_index) - 1) * 20;
                    }
                    // this.searchBlock(cameraLocation,this.ts.currentFacingUVImageDefault.blocks__id, image_type);
                  }
                } else {
                  this.ts.activeProject(String(this.ts.worker.cameraLocation), true, image_type);
                  this.ts.currentFacingUVImageDefault = data; 
                  if(this.ts.currentFacingUVImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentFacingUVImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentFacingUVImageDefault.cut_index)+1;
                      this.ts.currentFacingUVImageDefault.cut_index = String(this.refCutIndex);
                      console.log(" this.ts.currentFacingUVImageDefault.cut_index ",this.ts.currentFacingUVImageDefault.cut_index);
                      this.ts.currentFacingUVImagemicrons = (Number(this.ts.currentFacingUVImageDefault.cut_index) - 1) * 20;
                    }
                    // this.searchBlock(cameraLocation,this.ts.currentFacingUVImageDefault.blocks__id, image_type);
                  }
                }
                console.log("searchCamLocaImageTypeBlockId ",data);    
                break;
              case 2:
                if(activateFlag) { 
                  this.ts.firstFacingLDFImageDefault = data; 
                  if(this.ts.firstFacingLDFImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.firstFacingLDFImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.firstFacingLDFImageDefault.cut_index)+1;
                      this.ts.firstFacingLDFImageDefault.cut_index = String(this.refCutIndex);
                      console.log(" this.ts.firstFacingLDFImageDefault.cut_index ",this.ts.firstFacingLDFImageDefault.cut_index);
                      this.ts.firstFacingLDFImagemicrons = (Number(this.ts.firstFacingLDFImageDefault.cut_index) - 1) * 20;
                    }
                    // this.searchBlock(cameraLocation, this.ts.currentFacingLDFImageDefault.blocks__id, image_type);
                  }
                } else  { 
                  this.ts.activeProject(String(this.ts.worker.cameraLocation), true, image_type);
                  this.ts.currentFacingLDFImageDefault = data; 
                  if(this.ts.currentFacingLDFImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentFacingLDFImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentFacingLDFImageDefault.cut_index)+1;
                      this.ts.currentFacingLDFImageDefault.cut_index = String(this.refCutIndex);
                      console.log(" this.ts.currentFacingLDFImageDefault.cut_index ",this.ts.currentFacingLDFImageDefault.cut_index);
                      this.ts.currentFacingLDFImagemicrons = (Number(this.ts.currentFacingLDFImageDefault.cut_index) - 1) * 20;
                    }
                    this.searchAllLdf(true);
                    // this.searchBlock(cameraLocation, this.ts.currentFacingLDFImageDefault.blocks__id, image_type);
                  }
                }
                console.log("searchCamLocaImageTypeBlockId ",data);    
                break;
            }
          break;
        }
      }
          return 0;

          
          // console.log("searchCamLocaImageTypeBlockId ",data);          
        } ),
        error => {
          console.log(error);
          return 0;

        });
        return 0;

  }

  searchBlock(camera_location: number, blk_id: string, image_type: number, cutIndex: number): void {
    this.blockService.get(blk_id)
      .subscribe(
        data => {
          switch(camera_location)
          {
            case 0:
              if(cutIndex==0){
                switch(image_type)
                {
                  case 0: this.ts.firstFacingWhtBlockDefault = data; break;
                  case 1: this.ts.firstFacingUVBlockDefault = data; break;
                  case 2: this.ts.firstFacingLDFBlockDefault = data; break;
                }
              } else {
                switch(image_type)
                {
                  case 0: this.ts.currentFacingWhtBlockDefault = data; break;
                  case 1: this.ts.currentFacingUVBlockDefault = data; break;
                  case 2: this.ts.currentFacingLDFBlockDefault = data; break;
                }
              }
              break;
            }
              
          
          console.log("facingComponent::searchBlock",data);
        },
        error => {
          console.log(error);
        });
  }

   //  we have id in session table find the session_id 
   searchSessionLabelFromDBId(id: String): void {
    // this.session = null;
    //const apiDataId = this.sessionService.get(id);
    const apiDataId = this.sessionService.get(id) // findByCamLocImageTypeBlockId(cameraLocation, image_type, block_id, cut_index, sessionId)
    .pipe(
      map((res: any) => {
        if ((res==null) || (res==undefined) ) {
          console.log('searchSessionLabelFromDBId::Error occurred.');
          // delay(100);
          throw new Error('searchSessionLabelFromDBId::Value expected!');
        }
        return res;
      }),
      retry(), // Retry forever
      // delay(10000),
      catchError(() => of([]))
    );

    apiDataId
    .subscribe(
      (sessionData => {
        if (sessionData == null) {
          console.log("--------------sessionData found null searchSessionLabelFromDBId ::",sessionData);
          // throw new Error("Invalid data");
          return throwError(-1);
        } else {
          console.log("--------------sessionData in searchSessionLabelFromDBId ::",sessionData);
          this.session =sessionData;
        }
        return ;
      }
      ))
  }

   //  we have session_id in session table find the id 
  searchSessionId(sessionId: String): void {
    // this.session = null;
    const apiDataSessionId = this.sessionService.findBySessionId(sessionId) // findByCamLocImageTypeBlockId(cameraLocation, image_type, block_id, cut_index, sessionId)
    .pipe(
      map((res: any) => {
        if ((res==null) || (res==undefined) ) {
          console.log('Error occurred.');
          // delay(100);
          throw new Error('Value expected!');
        }
        return res;
      }),
      retry(), // Retry forever
      // delay(10000),
      catchError(() => of([]))
    );

    apiDataSessionId
    .subscribe(
      (sessionData => {
        if (sessionData == null) {
          console.log("--------------sessionData found null searchSessionId ::",sessionData);
          // throw new Error("Invalid data");
          return throwError(-1);
        } else {
          console.log("--------------sessionData searchSessionId ::",sessionData);
          console.log("--------------sessionData searchSessionId ::",sessionData.id);
          this.session =sessionData;

          console.log("searchBlockId id",this.ts.worker.id,"  this.session?.id ", this.session?.id);
          const apiblkId = this.blockService?.findByBlockId(this.ts.worker.id,this.session?.id)
          .pipe(
            map((res: any) => {
              if ((res==null) || (res==undefined) ) {
                console.log('Error occurred.');
                // delay(100);
                throw new Error('Value expected!');
              }
              return res;
            }),
            retry(), 
            // delay(10000),
            catchError(() => of([]))
          );
          
      
          apiblkId
            .subscribe(
              data => {
                if (data === null || data === undefined   ) {
                  console.log("--------------data found null searchCamLocaImageTypeBlockId ::",data);
                  console.log("--------------data found null searchCamLocaImageTypeBlockId ::",data.block_id);
                  // throw new Error("Invalid data");
                  return throwError(-1);
                } else {
                this.ret_val = -1;
                this.blocks = data;
                console.log("facingComponent::searchBlockId blocks",this.blocks);
                 // facing white UV and LDF images
                // if( this.blocks != undefined && this.blocks[0] != undefined && this.blocks[0].id ) {
                  if(this.blocks?.id) {
                    console.log("facingComponent::searchBlockId blocks",this.blocks);
                  console.log("facingComponent::searchBlockId blocks__id",this.blocks.id);
                  console.log("this.blocks[0].id ", this.blocks.id)
                  console.log("this.currentTapeWhtBlockDefault?.id ",this.ts.currentFacingWhtBlockDefault?.id)
                  // console.log("this.currentTapeUVBlockDefault?.id ",this.currentTapeUVBlockDefault?.id)
                  // console.log("this.currentTapeLDFBlockDefault?.id ",this.currentTapeLDFBlockDefault?.id)
                  this.searchBlock(Number(this.ts.worker.cameraLocation),this.blocks.id,0, Number(this.ts.worker.cutIndex));
                  // while(this.ret_val == -1) {
                  // this.searchSessionId(String(session_id));
                  if(this.session != undefined && this.session.id) {
                  this.ret_val = this.searchCamLocaImageTypeBlockId(Number(this.ts.worker.cameraLocation),0,this.blocks.id, Number(this.ts.worker.cutIndex), String( this.session.id), false);
                  }
                  //if(this.ret_val == -1) {
                    console.log("...............searchBlockId  :: error detected?",this.ret_val);
                  // }
                  //}
                  this.searchBlock(Number(this.ts.worker.cameraLocation), this.blocks.id,1, Number(this.ts.worker.cutIndex));
                  this.ret_val = -1;
      
                  // while(this.ret_val == -1) {
                  if(this.session != undefined && this.session.id) {
                  this.ret_val = this.searchCamLocaImageTypeBlockId(Number(this.ts.worker.cameraLocation),1,this.blocks.id, Number(this.ts.worker.cutIndex), String( this.session.id), false);
                  }
                  this.searchBlock(Number(this.ts.worker.cameraLocation), this.blocks.id,2, Number(this.ts.worker.cutIndex));
                  this.ret_val = -1;
      
                  // while(this.ret_val == -1) {
                  if(this.session != undefined && this.session.id) {
                  this.ret_val = this.searchCamLocaImageTypeBlockId(Number(this.ts.worker.cameraLocation),2,this.blocks.id, Number(this.ts.worker.cutIndex), String( this.session.id), false);
                  // this.searchAllLdf(true);
                }
                  //if(this.ret_val == -1) {
                  console.log("...............searchBlockId  :: error detected?",this.ret_val);
                  if(Number(this.ts.worker.cameraLocation) === 0) {
                    this.searchBlock(Number(this.ts.worker.cameraLocation), this.blocks.id,2, Number(this.ts.worker.cutIndex));
                    if(this.session != undefined && this.session.id) {
                    this.ret_val = this.searchCamLocaImageTypeBlockId(Number(this.ts.worker.cameraLocation),2,this.blocks.id, Number(this.ts.worker.cutIndex), String( this.session.id), false);
                    // this.searchAllLdf(true);
                    }
                    console.log("...............searchBlockId  :: error detected?");
                  }
                  
                  // }
                  //}
                }
                // this.searchAllLdf(true);
                console.log("...............searchBlockId  :: error detected?");
              }
              return ;
              },
              error => {
                console.log("..............searchBlockId  :: error detected!!!.....", error);
              });


          
        }
        return ;
      }
      ));
  }


  searchBlockId(camera_location: number,cut_index: number, session_id:string): void {
    this.searchSessionId(this.ts.worker.session_id);
    console.log("searchBlockId id",this.ts.worker.id);
  }

  facingPercentCommentSave(): void{
    console.log("user comment saved :", this.userComment);

    const data = {
      download_path: this.ts.currentFacingWhtImageDefault?.download_path,
      description: this.ts.currentFacingWhtImageDefault?.description,
      camera_location: this.ts.currentFacingWhtImageDefault?.camera_location,
      image_type: this.ts.currentFacingWhtImageDefault?.image_type,
      json: this.ts.currentFacingWhtImageDefault?.json,
      id: this.ts.currentFacingWhtImageDefault?.id,
      thumbnail_path: this.ts.currentFacingWhtImageDefault?.download_path,
      comment: this.userComment,
      blocks__id:this.ts.currentFacingWhtImageDefault?.blocks__id,
      sessions__id: this.ts.currentFacingWhtImageDefault?.sessions__id,
      cut_index: this.ts.currentFacingWhtImageDefault?.cut_index,
      func: this.ts.currentFacingWhtImageDefault?.func,
      facing_decision: this.selectedfacingPercent,
      facing_action: this.ts.currentFacingWhtImageDefault?.facing_action,
      image_index: this.ts.currentFacingWhtImageDefault?.image_index,
      created_at: this.ts.currentFacingWhtImageDefault?.created_at,
      updated_at: this.ts.currentFacingWhtImageDefault?.updated_at,
    };

    this.message = '';

    this.imageService.update(this.ts.currentFacingWhtImageDefault?.id, data)
      .subscribe(
        response => {
          if(this.ts.currentFacingWhtImageDefault !== undefined) {
          this.ts.currentFacingWhtImageDefault.comment = this.userComment;
          this.ts.currentFacingWhtImageDefault.facing_decision = this.selectedfacingPercent;
          }
          console.log(response);
          this.message = response.message ? response.message : 'This image was updated successfully!';
        },
        error => {
          console.log(error);
        });
  }

}
