import { Component, OnInit } from '@angular/core';
import { ImageService } from 'src/app/services/image.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Image } from 'src/app/models/image.model';

@Component({
  selector: 'app-image-details',
  templateUrl: './image-details.component.html',
  styleUrls: ['./image-details.component.css']
})
export class ImageDetailsComponent implements OnInit {
  currentImage: Image = {
    download_path: '',
    description: '',
    comment: '',
    camera_location: '',
    image_type: '',
    json: ''
  };
  message = '';

  constructor(
    private imageService: ImageService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.message = '';
    this.getImage(this.route.snapshot.params.id);
  }

  getImage(id: string): void {
    this.imageService.get(id)
      .subscribe(
        data => {
          this.currentImage = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  updatePublished(status: string): void {
    const data = {
      download_path: this.currentImage.download_path,
      description: this.currentImage.description,
      camera_location: this.currentImage.camera_location,
      image_type: this.currentImage.image_type,
      json: this.currentImage.json,
      published: status
    };

    this.message = '';

    this.imageService.update(this.currentImage.id, data)
      .subscribe(
        response => {
          this.currentImage.comment = status;
          console.log(response);
          this.message = response.message ? response.message : 'This image was updated successfully!';
        },
        error => {
          console.log(error);
        });
  }

  updateImage(): void {
    this.message = '';

    this.imageService.update(this.currentImage.id, this.currentImage)
      .subscribe(
        response => {
          console.log(response);
          this.message = response.message ? response.message : 'This image was updated successfully!';
        },
        error => {
          console.log(error);
        });
  }

  deleteImage(): void {
    this.imageService.delete(this.currentImage.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/images']);
        },
        error => {
          console.log(error);
        });
  }
}
