import { Component, OnInit } from '@angular/core';
import { Image } from 'src/app/models/image.model';
import { ImageService } from 'src/app/services/image.service';

@Component({
  selector: 'app-images-list',
  templateUrl: './images-list.component.html',
  styleUrls: ['./images-list.component.css']
})
export class ImagesListComponent implements OnInit {
  images?: Image[];
  currentImage?: Image;
  currentIndex = -1;
  download_path = '';
  blocks__id = '';
  constructor(private imageService: ImageService) { }

  ngOnInit(): void {
    this.retrieveImages();
  }

  retrieveImages(): void {
    this.imageService.getAll()
      .subscribe(
        data => {
          this.images = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.retrieveImages();
    this.currentImage = undefined;
    this.currentIndex = -1;
  }

  setActiveImage(image: Image, index: number): void {
    this.currentImage = image;
    this.currentIndex = index;
    
  }

  removeAllImages(): void {
    this.imageService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.refreshList();
        },
        error => {
          console.log(error);
        });
  }

  searchTitle(): void {
    this.currentImage = undefined;
    this.currentIndex = -1;

    this.imageService.findByTitle(this.blocks__id)
      .subscribe(
        data => {
          this.images = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  searchBlockId(): void {
    this.currentImage = undefined;
    this.currentIndex = -1;

    this.imageService.findById(this.blocks__id)
      .subscribe(
        data => {
          this.images = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

}
