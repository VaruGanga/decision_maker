import { Component, HostListener, OnInit } from '@angular/core';
import { Block } from 'src/app/models/block.model';
import { Image } from 'src/app/models/image.model';
import { BlockService } from 'src/app/services/block.service';
import { ImageService } from 'src/app/services/image.service';
import { ImagesListComponent } from '../images-list/images-list.component';

@Component({
  selector: 'app-blocks-list',
  templateUrl: './blocks-list.component.html',
  styleUrls: ['./blocks-list.component.css']
})
export class BlocksListComponent implements OnInit {
  blocksAll?: Block[];
  blocks?: Block;
  images?: Image[];
  currentBlock?: Block;
  currentImage?: Image;
  currentIndex = -1;
  currentCutIndex = -1;
  block_id = '';
  show = true;
  
  // @HostListener('document:click', ['$event'])

    clickout() {
     this.show = true;
    }
  constructor(private blockService: BlockService, private imageService: ImageService) { }

  ngOnInit(): void {
    this.retrieveBlocks();
  }
  clickit(){
    this.show = !this.show;
    }
  retrieveBlocks(): void {
    this.blockService.getAll()
      .subscribe(
        data => {
          this.blocksAll = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });    
        
    // this.imageService.getAll()
    //   .subscribe(
    //     data => {
    //       this.images = data;
    //       console.log(data);
    //     },
    //     error => {
    //       console.log(error);
    //     });  
  }


  refreshList(): void {
    this.retrieveBlocks();
    this.currentBlock = undefined;
    this.currentIndex = -1;
  }

  setActiveBlocks(block: Block, index: number): void {
    this.currentBlock = block;
    this.currentIndex = index;
    
    
    this.imageService.findById(this.currentBlock.id)
      .subscribe(
        data => {
          this.images = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
    this.clickit();
}

  setCutIndexActiveBlocks(image: Image, indx: number): void {
    this.currentImage = image;
    this.currentCutIndex = indx;
  }

  removeAllBlocks(): void {
    this.blockService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.refreshList();
        },
        error => {
          console.log(error);
        });
  }

  searchTitle(): void {
    this.currentBlock = undefined;
    this.currentIndex = -1;

    this.blockService.findByTitle(this.block_id)
      .subscribe(
        data => {
          this.blocksAll = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  searchBlockId(): void {
    this.currentBlock = undefined;
    this.currentIndex = -1;

    this.blockService.findByBlockId(this.block_id, 0) // temporarily passing 0 as argument
      .subscribe(
        data => {
          this.blocks = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

}



