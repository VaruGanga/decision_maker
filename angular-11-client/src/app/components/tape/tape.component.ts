import { Component, OnInit } from '@angular/core';
import { WorkerService } from 'src/app/services/worker.service';
import { Worker } from "src/app/models/worker.model"
import { Block } from 'src/app/models/block.model';
import { Image } from 'src/app/models/image.model';
import { BlockService } from 'src/app/services/block.service';
import { ImageService } from 'src/app/services/image.service';
import { BlocksListComponent } from 'src/app/components/blocks-list/blocks-list.component';
import { GlobalConstants } from 'src/app/common/global-constants';
import { catchError, delay, map, retry } from 'rxjs/operators';
import { of, throwError } from 'rxjs';
import { Session } from 'src/app/models/session.model';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-tape',
  templateUrl: './tape.component.html',
  styleUrls: ['./tape.component.css']
})
export class TapeComponent implements OnInit {
  worker = this.ts.worker;
  // worker?: Worker[];
  // worker: Worker = {
  //   id: '...wait to receive information from the queue...',
  //   cameraLocation: '0'
  //     };
  blockList?:BlocksListComponent;
  // ={
  //   currentTapeWhtImageDefault.download_path = '0/0',
  // };
  blocks?: Block;
  images?: Image[];
  session?: Session = {
    id : null,
    session_id : ''

  };
  show = false;
  refCutIndex = 0;
  ret_val = 0;
  index = 0;
  foundSectionUV = false;
  foundSectionSeg = false;
  // currentImageDefault?: Image;
  // currentTapeWhtImagemicrons = 0;
  // currentTapeWhtBlockDefault?: Block;
  // currentTapeWhtImageDefault?: Image;
  // currentTapeUVImagemicrons = 0;
  // currentTapeUVBlockDefault?: Block;
  // currentTapeUVImageDefault?: Image;

  title = 'DecisionMaker';
  constructor(private workerService: WorkerService,private blockService: BlockService, private imageService: ImageService , private sessionService: SessionService, public ts: GlobalConstants) { }

  ngOnInit(): void {
    if(this.ts.workerStarted === false) {
      this.ts.workerStarted=true;
      this.retrieveDefault();
    }
  
    // Tape White and UV images
    this.ret_val =  this.searchCameraLocation(2,0);
    console.log("ret_val:: searchCameraLocation(2,0)::",this.ret_val);
    this.ret_val =  this.searchCameraLocation(2,1);
    console.log("ret_val:: searchCameraLocation(2,0)::",this.ret_val);

    // this.ret_val =  this.searchCamLocCutIndex(1,1,0);
    // console.log("ret_val:: searchCameraLocation(1,1)::",this.ret_val);
    // this.ret_val =  this.searchCamLocCutIndex(1,3,0);
    // console.log("ret_val:: searchCameraLocation(1,3)::",this.ret_val);
    
  }
  retrieveDefault(): void{
    const workerApiData = this.workerService.get()
    .pipe(
      map((res: any) => {
        if ((res==null) || (res==undefined) || res.cameraLocation == null) {
          console.log('Error occurred.');
          // delay(100);
          throw new Error('Value expected!');
        }
        return res;
      }),
      retry(), // Retry forever
      // delay(10000),
      catchError(() => of([]))
    );

    workerApiData
    .subscribe(
      data => {
        this.ts.worker = data;
          console.log("tapeComponent:: before ",data);
        // if(this.ts.worker.cameraLocation) {
          // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
          if(this.ts.worker.cameraLocation == "2" || this.ts.worker.cameraLocation == '2') {
            this.searchBlockId(this.ts.worker.cameraLocation, this.ts.worker.cutIndex, this.ts.worker.session_id);
          }
        // }
        console.log("tapeComponent:: after ",data);
      },
      error => {
        console.log(error);
      });    
  }

  clickout() {
    this.show = true;
  }
 
  clickit(){
    this.show = !this.show;
  }

  tapeTransferToSlide(): void{
    this.ts.deactiveProject("2");
    this.workerService.getTapeTransferToSlide()
    .subscribe(
      data => {
        this.workerService.get()
        .subscribe(
          data => {
            this.ts.worker = data;
            console.log("tape:: tapeTransferToSlide :: data ",data);
            // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
            this.searchBlockId(this.ts.worker.cameraLocation, this.ts.worker.cutIndex, this.ts.worker.session_id);
            console.log(data);
          },
          error => {
            console.log(error);
          }); 
      },
      error => {
        console.log(error);
      });    
      
      if(this.blockList!=undefined)
        this.blockList.block_id=this.worker.id;
        this.blockList?.searchTitle();
  }

  tapeDiscard(): void{
    this.ts.deactiveProject("2");
    this.workerService.getTapeDiscard()
    .subscribe(
      data => {
        this.workerService.get()
        .subscribe(
          data => {
            this.ts.worker = data;
            console.log("tape:: tapeDiscard :: data ",data);
            // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
            this.searchBlockId(this.ts.worker.cameraLocation, this.ts.worker.cutIndex, this.ts.worker.session_id);
            console.log(data);
          },
          error => {
            console.log(error);
          }); 
        console.log(data);
      },
      error => {
        console.log(error);
      });    
  }
   
  tapeRehydrate(): void{
    this.ts.deactiveProject("2");
    this.workerService.getTapeRehydrate()
    .subscribe(
      data => {
        this.workerService.get()
        .subscribe(
          data => {
            this.ts.worker = data;
            console.log("tape:: tapeRehydrate :: data ",data);
            // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
            this.searchBlockId(this.ts.worker.cameraLocation, this.ts.worker.cutIndex, this.ts.worker.session_id);
            console.log(data);
          },
          error => {
            console.log(error);
          }); 
      },
      error => {
        console.log(error);
      });    
  }

  tapeReset(): void{
    this.ts.deactiveProject("2");
    this.workerService.getTapeReset()
    .subscribe(
      data => {
        this.workerService.get()
        .subscribe(
          data => {
            this.ts.worker = data;
            console.log("tape:: tapeReset :: data ",data);
            // this.ts.activeProject(String(this.ts.worker.cameraLocation), true);
            this.searchBlockId(this.ts.worker.cameraLocation, this.ts.worker.cutIndex, this.ts.worker.session_id);
            console.log(data);
          },
          error => {
            console.log(error);
          }); 
      },
      error => {
        console.log(error);
      });    
  }

  searchCamLocCutIndex(cameraLocation: number, blk_id: string, image_type: number, cut_index: number): number {
    console.log("searchCamLocCutIndex:: first line");
    this.index = 0;
    const apiData = this.imageService.findByCameraLocationImageTypeCutIndex(cameraLocation,image_type, cut_index)
    .pipe(
      map((res: any) => {
        if ((res==null) || (res==undefined) || (blk_id == null) && (cameraLocation == 1)  ) {
          console.log('Error occurred.');
          // delay(100);
          throw new Error('Value expected!');
        }
        return res;
      }),
      retry(), // Retry forever
      // delay(10000),
      catchError(() => of([]))
    );

    
    
    apiData
    .subscribe(
        (data => {
          //throw error for demonstration
          if (data == null) {
            console.log("--------------found null searchCamLocCutIndex ::",data);
            return throwError(-1);
          }
          for( let index = 0; index <data.length; index++) {
            console.log("...... image_type = ",image_type,  ".....index = ", index)
          switch(cameraLocation)
          {
            case 1:
              // this.ts.currentTapeWhtImageDefault = data;
                  switch(image_type)
                  {
                                         
                    case 1: this.ts.currentTapeSectionUVImageDefault = data[index]; 
                      if(this.ts.currentTapeSectionUVImageDefault?.blocks__id !== undefined) {
                        if(Number(this.ts.currentTapeSectionUVImageDefault?.cut_index) >= 0) {
                          this.refCutIndex = Number(this.ts.currentTapeSectionUVImageDefault?.cut_index)+1;
                          this.ts.currentTapeSectionUVImageDefault.cut_index = String(this.refCutIndex);
                          this.ts.currentTapeSectionUVImagemicrons = (Number(this.ts.currentTapeSectionUVImageDefault.cut_index) - 1) * 4;
                        }
                        this.ts.activeProject("2", true, 2);
                        // this.searchCamLocaImageTypeBlockId(cameraLocation, image_type, this.ts.currentTapeUVImageDefault.blocks__id, 0)
                        this.searchBlock(cameraLocation,this.ts.currentTapeSectionUVImageDefault.blocks__id, image_type, Number(this.ts.currentTapeSectionUVImageDefault?.cut_index)-1);
                        if(this.foundSectionUV == true) {
                          console.log("***************** foundSectionUV true index = ", index)
                          index = data.length + 1; //to exit from for loop give index a greater value
                        }
                        if(index == (data.length -1) && this.foundSectionUV == false) {
                          console.log("***************** foundSectionUV false index = ", index)
                          this.ts.currentTapeSectionUVImageDefault = undefined;
                        }
                      }
                    break;
    
                    case 3: 
                    this.ts.currentTapeSectionSegImageDefault = data[index]; 
                  if(this.ts.currentTapeSectionSegImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentTapeSectionSegImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentTapeSectionSegImageDefault?.cut_index)+1;
                      this.ts.currentTapeSectionSegImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentTapeSectionSegImagemicrons = (Number(this.ts.currentTapeSectionSegImageDefault.cut_index) - 1) * 4;
                    }
                    // this.searchCamLocaImageTypeBlockId(cameraLocation, image_type, this.ts.currentTapeUVImageDefault.blocks__id, 0)
                    this.searchBlock(cameraLocation,this.ts.currentTapeSectionSegImageDefault.blocks__id, image_type, Number(this.ts.currentTapeSectionSegImageDefault?.cut_index)-1);
                    if(this.foundSectionSeg == true) {
                      console.log("***************** foundSectionSeg true index = ", index)
                      index = data.length + 1; //to exit from for loop give index a greater value
                    }
                    if(index == (data.length -1) && this.foundSectionSeg == false) {
                      console.log("***************** foundSectionSeg false index = ", index)
                      this.ts.currentTapeSectionSegImageDefault = undefined;
                    }
                  }
                break;
    
                  }
                  break;
                }
          }
          console.log("searchCamLocCutIndex:: ",data);  
          console.log("searchCamLocCutIndex:: last line1");      
          return 0;  
        }),
        error => {
          console.log("searchCamLocCutIndex:: last line2");      
          
          console.log("searchCamLocCutIndex:: error",error);
        });
        console.log("searchCamLocCutIndex:: last line3");      

        return 0;
  }

  searchCameraLocation(cameraLocation: number, image_type: number): number {
    console.log("searchCameraLocation:: first line");
    this.imageService.findByCameraLocationImageType(cameraLocation, image_type)
      .subscribe(
        (data => {
          //throw error for demonstration
          if (data == null) {
            console.log("--------------found null searchCameraLocation ::",data);
            return throwError(-1);
          }
          switch(cameraLocation)
          {
            case 2:
          // this.ts.currentTapeWhtImageDefault = data;
              switch(image_type)
              {
                case 0: this.ts.currentTapeWhtImageDefault = data; 
                  console.log(" this.ts.currentTapeWhtImageDefault?.sessions__id  ", this.ts.currentTapeWhtImageDefault?.sessions__id);
                  this.searchSessionLabelFromDBId(String(this.ts.currentTapeWhtImageDefault?.sessions__id));
                  console.log(" session  ", this.session);
                  if(this.ts.currentTapeWhtImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentTapeWhtImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentTapeWhtImageDefault?.cut_index)+1;
                      this.ts.currentTapeWhtImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentTapeWhtImagemicrons = (Number(this.ts.currentTapeWhtImageDefault.cut_index) - 1) * 4;
                    }
                    
                    // this.searchCamLocaImageTypeBlockId(cameraLocation, image_type, this.ts.currentTapeWhtImageDefault.blocks__id, 0)
                    this.searchBlock(cameraLocation,this.ts.currentTapeWhtImageDefault.blocks__id, image_type, Number(this.ts.currentTapeWhtImageDefault.cut_index)-1);
                  }
                  break;
                  
                case 1: this.ts.currentTapeUVImageDefault = data; 
                  if(this.ts.currentTapeUVImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentTapeUVImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentTapeUVImageDefault?.cut_index)+1;
                      this.ts.currentTapeUVImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentTapeUVImagemicrons = (Number(this.ts.currentTapeUVImageDefault.cut_index) - 1) * 4;
                    }
                    // this.searchCamLocaImageTypeBlockId(cameraLocation, image_type, this.ts.currentTapeUVImageDefault.blocks__id, 0)
                    this.searchBlock(cameraLocation,this.ts.currentTapeUVImageDefault.blocks__id, image_type, Number(this.ts.currentTapeUVImageDefault?.cut_index)-1);

                    this.searchCamLocCutIndex(1,this.ts.currentTapeUVImageDefault.blocks__id, 1,0); // find section UV cut index 0
                    // this.searchCamLocCutIndex(1,this.ts.currentTapeUVImageDefault.blocks__id, 3,0);  //find section seg cut index 0
                  }
                break;
              }
              break;
          }
          console.log("searchCameraLocation:: ",data);  
          console.log("searchCameraLocation:: last line1");      
          return 0;  
        }),
        error => {
          console.log("searchCameraLocation:: last line2");      
          
          console.log("searchCameraLocation:: error",error);
        });
        console.log("searchCameraLocation:: last line3");      

        return 0;
  }

  searchCamLocaImageTypeBlockId(cameraLocation: number, image_type: number, block_id: string, cut_index: number, sessionId: string, activateFlag: boolean): number {
    

    const apiData = this.imageService.findByCamLocImageTypeBlockId(cameraLocation, image_type, block_id, cut_index, sessionId)
    .pipe(
      map((res: any) => {
        if ((res==null) || (res==undefined) || (sessionId == null) && (cameraLocation == 1)  ) {
          console.log('Error occurred.');
          // delay(100);
          throw new Error('Value expected!');
        }
        return res;
      }),
      retry(), // Retry forever
      // delay(10000),
      catchError(() => of([]))
    );

    
    
    apiData
      .subscribe(
        (data => {
          //throw error for demonstration
          if (data == null) {
            console.log("--------------found null searchCamLocaImageTypeBlockId ::",data);
            // throw new Error("Invalid data");
            return throwError(-1);
          } else {
            console.log("--------------activate menu flag searchCamLocaImageTypeBlockId ");
            // if(Number(this.ts.worker.cameraLocation) === 2 && image_type === 1) {
            //   this.ts.activeProject("2", true, 2);
            // } else if(Number(this.ts.worker.cameraLocation) === 2) {
              // this.ts.activeProject("2", true, image_type);
            // }
            
          switch(cameraLocation)
          {
            case 2:
            // this.ts.currentTapeWhtImageDefault = data;
            switch(image_type)
            {
              case 0: 
                // if(cut_index==0) {
                //   this.firstFacingWhtImageDefault = data; 
                //   if(this.firstFacingWhtImageDefault?.blocks__id !== undefined) {
                //     if(this.firstFacingWhtImageDefault?.cut_index) {
                //       this.refCutIndex = Number(this.firstFacingWhtImageDefault?.cut_index)+1;
                //       this.firstFacingWhtImageDefault.cut_index = String(this.refCutIndex);
                //       this.firstFacingWhtImagemicrons = (Number(this.firstFacingWhtImageDefault.cut_index) - 1) * 4;
                //     }
                //   }
                // } else {
                  this.ts.currentTapeWhtImageDefault = data; 
            
                  if(this.ts.currentTapeWhtImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentTapeWhtImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentTapeWhtImageDefault.cut_index)+1;
                      this.ts.currentTapeWhtImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentTapeWhtImagemicrons = (Number(this.ts.currentTapeWhtImageDefault.cut_index) - 1) * 4;
                    }
                    this.ts.activeProject("2", true, 0);
                    // this.searchBlock(cameraLocation,this.ts.currentTapeWhtImageDefault.blocks__id, image_type);
                  }
                //}
                console.log("searchCamLocaImageTypeBlockId ",data);    
                break;
              case 1: 
                // if(cut_index==0) {
                //   this.firstFacingUVImageDefault = data; 
                //   if(this.firstFacingUVImageDefault?.blocks__id !== undefined) {
                //     if(this.firstFacingUVImageDefault?.cut_index) {
                //       this.refCutIndex = Number(this.firstFacingUVImageDefault.cut_index)+1;
                //       this.firstFacingUVImageDefault.cut_index = String(this.refCutIndex);
                //       this.firstFacingUVImagemicrons = (Number(this.firstFacingUVImageDefault.cut_index) - 1) * 4;
                //     }
                //     // this.searchBlock(cameraLocation,this.ts.currentTapeUVImageDefault.blocks__id, image_type);
                //   }
                // } else {
                  this.ts.currentTapeUVImageDefault = data; 
                  if(this.ts.currentTapeUVImageDefault?.blocks__id !== undefined) {
                    if(Number(this.ts.currentTapeUVImageDefault?.cut_index) >= 0) {
                      this.refCutIndex = Number(this.ts.currentTapeUVImageDefault.cut_index)+1;
                      this.ts.currentTapeUVImageDefault.cut_index = String(this.refCutIndex);
                      this.ts.currentTapeUVImagemicrons = (Number(this.ts.currentTapeUVImageDefault.cut_index) - 1) * 4;
                    }
                    console.log(".............activeProject  tape UV flag")
                    this.ts.activeProject("2", true, 1);
                    // this.searchBlock(cameraLocation,this.ts.currentTapeUVImageDefault.blocks__id, image_type);
                  }
                //}
                console.log("searchCamLocaImageTypeBlockId ",data);    
                break;
                
            }
          break;
          }
        }
          return 0;
          // console.log("searchCamLocaImageTypeBlockId ",data);          
        } ),
        
        error => {
          console.log("searchCamLocaImageTypeBlockId :: error",error);
          return 0;
        });
        return 0;
  }

  searchBlock(camera_location: number, blk_id: string, image_type: number, cutIndex: number): void {
    this.blockService.get(blk_id)
      .subscribe(
        data => {
          switch(camera_location)
          {
            case 2:
      
                switch(image_type)
                {
                  case 1: this.ts.currentTapeSectionUVBlockDefault = data; 
                  if(this.ts.currentTapeSectionUVBlockDefault.block_id == this.ts.currentTapeUVBlockDefault?.block_id) {
                    this.foundSectionUV = true;
                  } else {
                    this.foundSectionUV = false;
                  }
                  break;
                  case 3: this.ts.currentTapeSectionSegBlockDefault = data;
                  if(this.ts.currentTapeSectionSegBlockDefault?.block_id == this.ts.currentTapeUVBlockDefault?.block_id) {
                    this.foundSectionSeg = true;
                  } else {
                    this.foundSectionSeg = false;
                  }
                   break;
                }
              break;
            case 2:
             
                switch(image_type)
                {
                  case 0: this.ts.currentTapeWhtBlockDefault = data; break;
                  case 1: this.ts.currentTapeUVBlockDefault = data; break;
                }
              break;
            }
              
          
          console.log("sectionComponent::searchBlock",data);
        },
        error => {
          console.log("searchBlock::error",error);
        });
  }

  //  we have id in session table find the session_id 
  searchSessionLabelFromDBId(id: String): void {
    // this.session = null;
    //const apiDataId = this.sessionService.get(id);
    const apiDataId = this.sessionService.get(id) // findByCamLocImageTypeBlockId(cameraLocation, image_type, block_id, cut_index, sessionId)
    .pipe(
      map((res: any) => {
        if ((res==null) || (res==undefined) ) {
          console.log('searchSessionLabelFromDBId::Error occurred.');
          // delay(100);
          throw new Error('searchSessionLabelFromDBId::Value expected!');
        }
        return res;
      }),
      retry(), // Retry forever
      // delay(10000),
      catchError(() => of([]))
    );

    apiDataId
    .subscribe(
      (sessionData => {
        if (sessionData == null) {
          console.log("--------------sessionData found null searchSessionLabelFromDBId ::",sessionData);
          // throw new Error("Invalid data");
          return throwError(-1);
        } else {
          console.log("--------------sessionData in searchSessionLabelFromDBId ::",sessionData);
          this.session =sessionData;
        }
        return ;
      }
      ))
  }

   //  we have session_id in session table find the id 
  searchSessionId(sessionId: String): void {
    // this.session = null;
    const apiDataSessionId = this.sessionService.findBySessionId(sessionId) // findByCamLocImageTypeBlockId(cameraLocation, image_type, block_id, cut_index, sessionId)
    .pipe(
      map((res: any) => {
        if ((res==null) || (res==undefined) ) {
          console.log('Error occurred.');
          // delay(100);
          throw new Error('Value expected!');
        }
        return res;
      }),
      retry(), // Retry forever
      // delay(10000),
      catchError(() => of([]))
    );

    apiDataSessionId
    .subscribe(
      (sessionData => {
        if (sessionData == null) {
          console.log("--------------sessionData found null searchSessionId ::",sessionData);
          // throw new Error("Invalid data");
          return throwError(-1);
        } else {
          console.log("--------------sessionData searchSessionId ::",sessionData);
          console.log("--------------sessionData searchSessionId ::",sessionData.id);
          this.session =sessionData;

          console.log("searchBlockId id",this.ts.worker.id,"  this.session?.id ", this.session?.id);
          const apiblkId = this.blockService?.findByBlockId(this.ts.worker.id,this.session?.id)
          .pipe(
            map((res: any) => {
              if ((res==null) || (res==undefined) ) {
                console.log('Error occurred.');
                // delay(100);
                throw new Error('Value expected!');
              }
              return res;
            }),
            retry(), 
            // delay(10000),
            catchError(() => of([]))
          );
          
      
          apiblkId
            .subscribe(
              data => {
                if (data === null || data === undefined   ) {
                  console.log("--------------data found null searchCamLocaImageTypeBlockId ::",data);
                  console.log("--------------data found null searchCamLocaImageTypeBlockId ::",data.block_id);
                  // throw new Error("Invalid data");
                  return throwError(-1);
                } else {
                this.ret_val = -1;
                this.blocks = data;
                console.log("sectionComponent::searchBlockId blocks",this.blocks);
                 // facing white UV and LDF images
                // if( this.blocks != undefined && this.blocks[0] != undefined && this.blocks[0].id ) {
                  if(this.blocks?.id) {
                    console.log("sectionComponent::searchBlockId blocks",this.blocks);
                  console.log("sectionComponent::searchBlockId blocks__id",this.blocks.id);
                  console.log("this.blocks[0].id ", this.blocks.id)
                  console.log("this.ts.currentTapeWhtBlockDefault?.id ",this.ts.currentTapeWhtBlockDefault?.id)
                  // console.log("this.ts.currentTapeUVBlockDefault?.id ",this.ts.currentTapeUVBlockDefault?.id)
                  // console.log("this.ts.currentTapeLDFBlockDefault?.id ",this.ts.currentTapeLDFBlockDefault?.id)
                  this.searchBlock(Number(this.ts.worker.cameraLocation),this.blocks.id,0, Number(this.ts.worker.cutIndex));
                  // while(this.ret_val == -1) {
                  // this.searchSessionId(String(session_id));
                  if(this.session != undefined && this.session.id) {
                  this.ret_val = this.searchCamLocaImageTypeBlockId(Number(this.ts.worker.cameraLocation),0,this.blocks.id, Number(this.ts.worker.cutIndex), String( this.session.id), false);
                  }
                  //if(this.ret_val == -1) {
                    console.log("...............searchBlockId  :: error detected?",this.ret_val);
                  // }
                  //}
                  this.searchBlock(Number(this.ts.worker.cameraLocation), this.blocks.id,1, Number(this.ts.worker.cutIndex));
                  this.ret_val = -1;
      
                  // while(this.ret_val == -1) {
                  if(this.session != undefined && this.session.id) {
                  this.ret_val = this.searchCamLocaImageTypeBlockId(Number(this.ts.worker.cameraLocation),1,this.blocks.id, Number(this.ts.worker.cutIndex), String( this.session.id), false);
                  }
                  //if(this.ret_val == -1) {
                    console.log("...............searchBlockId  :: error detected?",this.ret_val);
                  // }
                  //}
                }
              }
              return ;
              },
              error => {
                console.log("..............searchBlockId  :: error detected!!!.....", error);
              });


          
        }
        return ;
      }
      ));
  }

  searchBlockId(camera_location: number,cut_index: number, session_id:string): void {
    // this.currentImage = undefined;
    // this.currentIndex = -1;
    
    this.searchSessionId(this.ts.worker.session_id);
    console.log("searchBlockId id",this.ts.worker.id,"  this.session?.id ", this.session?.id);
    if(this.session != null && this.session.id != null) {
    // const apiblkId = this.blockService.findByBlockId(this.ts.worker.id,this.session.id);
    // const apiblkId = this.blockService?.findByBlockId(this.ts.worker.id,this.session?.id)
    // .pipe(
    //   map((res: any) => {
    //     if ((res==null) || (res==undefined) ) {
    //       console.log('Error occurred.');
    //       // delay(100);
    //       throw new Error('Value expected!');
    //     }
    //     return res;
    //   }),
    //   retry(), // Retry forever
    //   // delay(10000),
    //   catchError(() => of([]))
    // );
    

    // apiblkId
    //   .subscribe(
    //     data => {
    //       if (data === null || data === undefined || || data[0] == null || data[0] == undefined ||data[0].id === undefined) {
    //         console.log("--------------data found null searchCamLocaImageTypeBlockId ::",data);
    //         // throw new Error("Invalid data");
    //         return throwError(-1);
    //       } else {
    //       this.ret_val = -1;
    //       this.blocks = data;
    //       console.log("sectionComponent::searchBlockId blocks",this.blocks);
    //        // facing white UV and LDF images
    //       // if( this.blocks != undefined && this.blocks[0] != undefined && this.blocks[0].id ) {
    //         if(this.blocks[0].id) {
    //           console.log("sectionComponent::searchBlockId blocks",this.blocks);
    //         console.log("sectionComponent::searchBlockId blocks__id",this.blocks[0].id);
    //         console.log("this.blocks[0].id ", this.blocks[0].id)
    //         console.log("this.ts.currentTapeWhtBlockDefault?.id ",this.ts.currentTapeWhtBlockDefault?.id)
    //         // console.log("this.ts.currentTapeUVBlockDefault?.id ",this.ts.currentTapeUVBlockDefault?.id)
    //         // console.log("this.ts.currentTapeLDFBlockDefault?.id ",this.ts.currentTapeLDFBlockDefault?.id)
    //         this.searchBlock(Number(camera_location),this.blocks[0].id,0, Number(cut_index));
    //         // while(this.ret_val == -1) {
    //         // this.searchSessionId(String(session_id));
    //         if(this.session != undefined && this.session.id) {
    //         this.ret_val = this.searchCamLocaImageTypeBlockId(Number(camera_location),0,this.blocks[0].id, Number(cut_index), String( this.session.id));
    //         }
    //         //if(this.ret_val == -1) {
    //           console.log("...............searchBlockId  :: error detected?",this.ret_val);
    //         // }
    //         //}
    //         this.searchBlock(Number(camera_location), this.blocks[0].id,1, Number(cut_index));
    //         this.ret_val = -1;

    //         // while(this.ret_val == -1) {
    //         this.ret_val = this.searchCamLocaImageTypeBlockId(Number(camera_location),1,this.blocks[0].id, Number(cut_index), String(session_id));
    //         //if(this.ret_val == -1) {
    //           console.log("...............searchBlockId  :: error detected?",this.ret_val);
    //         // }
    //         //}
    //       }
    //     }
    //     return ;
    //     },
    //     error => {
    //       console.log("..............searchBlockId  :: error detected!!!.....", error);
    //     });
      }
  }


}


