# Angular Node.js MySQL Application

In this image, I will show you how to build a full-stack (Angular 11 + Node.js + Express + MySQL) example with a CRUD Application. The back-end server uses Node.js + Express for REST APIs, front-end side is an Angular App with HTTPClient.

We will build a full-stack Image Application in that:
- Image has id, title, description, published status.
- User can create, retrieve, update, delete Images.
- There is a search box for finding Images by title.

## run deployment (on 48 machine)
cd /HULI/deploy
node decision_server.js


### Node.js Server
```
cd node-js-server
node server.js
```

### Angular Client
```
cd angular-11-client
```
Run `ng serve --port 8080` for a dev server. Navigate to `http://10.0.0.98:8080/`.


### port numbers
### for dev              sql server 8080 angular 8080
### for build deployment sql server 8080 angular 8080