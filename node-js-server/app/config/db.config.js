module.exports = {
  HOST: "10.0.0.98",
  USER: "clarapath",
  PASSWORD: "clarapath",
  DB: "image_viewer",
  dialect: "mysql",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};
