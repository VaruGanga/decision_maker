const db = require("../models");
const Block = db.blocks;
const Image = db.images;
const Op = db.Sequelize.Op;

// Create and Save a new Block
exports.create = (req, res) => {
  // Validate request
  if (!req.body.block_id) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a Block
  const block = {
    id: req.body.id,
    devices__id: req.body.devices__id,
    sessions__id: req.body.sessions__id,
    block_id: req.body.block_id,
  };

  // Save Block in the database
  Block.create(block)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Block."
      });
    });
};

// Retrieve all Blocks from the database.
exports.findBlocks = (req, res) => {
  const block_id = req.query.block_id;
  const camera_location = req.query.camera_location;
  const cut_index = req.query.cut_index;

  // const block_id = '2022-02-10/0/20220209';
  var condition = block_id ? { block_id: block_id } : null;
  // var condition = block_id ? { block_id:  block_id  } : null;
  
  Block.findAll ({ 
    where: {
      block_id: block_id,
      camera_location : camera_location,
      cut_index : cut_index,
    },
    // where: condition ,
     order: [['created_at', 'DESC' ]]
   })   // id)
  .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving blocks."
      });
    });
};

// Retrieve all Blocks from the database.
exports.findAll = (req, res) => {
  const block_id = req.query.block_id;

  // const block_id = '2022-02-10/0/20220209';
  var condition = block_id ? { block_id: { [Op.like]: `%${block_id}%` } } : null;
  // var condition = block_id ? { block_id:  block_id  } : null;
  
  Block.findAll ({ 
    where: {
      block_id: condition,
    },
    // where: condition ,
     order: [['created_at', 'DESC' ]]
   })   // id)
  .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving blocks."
      });
    });
};

// Find a single Block with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Block.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Block with id=" + id
      });
    });
};

// Find a single Block with a block_id
exports.findOneBlockId = (req, res) => {
  const id = req.params.block_id;
  const sessionId = req.params.sessionId;

  Block.findOne ({ 
    where: {
      block_id: id,
      sessions__id: sessionId,
    },
    // where: condition ,
     order: [['created_at', 'DESC' ]]
   })   // id)

    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "findOneBlockId:Error retrieving Block with block_id= " + block_id
      });
    });
};

// // Find a single Block with a block_id
// exports.findOneId = (req, res) => {
//   const id = req.params.block_id;

//   Block.findByPk(id)
//     .then(data => {
//       res.send(data);
//     })
//     .catch(err => {
//       res.status(500).send({
//         message: "Error retrieving Block with block_id=" + block_id
//       });
//     });
// };

// Update a Block by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Block.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Block was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Block with id=${id}. Maybe Block was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Block with id=" + id
      });
    });
};

// Delete a Block with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Block.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Block was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Block with id=${id}. Maybe Block was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Block with id=" + id
      });
    });
};

// Delete all Blocks from the database.
exports.deleteAll = (req, res) => {
  Block.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Blocks were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all blocks."
      });
    });
};

// find all published Block
exports.findAllPublished = (req, res) => {
  Block.findAll({ where: { published: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving blocks."
      });
    });
};
