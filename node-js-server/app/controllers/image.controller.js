const db = require("../models");
const Image = db.images;
const Op = db.Sequelize.Op;

// Create and Save a new Image
exports.create = (req, res) => {
  // Validate request
  if (!req.body.download_path) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a Image
  const image = {
    download_path: req.body.download_path,
    description: req.body.description,
    comment: req.body.comment,
    camera_location: req.body.camera_location,
    image_type: req.body.image_type,
    json: req.body.json 
  };

  // Save Image in the database
  Image.create(image)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Image."
      });
    });
};

// // Retrieve all Images from the database.
// exports.findAll = (req, res) => {
//   const download_path = req.query.download_path;
//   // const download_path = '2022-02-10/0/20220209';
//   var condition = download_path ? { download_path: { [Op.like]: `%${download_path}%` } } : null;

//   Image.findAll({ where: condition })
//     .then(data => {
//       res.send(data);
//     })
//     .catch(err => {
//       res.status(500).send({
//         message:
//           err.message || "Some error occurred while retrieving images."
//       });
//     });
// };

// Retrieve all Images from the database.
exports.findAll = (req, res) => {
  const  blocks__id = req.query.blocks__id;
  // const blocks__id = '7989';
  // var condition = blocks__id ? { blocks__id: { [Op.like]: `%${blocks__id}%` } } : null;
 
  Image.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving images."
      });
    });
};
 
// Find a single Image with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Image.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Image with id=" + id
      });
    });
};

// Find a single Image with an id as image_type
exports.findFacingForCamInfo = (req, res) => {
  const img_type = req.params.id;

  // var condition = camera_location ? { camera_location: { [Op.like]: `%${camera_location}%` } } : null;
  Image.findOne({ 
    where: {
      image_type: img_type,
      camera_location: 0,
      image_index:0,
    },
    // where: condition ,
     order: [['id', 'DESC' ]]
   })
  .then(data => {
    message: "found record(s)" + img_type
    res.send(data);
  })
  .catch(err => {
    res.status(500).send({
      message: "Error retrieving Image table with image_type=" + img_type
    });
  });
};

// Find a single Image with an id as image_type
exports.findLdf = (req, res) => {
  const session_id = req.params.sessionId
  const cut_index = req.params.cut_index;
  
  // var condition = camera_location ? { camera_location: { [Op.like]: `%${camera_location}%` } } : null;
  Image.findAll({ 
    where: {
      sessions__id: session_id,
      image_type: 2,
      camera_location: 0, 
      cut_index: cut_index,
    },
    // where: condition ,
     order: [['id', 'DESC' ]]
   })
  .then(data => {
    message: "found record(s) " + session_id
    res.send(data);
  })
  .catch(err => {
    res.status(500).send({
      message: "Error retrieving Image table with image_type=" + session_id
    });
  });
};

exports.findFacingForCamInfoWithBlockId = (req, res) => {
  const img_type = req.params.id;
  const block_id = req.params.blockId;
  const cut_index = req.params.cut_index;
  const session_id = req.params.sessionId

  // var condition = camera_location ? { camera_location: { [Op.like]: `%${camera_location}%` } } : null;
  Image.findOne({ 
    where: {
      image_type: img_type,
      camera_location: 0, 
      blocks__id: block_id,
      cut_index: cut_index,
      image_index:0,
      sessions__id: session_id,
    },
    // where: condition ,
     order: [['id', 'DESC' ]]
   })
  .then(data => {
    message: "found record(s)" + img_type
    res.send(data);
  })
  .catch(err => {
    res.status(500).send({
      message: "Error retrieving Image table with image_type=" + img_type
    });
  });
};

exports.findTapeForCamInfoWithBlockId = (req, res) => {
  const img_type = req.params.id;
  const block_id = req.params.blockId;
  const cut_index = req.params.cut_index;
  // var condition = camera_location ? { camera_location: { [Op.like]: `%${camera_location}%` } } : null;
  Image.findOne({ 
    where: {
      image_type: img_type,
      camera_location: 2, 
      blocks__id: block_id,
      cut_index: cut_index,
    },
    // where: condition ,
     order: [['created_at', 'DESC' ]]
   })
  .then(data => {
    message: "found record(s)" + img_type
    res.send(data);
  })
  .catch(err => {
    res.status(500).send({
      message: "Error retrieving Image table with image_type=" + img_type
    });
  });
};

exports.findSectionForCamInfoWithBlockId = (req, res) => {
  const img_type = req.params.id;
  const block_id = req.params.blockId;
  const cut_index = req.params.cut_index;
  const sessionId = req.params.sessionId;
  // var condition = camera_location ? { camera_location: { [Op.like]: `%${camera_location}%` } } : null;
  Image.findOne({ 
    where: {
      image_type: img_type,
      camera_location: 1, 
      blocks__id: block_id,
      cut_index: cut_index,
      sessions__id: sessionId,
    },
    // where: condition ,
     order: [['created_at', 'DESC' ]]
   })
  .then(data => {
    message: "findSectionForCamInfoWithBlockId::found record(s)" + img_type
    res.send(data);
  })
  .catch(err => {
    res.status(500).send({
      message: "findSectionForCamInfoWithBlockId::Error retrieving Image table with image_type=" + img_type
    });
  });
};

exports.findSlideForCamInfoWithBlockId = (req, res) => {
  const img_type = req.params.id;
  const block_id = req.params.blockId;
  const cut_index = req.params.cut_index;
  // var condition = camera_location ? { camera_location: { [Op.like]: `%${camera_location}%` } } : null;
  Image.findOne({ 
    where: {
      image_type: img_type,
      camera_location: 3, 
      blocks__id: block_id,
      cut_index: cut_index,
    },
    // where: condition ,
     order: [['created_at', 'DESC' ]]
   })
  .then(data => {
    message: "found record(s)" + img_type
    res.send(data);
  })
  .catch(err => {
    res.status(500).send({
      message: "Error retrieving Image table with image_type=" + img_type
    });
  });
};

// Find a single Image with an id as image_type
exports.findSectionForCamInfo = (req, res) => {
  const img_type = req.params.id;

  // var condition = camera_location ? { camera_location: { [Op.like]: `%${camera_location}%` } } : null;
  Image.findOne({ 
    where: {
      image_type: img_type,
      camera_location: 1, 
    },
    // where: condition ,
     order: [['id', 'DESC' ]]
   })
  .then(data => {
    res.send(data);
  })
  .catch(err => {
    res.status(500).send({
      message: "Error retrieving Image table with image_type=" + img_type
    });
  });
};

// Find a single Image with an id as image_type
exports.findSectionForCamInfoCutIndex = (req, res) => {
  const img_type = req.params.id;
  const cut_index = req.params.id1;
  // const blkId = req.params.blkId;

  // var condition = camera_location ? { camera_location: { [Op.like]: `%${camera_location}%` } } : null;
  Image.findAll({ 
    where: {
      image_type: img_type,
      camera_location: 1, 
      cut_index: cut_index, 
      // blocks__id: blkId, 
    },
    // where: condition ,
     order: [['id', 'DESC' ]]
   })
  .then(data => {
    res.send(data);
  })
  .catch(err => {
    res.status(500).send({
      message: "Error retrieving Image table with image_type=" + img_type
    });
  });
};

// Find a single Image with an id as image_type
exports.findTapeForCamInfo = (req, res) => {
  const img_type = req.params.id;

  // var condition = camera_location ? { camera_location: { [Op.like]: `%${camera_location}%` } } : null;
  Image.findOne({ 
    where: {
      image_type: img_type,
      camera_location: 2, 
    },
    // where: condition ,
     order: [['id', 'DESC' ]]
   })
  .then(data => {
    res.send(data);
  })
  .catch(err => {
    res.status(500).send({
      message: "Error retrieving Image table with image_type=" + img_type
    });
  });
};

// Find a single Image with an id as image_type
exports.findSlideForCamInfo = (req, res) => {
  const img_type = req.params.id;

  // var condition = camera_location ? { camera_location: { [Op.like]: `%${camera_location}%` } } : null;
  Image.findOne({ 
    where: {
      image_type: img_type,
      camera_location: 3, 
    },
    // where: condition ,
     order: [['id', 'DESC' ]]
   })
  .then(data => {
    res.send(data);
  })
  .catch(err => {
    res.status(500).send({
      message: "Error retrieving Image table with image_type=" + img_type
    });
  });
};

// Update a Image by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Image.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Image was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Image with id=${id}. Maybe Image was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Image with id=" + id
      });
    });
};

// Delete a Image with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Image.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Image was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Image with id=${id}. Maybe Image was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Image with id=" + id
      });
    });
};

// Delete all Images from the database.
exports.deleteAll = (req, res) => {
  Image.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Images were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all images."
      });
    });
};

// find all published Image
exports.findAllPublished = (req, res) => {
  // Image.findAll({ where: { published: true } })
  Image.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving images."
      });
    });
};
