const db = require("../models");
const Session = db.sessions;
const Op = db.Sequelize.Op;

// Create and Save a new Session
exports.create = (req, res) => {
  // Validate request
  if (!req.body.download_path) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a session
  const session = {
    devices__id: req.body.devices__id,
    session_id: req.body.session_id
  };

  // Save Session in the database
  Session.create(session)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Session."
      });
    });
};

// Retrieve all Sessions from the database.
exports.findAll = (req, res) => {
  const  session_id = req.query.session_id;
  // const blocks__id = '7989';
  // var condition = blocks__id ? { blocks__id: { [Op.like]: `%${blocks__id}%` } } : null;
 
  Session.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "findAll::Some error occurred while retrieving sessions."
      });
    });
};

// Find a single Session with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Session.findOne({ 
    where: {
      id: id,
    },})
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "findOne:Error retrieving Session with id=" + id
      });
    });
};

exports.findSessionId = (req, res) => {
  const session_id = req.params.sessionId;

  Session.findOne({ 
    where: {
      session_id: session_id,
    },
    // where: condition ,
     order: [['created_at', 'DESC' ]]
   })
  .then(data => {
    message: "findSessionId:found record(s)" + session_id
    res.send(data);
  })
  .catch(err => {
    res.status(500).send({
      message: "findSessionId:Error retrieving Session table with session_id=" + session_id
    });
  });
};

// Update a Session by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Session.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Session was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Session with id=${id}. Maybe Session was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Session with id=" + id
      });
    });
};

// Delete a Session with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Session.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Session was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Session with id=${id}. Maybe Session was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Session with id=" + id
      });
    });
};

// Delete all Sessions from the database.
exports.deleteAll = (req, res) => {
  Session.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Sessions were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all sessions."
      });
    });
};

// // find all published session
// exports.findAllPublished = (req, res) => {
//   Session.findAll({ where: { published: true } })
//     .then(data => {
//       res.send(data);
//     })
//     .catch(err => {
//       res.status(500).send({
//         message:
//           err.message || "Some error occurred while retrieving sessions."
//       });
//     });
// };
