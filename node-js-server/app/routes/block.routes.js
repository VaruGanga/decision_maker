module.exports = app => {
  const blocks = require("../controllers/block.controller.js");

  var router = require("express").Router();

  // Create a new Block
  router.post("/", blocks.create);

  // Retrieve all Blocks
  router.get("/", blocks.findBlocks);
  router.get("/all/blocks", blocks.findAll);

  // Retrieve all block_id Blocks
  // router.get("/block_id", blocks.findOneBlockId);
  router.get("/block_id/:block_id/sessionId/:sessionId", blocks.findOneBlockId);
  // router.get("/id/:id/sessionId/:sessionId", blocks.findOneBlockId);
  // blockId/:blockId/cutIndex/:cut_index/sessionId/:sessionId

  // Retrieve a single Block with id
  router.get("/:id", blocks.findOne);

  // Update a Block with id
  router.put("/:id", blocks.update);

  // Delete a Block with id
  router.delete("/:id", blocks.delete);

  // Delete all Blocks
  router.delete("/", blocks.deleteAll);

  app.use('/api/blocks', router);
};
