module.exports = app => {
  const sessions = require("../controllers/session.controller.js");

  var router = require("express").Router();

  // Create a new session
  router.post("/", sessions.create);

  // Retrieve all sessions
  router.get("/", sessions.findAll);

  // Retrieve all published sessions
  // router.get("/published", sessions.findAllPublished);

  // Retrieve a single session with id
  router.get("/:id", sessions.findOne);

  // //this.http.get<Image[]>(`${baseUrl}?cameraLocation=${cameraLocation}/?image_type=${image_type}/?blocks__id=${blocks__id}`);
  router.get("/sessionId/:sessionId", sessions.findSessionId);

  // router.get("/tape/:id/blockId/:blockId/cutIndex/:cut_index/sessionId/:sessionId", sessions.findTapeForCamInfoWithBlockId);

  // router.get("/section/:id/blockId/:blockId/cutIndex/:cut_index/sessionId/:sessionId", sessions.findSectionForCamInfoWithBlockId);

  // router.get("/slide/:id/blockId/:blockId/cutIndex/:cut_index/sessionId/:sessionId", sessions.findSlideForCamInfoWithBlockId);

  // // Retrieve a single Image with id
  // // router.get("/?camera_location=:id1/image_type=:id2", images.findFacingForCamInfo);

  // // Retrieve a single Image from facing with session type input here as :id 
  // router.get("/facing/:id", sessions.findFacingForCamInfo);

  // // Retrieve a single Image from tape with session type input here as :id 
  // router.get("/section/:id", sessions.findSectionForCamInfo);

  // // Retrieve a single Image from tape with session type input here as :id 
  // router.get("/tape/:id", sessions.findTapeForCamInfo);

  // // Retrieve a single Image from tape with session type input here as :id 
  // router.get("/slide/:id", images.findSlideForCamInfo);

  // Update a Image with id
  router.put("/:id", sessions.update);

  // Delete a Image with id
  router.delete("/:id", sessions.delete);

  // Delete all Images
  router.delete("/", sessions.deleteAll);

  app.use('/api/sessions', router);
};
