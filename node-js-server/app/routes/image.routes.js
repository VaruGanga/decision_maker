module.exports = app => {
  const images = require("../controllers/image.controller.js");

  var router = require("express").Router();

  // Create a new Image
  router.post("/", images.create);

  // Retrieve all Images
  router.get("/", images.findAll);

  // Retrieve all published Images
  router.get("/published", images.findAllPublished);

  // Retrieve a single Image with id
  router.get("/:id", images.findOne);

  //this.http.get<Image[]>(`${baseUrl}?cameraLocation=${cameraLocation}/?image_type=${image_type}/?blocks__id=${blocks__id}`);
  router.get("/facing/:id/blockId/:blockId/cutIndex/:cut_index/sessionId/:sessionId", images.findFacingForCamInfoWithBlockId);

  router.get("/tape/:id/blockId/:blockId/cutIndex/:cut_index/sessionId/:sessionId", images.findTapeForCamInfoWithBlockId);

  router.get("/section/:id/blockId/:blockId/cutIndex/:cut_index/sessionId/:sessionId", images.findSectionForCamInfoWithBlockId);

  router.get("/slide/:id/blockId/:blockId/cutIndex/:cut_index/sessionId/:sessionId", images.findSlideForCamInfoWithBlockId);

  // Retrieve a single Image with id
  // router.get("/?camera_location=:id1/image_type=:id2", images.findFacingForCamInfo);

  // Retrieve a single Image from facing with image type input here as :id 
  router.get("/facing/:id", images.findFacingForCamInfo);

  // Retrieve all ldf records for given sessions_id
  router.get("/ldf/sessionId/:sessionId/cutIndex/:cut_index", images.findLdf);

  // Retrieve a single Image from tape with image type input here as :id 
  router.get("/section/:id", images.findSectionForCamInfo);

    // Retrieve a single Image from tape with image type input here as :id 
    // router.get("/section/:id/:id1/:blkId", images.findSectionForCamInfoCutIndex);
    router.get("/section/:id/:id1", images.findSectionForCamInfoCutIndex);

  // Retrieve a single Image from tape with image type input here as :id 
  router.get("/tape/:id", images.findTapeForCamInfo);

  // Retrieve a single Image from tape with image type input here as :id 
  router.get("/slide/:id", images.findSlideForCamInfo);

  // Update a Image with id
  router.put("/:id", images.update);

  // Delete a Image with id
  router.delete("/:id", images.delete);

  // Delete all Images
  router.delete("/", images.deleteAll);

  app.use('/api/images', router);
};
