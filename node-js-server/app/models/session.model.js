module.exports = (sequelize, Sequelize) => {
  const Session = sequelize.define("session", {
    created_at: {
      type: Sequelize.STRING,
      get () {
          if (this.getDataValue('created_at')) // or use get(){ }
              return this.getDataValue('created_at')
              .toLocaleString('en-US');
      }
  },
    updated_at: {
        type: Sequelize.STRING,
        get () {
            if (this.getDataValue('updated_at')) // or use get(){ }
                return this.getDataValue('updated_at')
                .toLocaleString('en-US');
        }
    },
    devices__id: {
      type: Sequelize.STRING
    },
    session_id: {
      type: Sequelize.STRING
    }},
      {
        timestamps: false
    });

  return Session;
};
