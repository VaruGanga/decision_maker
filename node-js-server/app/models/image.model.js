
module.exports = (sequelize, Sequelize) => {
  const Block = sequelize.define("block", {
    created_at: {
      type: Sequelize.STRING,
      get () {
          if (this.getDataValue('created_at')) // or use get(){ }
              return this.getDataValue('created_at')
              .toLocaleString('en-US');
      }
  },
    updated_at: {
        type: Sequelize.STRING,
        get () {
            if (this.getDataValue('updated_at')) // or use get(){ }
                return this.getDataValue('updated_at')
                .toLocaleString('en-US');
        }
    },
    devices__id: {
      type: Sequelize.STRING
    },
    sessions__id: {
      type: Sequelize.STRING
    },
    block_id: {
      type: Sequelize.STRING
    }},
    {
      timestamps: false
  
  });

  return Block;
};

module.exports = (sequelize, Sequelize) => {
  const Image = sequelize.define("image", {
    created_at: {
      type: Sequelize.DATE,
      get () {
          if (this.getDataValue('created_at')) // or use get(){ }
              return this.getDataValue('created_at')
              .toLocaleString('en-US');
      }
  },
    updated_at: {
        type: Sequelize.DATE,
        get () {
            if (this.getDataValue('updated_at')) // or use get(){ }
                return this.getDataValue('updated_at')
                .toLocaleString('en-US');
        }
    },
    download_path: {
      type: Sequelize.STRING
    },
    download_path: {
      type: Sequelize.STRING
    },
    description: {
      type: Sequelize.STRING
    },
    comment: {
      type: Sequelize.STRING
    },
    camera_location: {
      type: Sequelize.STRING
    },
    image_type: {
      type: Sequelize.STRING
    },
    blocks__id: {
      type: Sequelize.STRING
    },
    sessions__id: {
      type: Sequelize.STRING
    },  
    cut_index: {
      type: Sequelize.STRING
    }, 
    func: {
      type: Sequelize.STRING
    },  
    facing_decision: {
      type: Sequelize.STRING
    },  
    facing_action: {
      type: Sequelize.STRING
    },  
    image_index: {
      type: Sequelize.STRING
    },  
    json: {
      type: Sequelize.STRING
    }},
    {
      timestamps: false
  });

  return Image;
};



