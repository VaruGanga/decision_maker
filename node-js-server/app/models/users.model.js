module.exports = (sequelize, Sequelize) => {
  const Image = sequelize.define("image", {
    download_path: {
      type: Sequelize.STRING
    },
    description: {
      type: Sequelize.STRING
    },
    comment: {
      type: Sequelize.STRING
    },
    camera_location: {
      type: Sequelize.STRING
    },
    image_type: {
      type: Sequelize.STRING
    },
    json: {
      type: Sequelize.STRING
    }
  });

  return Image;
};
