
module.exports = (sequelize, Sequelize) => {
  const Device = sequelize.define("device", {
    created_at: {
      type: Sequelize.STRING,
      get () {
          if (this.getDataValue('created_at')) // or use get(){ }
              return this.getDataValue('created_at')
              .toLocaleString('en-US');
      }
  },
    updated_at: {
        type: Sequelize.STRING,
        get () {
            if (this.getDataValue('updated_at')) // or use get(){ }
                return this.getDataValue('updated_at')
                .toLocaleString('en-US');
        }
    },
    device_id: {
      type: Sequelize.STRING
    },  
    json: {
      type: Sequelize.STRING
    }},
    {
      timestamps: false
  });

  return Device;
};



